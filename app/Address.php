<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model {
    
    use SoftDeletes;

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function order() {
        return $this->belongsTo('App\Order', 'shipping_address_id', 'billing_address_id');
    }
    
    public function state() {
        return $this->belongsTo('App\State', 'state', 'state_code');
    }
    
    public function city() {
        return $this->belongsTo('App\City', 'city', 'id');
    }

}
