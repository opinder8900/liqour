<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'phone', 'registered_for'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }
    //Custom claims are used in generating the JWT token.
    public function getJWTCustomClaims() {
        return [];
    }

    /**
     * This will be used for tradesman
     * @return type
     */
    public function shops() {
        return $this->hasMany('App\Shop');
    }

    public function products() {
        return $this->hasMany('App\Product');
    }

    public function addresses() {
        return $this->hasMany('App\Address');
    }

    public function orders() {
        return $this->hasMany('App\Order');
    }

    /**
     * This will be used as a shopkeeper
     * @return type
     */
    public function shop() {
        return $this->hasOne('App\Shop', 'shopkeeper_id');
    }

    public function delivery_profile() {
        return $this->hasOne('App\DeliveryProfile');
    }

}
