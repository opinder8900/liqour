<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model {
    protected $table = 'business';
    public function categories() {
        return $this->hasMany('App\Category', 'category_id', 'id');
    }
    public function shops() {
        return $this->hasMany('App\Shop', 'business_id', 'id');
    }
    
}
