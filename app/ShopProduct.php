<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopProduct extends Model {

    protected $fillable = ['product_id','price','shop_id'];    
    public function product() {
        return $this->belongsTo('App\Product');
    }

}
