<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model {

    public function orders() {
        return $this->hasMany('App\Order');
    }
    public function products() {
        return $this->hasMany('App\Product');
    }

    public function categories() {
        return $this->hasMany('App\Category');
    }
    
    public function customers() {
        return $this->hasManyThrough('App\User','App\Order','shop_id','id','user_id','user_id');
    }

    public function state() {
        return $this->belongsTo('App\State', 'state_code', 'state_code');
    }

    public function city() {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }
     public function user() {
        return $this->hasOne('App\User','id','user_id');
    }
        public function business() {
        return $this->hasOne('App\Business','id','business_id');
    }
     public function shopKeeper() {
        return $this->hasOne('App\User','id','shopkeeper_id');
    }
    

}
