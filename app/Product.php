<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    public function shop() {
        return $this->belongsTo('App\Shop');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

    public function brand() {
        return $this->belongsTo('App\Brand');
    }

    public function business() {
        return $this->belongsTo('App\Business');
    }

    public function order_item() {
        return $this->belongsTo('App\OrderItem');
    }

    //added by Jatinder
    //please dont remove
    public function shop_product() {
        // return $this->belongsTo('App\ShopProduct');
        return $this->hasOne('App\ShopProduct');
        //return $this->hasOneThrough('App\Product','App\ShopProduct','product_id','id');
    }

}
