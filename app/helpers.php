<?php

function getProductFromCartById($id,$shop) {
    $collection = ShoppingCart::search(['id' => $id]);
    $collection = $collection->toArray();
   
    $raw_id = array_key_first($collection);
    if (!empty($collection[$raw_id])) {
		if($collection[$raw_id]["shop_id"] == $shop){
			$qty = $collection[$raw_id]["qty"];
		}else{
			$qty = "";
		}
        
        return $qty;
    }
    
}

function in_range( $min, $max) {
	$currentTime = time();
    return $currentTime >= $min && $currentTime <= $max;
}

function orderPlacedSendSms($number,$textmessage){
		// Account details
$apiKey = urlencode('zWgwLYLIInY-424FIe8AaNI3xueRU34wREUA6gzFCD');
// Message details
$numbers = array($number);
$sender = urlencode('TXTLCL');
$message = rawurlencode($textmessage);
 
$numbers = implode(',', $numbers);
 
// Prepare data for POST request
$data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender'=> $sender, 'message' => $message);
// Send the POST request with cURL
$ch = curl_init('https://api.textlocal.in/send/');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
// Process your response here
return $response;

}

function orderPlacedSendSmsToShop($number,$textmessage){
		// Account details
$apiKey = urlencode('zWgwLYLIInY-424FIe8AaNI3xueRU34wREUA6gzFCD');
// Message details
$numbers = array($number);
$sender = urlencode('TXTLCL');
$message = rawurlencode($textmessage);
 
$numbers = implode(',', $numbers);
 
// Prepare data for POST request
$data = array('apikey' => $apiKey, 'numbers' => $numbers, 'sender'=> $sender, 'message' => $message);
// Send the POST request with cURL
$ch = curl_init('https://api.textlocal.in/send/');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
// Process your response here
return $response;

}

?>
