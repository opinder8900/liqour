<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model {
    public function products() {
        return $this->hasMany('App\Product');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }
    public function business() {
        return $this->belongsTo('App\Business');
    }

}
