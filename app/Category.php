<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

//    public function categories() {
//        return $this->hasMany(Category::class);
//    }
//
//    public function childrenCategories() {
//        return $this->hasMany(Category::class)->with('categories');
//    }

    public function childs() {
        return $this->hasMany('App\Category', 'category_id', 'id');
    }

    public function products() {
        return $this->hasMany('App\Product');
    }

    public function shop() {
        return $this->belongsTo('App\Shop');
    }
    
    public function business() {
        return $this->belongsTo('App\Business');
    }

}
