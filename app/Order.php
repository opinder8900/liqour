<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function shop() {
        return $this->belongsTo('App\Shop');
    }

    public function items() {
        return $this->hasMany('App\OrderItem');
    }
     public function billing_address() {
        return $this->belongsTo('App\Address','billing_address_id');
    }
     public function shipping_address() {
        return $this->belongsTo('App\Address','shipping_address_id');
    }

}
