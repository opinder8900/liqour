<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model {

    protected $fillable = [
        'product_id',
        'name',
        'price',
        'image',
        'category',
        'brand',
        'size',
        'quantity',
        'sub_total'
        ];

    public function order() {
        return $this->belongsTo('App\Order');
    }
    
     public function product() {
        return $this->hasOne('App\Product','id','product_id');
    }

}
