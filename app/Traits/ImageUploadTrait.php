<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Support\Str;

trait ImageUploadTrait {

    public function UserImageUpload($query) { // Taking input image as parameter
        $image_name = Str::random(20);
        $ext = strtolower($query->getClientOriginalExtension()); // You can use also getClientOriginalName()
        $image_full_name = $image_name . '.' . $ext;
        $path = public_path() . '/uploads/' . Auth::user()->id;

        $subPath = "uploads/" . Auth::user()->id;
//        if (!Storage::exists($path)) {
//            Storage::makeDirectory($path, 0775, true); //creates directory
//        }
        //dd(Storage::disk('public')->has($subPath));
        if (!Storage::disk('public')->has($subPath)) {
            Storage::disk('public')->makeDirectory($subPath);
        }
        //die;
        $upload_path = $subPath;    //Creating Sub directory in Public folder to put image
        $image_url = $upload_path . '/' . $image_full_name;
        $success = $query->move($upload_path, $image_full_name);

        return $image_url; // Just return image
    }

}

?>
