<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function state()
    {
        return $this->hasOne('App\State','state_code','state_code');
    }
    
    
}
