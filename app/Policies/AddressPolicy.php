<?php

namespace App\Policies;

use App\User;
use App\Address;
use Illuminate\Auth\Access\HandlesAuthorization;

class AddressPolicy {

    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    public function update(User $user, Address $address) {
        return $user->id === $address->user_id;
    }

    public function delete(User $user, Address $address) {
        return $user->id === $address->user_id;
    }
    public function set(User $user, Address $address) {
        return $user->id === $address->user_id;
    }

}
