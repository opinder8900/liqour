<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddDeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'license_photo' => 'required|mimes:jpeg,jpg,png',
            'rc_photo' => 'required|mimes:jpeg,jpg,png',
            'adhaar_photo' => 'mimes:jpeg,jpg,png',
            'pan_photo' => 'mimes:jpeg,jpg,png',
        ];
    }
}


