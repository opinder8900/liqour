<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id' => 'required',
            'category_id' => 'required',
            'name' => 'required|min:2',
            'image' => 'mimes:jpeg,jpg,png',
            'slug' => ['required','alpha_dash',
                Rule::unique('brands')->ignore($this->id),'max:191'],
        ];
    }
}
