<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'business_id' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'name' => 'required|min:2',
            'image' => 'required|mimes:jpeg,jpg,png',
            'size' => 'required',
            'size_label' => 'required',
            'slug' => ['required','alpha_dash',
                Rule::unique('products')->ignore($this->id),'max:191'],
        ];
    }
}
