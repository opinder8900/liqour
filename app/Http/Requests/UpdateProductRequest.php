<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProductRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'category_id' => 'required',
            'description' => 'required',
            'name' => 'required|min:2',
            'image' => 'mimes:jpeg,jpg,png',
            'slug' => ['required',
                'alpha_dash',
                Rule::unique('products')->ignore($this->product),
            ],
        ];
    }

}
