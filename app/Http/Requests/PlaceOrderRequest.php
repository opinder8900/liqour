<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaceOrderRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'payment_mode' => 'required',
            //'billing_address_id' => 'required',
            //'shipping_address_id' => 'required',
        ];
    }

    public function messages() {
        return [
            'payment_mode.required' => 'Please select type of payment.',
           // 'shipping_address_id.required' => 'Please select shipping address',
        ];
    }

}
