<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddressRequest;
use Auth;
use App\Shop;
use App\Product;
use App\Category;
use App\Address;
use App\State;
use App\City;
use ShoppingCart;
use Validator,
    Redirect,
    Response;

class AddressController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        $addressId = 0; 
        if ($request->session()->has('location')) {
            $addressId = $request->session()->get('location.id');
         }
        $userAddresses = Address::with("city", 'state')
                ->where('user_id', Auth::user()->id)
                ->whereNotIn('id',[$addressId])
                ->get();
        return view('address.index', compact('userAddresses'));
    }

//    public function create1() {
//        $states = State::get();
//        $cities = City::get();
//        $addresses = Address::with("user")->get();
//        return view('address.create', compact('addresses', 'states', 'cities'));
//    }

//    public function store1(AddressRequest $request) {
//        $address = new Address;
//        $this->keepSingleDefaultAddress($request->default);
//        $address->full_name = $request->full_name;
//        $address->address_line_1 = $request->address_line_1;
//        $address->address_line_2 = $request->address_line_2;
//        $address->landmark = $request->landmark;
//        $address->city = $request->city;
//        $address->state = $request->state;
//        $address->country = $request->country;
//        $address->pin_code = $request->pin_code;
//        $address->phone = $request->phone;
//        $address->type = 'shipping';
//        $address->default = $request->default === null ? false : true;
//        $address->user_id = Auth::user()->id;
//
//        if ($address->save()) {
//            if (isset($request->redirect_url)) {
//                return redirect($request->redirect_url);
//                ;
//            }
//            return redirect('address')->with('success', 'Your address has been added!');
//        }
//    }

//    public function edit1($id) {
//        $address = Address::with('state', 'city')->find($id);
//        $states = State::get();
//        $cities = City::get();
//        return view('address.edit', compact('address', 'states', 'cities'));
//    }

//    public function update1(AddressRequest $request, $id) {
//        $address = Address::find($id);
//        $this->authorize('update', $address);
//        $this->keepSingleDefaultAddress($request->default);
//        $address->full_name = $request->full_name;
//        $address->address_line_1 = $request->address_line_1;
//        $address->address_line_2 = $request->address_line_2;
//        $address->landmark = $request->landmark;
//        $address->city = $request->city;
//        $address->state = $request->state;
//        $address->country = $request->country;
//        $address->pin_code = $request->pin_code;
//        $address->phone = $request->phone;
//        $address->type = 'shipping';
//        $address->default = $request->default === null ? false : true;
//        $address->user_id = Auth::user()->id;
//        $address->save();
//        return redirect('address')->with('success', 'Address updated!');
//    }

    public function destroy($id) {
        $address = Address::findOrFail($id);
        $this->authorize('delete', $address);
        $address->delete();
        return redirect('address')->with('warning', 'Address deleted!');
    }

//    private function keepSingleDefaultAddress($default) {
//        $default = $default === null ? false : true;
//        if ($default) {
//            $defaultAddressCount = Address::where([
//                        'user_id' => Auth::user()->id,
//                        'default' => true
//                    ])->count();
//            if ($defaultAddressCount) {
//                $values = Address::where('user_id', Auth::user()->id)
//                        ->update(['default' => false]);
//            }
//        }
//    }

    public function save(AddressRequest $request) {
        $response = ['status' => 'OK',
            'errors' => [],
            'redirect_to' => '\''
        ];
        $address = new Address;
        //$this->keepSingleDefaultAddress($request->default);
        $address->latitude = $request->latitude;
        $address->longitude = $request->longitude;
        $address->formatted_address = $request->formatted_address;
        $address->door_flat_number = $request->door_flat_number;
        $address->area = $request->area;
        $address->landmark = $request->landmark;
        $address->city = 'Mohali';
        $address->state = 'Punjab';
        $address->country = 'IN';
        $address->type = $request->type;
        $address->ip = $request->ip();
        $address->default = $request->default === null ? false : true;
        $address->user_id = Auth::user()->id;
        $address->save();
        $this->__setAddressInSession($request,$address);
        return Response()->json($response);
    }

    public function set(Request $request, $id) {
        $address = Address::findOrFail($id);
        $this->authorize('set', $address);
        $this->__setAddressInSession($request,$address);
        ShoppingCart::destroy();
        return redirect('home');
    }

    private function __setAddressInSession(Request $request , $address) {
        //set address in session too
        $locationArr['id'] = $address->id;
        $locationArr['display'] = $address->door_flat_number .', '.$address->area;
        $locationArr['formatted_address'] = $address->formatted_address;
        $locationArr['type'] = $address->type;
        $locationArr['latitude'] = $address->latitude;
        $locationArr['longitude'] = $address->longitude;
        $request->session()->put('location', $locationArr);
    }

}
