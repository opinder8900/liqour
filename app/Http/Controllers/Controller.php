<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\User;
use App\City;
use App\State;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
    public function CheckIfEmailAlreadExists($email){
		$user = User::where("email",$email)->first();
		
		return response()->json( array('status' => $status, 'html'=>$returnHTML,"message"=>$message,'class'=>$class,"booked"=>$booked) );

		
	}
	
    public function getLatLong($address = []) {
        if (!empty($address)) {
            //Send request and receive json data by address
            if (is_array($address)) {
                $formattedAddress = str_replace(' ', '+', implode(" ", $address));
            } else {
                $formattedAddress = str_replace(' ', '+', $address);
            }
            $address = urlencode($address);
           
            if (!empty($address)) {
				
                $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyDzbTW1_KROAI3HdP-Z9ImET4lFNAQxa7A');

                $output = json_decode($geocodeFromAddr);
                

                /*Get latitude and longitute from json data*/
                $lat = $output->results[0]->geometry->location->lat;
                $lng = $output->results[0]->geometry->location->lng;

                /*Return latitude and longitude of the given address*/
                if (!empty($lat) && !empty($lng)) {
                    return [
                        'lat' => $lat,
                        'lng' => $lng
                    ];
                } else {
                    return "lat lng not found";
                }
            }
        }
    }
    
    public function getCityAndState($city){
		$city 	= City::with("state")->where('id',$city)->first();
		return $city;
		
	}
}
