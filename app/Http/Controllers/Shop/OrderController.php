<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Shop;
use Redirect;

class OrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $shop = Shop::where('shopkeeper_id', Auth::user()->id)->first();
        $orders = Order::where("shop_id", $shop->id)
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        //->get();
        return view('shop.order.index')->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $shop = Shop::where('shopkeeper_id', Auth::user()->id)->first();
        $order = Order::with('items')
                ->where(['id' => $id, "shop_id" => $shop->id])
                ->first();
        return view('shop.order.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view('shop.order.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
		$order 	= Order::find($id);
		$order->status = $request->input("order_status");
		if($order->save()){
			return Redirect::back();
		}
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function invoice($id) {
        $shop = Shop::where('shopkeeper_id', Auth::user()->id)->first();
        $order = Order::with('items')
                ->where(['id' => $id, "shop_id" => $shop->id])
                ->first();
        return view('shop.order.invoice')->with('order', $order);
    }

}
