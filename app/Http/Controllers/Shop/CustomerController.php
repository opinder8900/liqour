<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Shop;

class CustomerController extends Controller {

    public function index() {
        //$customers = Shop::customers()->get();
        //dd($customers);
        //die;
        $shop = Shop::where('shopkeeper_id', Auth::user()->id)->first();
        //echo $shop->id;
        $customers = $shop->customers()
                ->distinct('users.id')
                ->get();

        return view('shop.customer.index')->with('customers', $customers);
    }

}
