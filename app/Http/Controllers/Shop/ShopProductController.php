<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ShopProduct;
use App\Shop;
use App\Category;
use App\Brand;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ShopProductController extends Controller {

    use AuthorizesRequests;

    public function index(Request $request) {

        $shop = Shop::where("shopkeeper_id", Auth::user()->id)->firstOrFail();
        $categories=Category::where('status',1)->where('business_id',$shop->business_id)->pluck('name','id')->toArray();
        
        $search = $request->input('search');
        $category_id = $request->input('category_id');
        $brand_id = $request->input('brand_id');
        
        $shop_products=ShopProduct::query()->with('product.category')->whereHas('product', function($q) use($search,$category_id,$brand_id){
            if(!empty($search)){
                $q->where('name','like','%'.$search.'%');
            }
            if(!empty($category_id)){
                $q->where('category_id',$category_id);
            }
            if(!empty($brand_id)){
                $q->where('brand_id',$brand_id);
            }
        })->where("shop_id",$shop->id)->paginate();
        return view('shop.product.index', compact('shop_products','categories','shop'));
    }

    public function addRequest(Request $request) {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
//    public function create() {
//        $shops = Shop::pluck('name', 'id');
//        $categories = Category::where('category_id', null)->get();
//        $allCategories = Category::pluck('name', 'id')->all();
//        //dd($shops);
//        return view('shop.product.create', compact('shops', 'categories', 'allCategories'));
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
//    public function store(StoreProductRequest $request) {
//        $validated = $request->validated();
//        $shop = Shop::where("shopkeeper_id", Auth::user()->id)->first();
//        $filePath = "";
//        if ($request->file('image')) {
//            $filePath = $this->UserImageUpload($request->file('image'));
//        }
//        $product = new Product;
//        $product->category_id = $request->category_id;
//        $product->user_id = Auth::user()->id;
//        $product->shop()->associate($shop->id);
//        $product->slug = $request->name;
//        $product->name = $request->name;
//        $product->price = $request->price;
//        $product->status = $request->status;
//        $product->image = $filePath;
//        $product->in_stock_quantity = $request->in_stock_quantity;
//        $product->description = $request->description;
//        $product->save();
//        return redirect('shop/products')->with('success', 'Product saved!');
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $product = Product::find($id);
        return view('shop.product.show')
                        ->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function edit($id) {
//        $categories = Category::where('category_id', null)->get();
//        $product = Product::find($id);
//        return view('shop.product.edit')
//                        ->with('categories', $categories)
//                        ->with('product', $product);
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $shopProduct = ShopProduct::where('product_id', $id)->first();
        if (!$shopProduct->status) {
            if (!$shopProduct) {
                $shopProduct = new ShopProduct;
            }
            $shopProduct->shop_id = Auth::user()->shop->id;
            $shopProduct->product_id = $id;
            $shopProduct->status = True;
        } else {

            $shopProduct->status = False;
        }
        $shopProduct->save();
        return response()->json(['status'=>'ok']);
    }

    public function changeStatus(Request $request, $id,$shop) {
        $shopProduct = ShopProduct::where('product_id', $id)->where("shop_id",$shop)->first();
       
        if (!$shopProduct->status) {
            if (!$shopProduct) {
                $shopProduct = new ShopProduct;
            }
            $shopProduct->shop_id = Auth::user()->shop->id;
            $shopProduct->product_id = $id;
            $shopProduct->status = True;
        } else {

            $shopProduct->status = False;
        }
        $shopProduct->save();
        return response()->json(['status'=>'ok']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function destroy($id) {
//        $product = Product::findOrFail($id);
//        $this->authorize('delete', $product);
//        $product->delete();
//        return redirect('shop/products')->with('warning', 'Product deleted!');
//    }
}
