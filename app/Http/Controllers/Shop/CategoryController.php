<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;

class CategoryController extends Controller {

    use ImageUploadTrait;

    public function index() {

//        $data = ([
//            'name' => ('jat'),
//             'email' => ('engineer.jazz@gmail.com'),
//             'username' => 'username',
//             'phone' => 'phone',
//             'message' => 'phone',
//        ]);
//        Mail::to('engineer.jazz@gmail.com')->send(new WelcomeMail($data));
        //$category = Category::with("childs")->where("user_id", Auth::user()->id);
        //dd($category);die;
        $categories = Category::where('category_id', null)
                ->where('user_id', Auth::user()->id)
                ->where('shop_id', Auth::user()->shop->id)
                ->get();
        //$allCategories = Category::pluck('name', 'id')->all();

//        $categories = DB::table('categories')
//                ->whereNull('category_id')
//                //->include('childrenCategories')
//                ->get();
//        echo "<pre>";
//        print_r($categories);
//        foreach($categories as $cat){
//            print_r($cat->childs);
//        }
//        print_r($allCategories);die;
        return view('shop.category.index')->with('categories', $categories);
    }

    public function create() {
        $shops = Shop::pluck('name', 'id');
        $categories = Category::where('category_id', null)->get();
        $allCategories = Category::pluck('name', 'id')->all();
        //dd($shops);
        return view('shop.category.create', compact('shops', 'categories', 'allCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreCategoryRequest $request) {
        $validated = $request->validated();
        $shop = Shop::where("shopkeeper_id", Auth::user()->id)->first();
        $filePath = "";
        if ($request->file('image')) {
            $filePath = $this->UserImageUpload($request->file('image'));
        }
        //dd($filePath);
        //die;
        $category = new Category;
        $category->category_id = $request->category_id;
        $category->user_id = Auth::user()->id;
        $category->shop()->associate($shop->id);
        $category->slug = $request->slug;
        $category->name = $request->name;
        $category->status = $request->status;
        $category->image = $filePath;
        $category->description = $request->description;
        $category->save();

        // redirect
        Session::flash('message', 'Successfully created product!');
        return redirect('shop/categories')->with('success', 'Category created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $categories = Category::where('category_id', null)->get();
        $category = Category::findOrFail($id);
        return view('shop.category.edit', compact('categories'))->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id) {
        $validated = $request->validated();
        $category = Category::find($id);

        if ($request->file('image')) {
            $filePath = $this->UserImageUpload($request->file('image'));
            $category->image = $filePath;
        }

        $category->slug = $request->slug;
        $category->name = $request->name;
        $category->status = $request->status;

        $category->description = $request->description;
        $category->save();
        return redirect('shop/categories')->with('success', 'Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = Category::findOrFail($id);

        $row->delete();

        return redirect('shop/categories')->with('warning', 'Category deleted!');
    }

}
