<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\Shop;
use App\ShopProduct;
use Auth;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller {

    public function index() {
		$user_id 			= Auth::user()->id;
		$shop 				= Shop::where("shopkeeper_id",$user_id)->first();
		$todayOrders 		=  Order::where("shop_id",$shop->id)->whereDate('created_at', Carbon::today())->count();
		$todaySales 		=  Order::selectRaw(' sum(total) as total')->where("shop_id",$shop->id)->whereDate('created_at', Carbon::today())->where("status","delivered")->first();
		$lastWeekSales 		=  Order::selectRaw(' sum(total) as total')->where("shop_id",$shop->id)->whereDate('created_at',">", Carbon::now()->subDays(7))->where("status","delivered")->first();
		$inStockProducts 	= ShopProduct::where("shop_id",$shop->id)->where("status",1)->count();
        //echo AUth::user()->id;
        //dd(AUth::user()->shop->id);
//        $totalProducts = Product::where('user_id', AUth::user()->id)
//                ->count();
//        $totalOrders = Order::where('shop_id', AUth::user()->shop->id)
//                ->count();
        return view('shop.dashboard.index',compact("todayOrders","todaySales","inStockProducts","lastWeekSales"));
                       
    }

}
