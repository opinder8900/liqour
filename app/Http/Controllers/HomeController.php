<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Shop;
use App\Product;
use App\Category;
use ShoppingCart;
use App\Business;
use Illuminate\Support\Facades\DB;

//use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function welcome(Request $request) {
        if (Auth::check()) {
            switch (Auth::user()->role) {
                case 'shop':
                    return redirect('shop/dashboard');
                    break;

                case 'tradesman':
                    return redirect('tradesman/dashboard');
                    break;
            }
        }
        $businesses = [];
        if ($request->session()->has('location')) {
            $latitude = $request->session()->get('location.latitude');
            $longitude = $request->session()->get('location.longitude');

            $businesses = Business::where('status', True)
                    ->whereHas('shops', function($query)use($latitude, $longitude) {
                        $query->select([
                            '*',
                            $this->__getDistanceRawQuery($latitude, $longitude)
                        ]);
                        $query->having('distance', '<=', config('app.radius_in_km'));
                        $query->where('status', true);
                    })
                    ->get()
                    ->groupBy(['slug']);
            //dd($businesses);
        }
        return view('welcome')
                        ->with('businesses', $businesses);
    }

    private function __getDistanceRawQuery($latitude, $longitude) {
        return DB::RAW('( 6371 * acos( cos( radians(' . $latitude . ') ) 
              * cos( radians( shops.latitude ) ) 
              * cos( radians( shops.longitude ) - radians(' . $longitude . ') ) 
              + sin( radians(' . $latitude . ') ) 
              * sin( radians( shops.latitude ) ) ) ) AS distance');
    }

    public function shops(Request $request, $business) {
//        $city = \Request::segment(1);
//        $business = \Request::segment(2);
        if ($request->session()->has('location')) {
            $latitude = $request->session()->get('location.latitude');
            $longitude = $request->session()->get('location.longitude');
            $search = $request->input('search');
            if ($search) {
                $shops = Shop::whereHas('business', function($query) use ($business) {
                            $query->where('slug', $business);
                            $query->where('status', true);
                        })
                        ->where(function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('slug', 'LIKE', '%' . $search . '%')
                            ->orWhere('address', 'LIKE', '%' . $search . '%');
                        })
                        ->select([
                            '*',
                            $this->__getDistanceRawQuery($latitude, $longitude)
                        ])
                        ->having('distance', '<=', config('app.radius_in_km'))
                        ->where('status', true)
                        ->get();
            } else {
                $shops = Shop::whereHas('business', function($query) use ($business) {

                            $query->where('slug', $business);
                            $query->where('status', true);
                        })
                        ->select([
                            '*',
                            $this->__getDistanceRawQuery($latitude, $longitude)
                        ])
                        ->having('distance', '<=', config('app.radius_in_km'))
                        ->where('status', true)
                        ->get();
            }
            //dd($shops);
            return view('business.shops')
                            ->with('city', 'mohali')
                            ->with('business', $business)
                            ->with('shops', $shops);
        } else {
            abort(404, 'You have not shared your location yet.');
        }
    }

    public function products(Request $request, $business, $shop) {
        if ($request->session()->has('location')) {
            $latitude = $request->session()->get('location.latitude');
            $longitude = $request->session()->get('location.longitude');


            $shop = Shop::where('slug', $shop)
                    ->select([
                        '*',
                        $this->__getDistanceRawQuery($latitude, $longitude)
                    ])
                    ->having('distance', '<=', config('app.radius_in_km'))
                    ->where('status', true)
                    ->first();

            abort_unless($shop, 404, 'Oops, we can not deliver at this location.');

            $search = $request->input('search');
            if ($search) {
                $groupByCategoryProducts = Product::where([
                            'status' => true
                        ])
                        ->with(['brand' => function($query) {
                                $query->where('status', true);
                            }])
                        ->with(['category' => function($query) {
                                $query->select('id', 'name', 'slug');
                                $query->where('status', true);
                            }])
                        ->whereHas('shop_product', function($query) use($shop) {
                            $query->where('shop_id', $shop->id);
                            $query->where('status', true);
                        })
                        ->where(function ($query) use ($search) {
                            $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('slug', 'LIKE', '%' . $search . '%')
                            ->orWhere('price', 'LIKE', '%' . $search . '%');
                        })
                        ->get()
                        ->groupBy(['category.name', 'brand.name']);
            } else {
                $groupByCategoryProducts = Product::where([
                            'status' => true
                        ])
                        ->with(['brand' => function($query) {
                                $query->where('status', True);
                            }])
                        ->with(['category' => function($query) {
                                $query->select('id', 'name', 'slug');
                                $query->where('status', True);
                            }])
                        ->whereHas('shop_product', function($query) use($shop) {
                            $query->where('shop_id', $shop->id);
                            $query->where('status', True);
                        })
                        ->get()
                        ->groupBy(['category.name', 'brand.name']);
            }

            return view('products')
                            ->with('business', $business)
                            ->with('shop', $shop)
                            //->with('products', $products)
                            ->with('groupByCategoryProducts', $groupByCategoryProducts);
        } else {
            abort(404, 'Oops, please share your delivery location.');
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('home');
    }

}
