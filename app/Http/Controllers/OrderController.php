<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PlaceOrderRequest;
use App\Order;
use App\OrderItem;
use App\Product;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use ShoppingCart;
Use Session;

class OrderController extends Controller {

    public function placeOrder(PlaceOrderRequest $request) {

        //dd($request->input());
        //$validated = $request->validated();
        //
        
        $cartItems = ShoppingCart::all();
        $user = Auth::user();
        //dd($cartItems);
        $order = new Order;
        $order->user()->associate(Auth::user()->id);

        //$order->billing_address()->associate($request->billing_address_id);
        $order->shipping_address()->associate(Session::get("location")["id"]);
        $order->tax = 0.00;
        $shipId = null;
        
        foreach ($cartItems as $cartItem) {
            $order->total += $cartItem->total;
            $shipId = $cartItem->shop_id;
        }
        $shop = Shop::where("id", $shipId)->first();
        $order->shop()->associate($shipId);
        $order->payment_mode = $request->payment_mode;
        $order->status = 'pending';
        $order->ip = $request->ip();
        $order->save();
        $message = "Hi ".Auth::user()->name.", You order for ";
        $shopMessage = "New Order ";
        $items = "";
        foreach ($cartItems as $cartItem) {
            $product = Product::find($cartItem->id)
                    ->with('category')
                    ->with('brand')
                    ->first();
            //dd($product->category->name);
            $order->items()->create([
                'order_id' => $order->id,
                'product_id' => $product->id,
                'name' => $cartItem->name,
                'price' => $cartItem->price,
                'quantity' => $cartItem->qty,
                'image' => $product->image,
                'size' => $product->size,
                'category' => $product->category->name,
                'brand' => $product->brand->name,
                'sub_total' => $cartItem->total,
            ]);
            $items.= $product->brand->name. " ". $cartItem->name." ";
            $shopMessage.= $product->brand->name. " ". $cartItem->name." Quantity - ".$cartItem->qty." Size - ".$product->size."<br>";

            //decrease in_stock_quantity
//            $product->in_stock_quantity = $product->in_stock_quantity - $cartItem->qty;
//            $product->save();
        }
        //delete cart items
        ShoppingCart::destroy();
		$message = $message."".$items." has been placed. The total amount for order is ".$order->total." Enjoy shopping!!";
        $response = orderPlacedSendSms($user->phone,$message);
        $response = orderPlacedSendSmsToShop($shop->primary_phone,$shopMessage);
         
        return redirect('thank-you')->with('success', 'Your order has been placed!');
    }

    public function thankYou() {
		ShoppingCart::destroy();
        return view('order.thankyou');
    }

    public function cancel($id) {
        $order = Order::with("shop", 'items.product')
                ->where([
                    "user_id" => Auth::user()->id,
                    'id' => $id,
                    "status" => "pending"
                ])
                ->first();
//dd($order);
        return view('order.cancel')
                        ->with('order', $order);
    }

    public function update(Request $request, $id) {
        //dd($id);
        $order = Order::findOrFail($id);
        $this->authorize('update', $order);
        $order->cancelled_reason = $request->reason;
        $order->status = 'cancelled';
        $order->save();
        return redirect('my-orders')->with('success', 'Order Canceled!');
    }

}
