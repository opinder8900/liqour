<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Geocoder\Laravel\ProviderAndDumperAggregator as Geocoder;
use App\Business;

class LocationController extends Controller {

    public function setPlace(Request $request, Geocoder $geocoder) {

        $locationArr['formatted_address'] = $request->formatted_address;
        $locationArr['latitude'] = $request->latitude;
        $locationArr['longitude'] = $request->longitude;
        $city = $locationArr['locality'] = 'mohail';
        $request->session()->put('location', $locationArr);
        $businesses = $this->getBusinesses();
        return view('location.main')
                        ->with('businesses', $businesses)
                        ->with('city', $city);
    }

    public function set(Request $request, Geocoder $geocoder) {
        //dd($geocoder->geocode('Los Angeles, CA')->get());
        $adderss = $geocoder->reverse($request->latitude, $request->longitude)->get();
        $locationArr['formatted_address'] = $adderss->first()->getFormattedAddress();
        $locationArr['sub_locality'] = $adderss->first()->getSubLocality();
        $city = $locationArr['locality'] = $adderss->first()->getLocality();
        $locationArr['street_name'] = $adderss->first()->getStreetName();
        $locationArr['street_number'] = $adderss->first()->getStreetNumber();
        $locationArr['postal_code'] = $adderss->first()->getPostalCode();
        $locationArr['country'] = $adderss->first()->getCountry()->getName();
        $locationArr['bounds'] = $adderss->first()->getBounds();
        $locationArr['latitude'] = $request->latitude;
        $locationArr['longitude'] = $request->longitude;
        $request->session()->put('location', $locationArr);
//        $city = 'mohali';
//        $request->session()->put('location.city', $city);
//        $request->session()->put('location.sub_locality', $city);
//        //$request->session()->put('location.state', 'punjab');
//        if($adderss->first()->getLocality()){
//            $city = strtolower($adderss->first()->getLocality());
//            $request->session()->put('location.city', $city);
//        }
//        if($adderss->first()->getSubLocality()){
//            $request->session()->put('location.sub_locality', $adderss->first()->getSubLocality());
//        }
//        $request->session()->put('location.latitude', $request->latitude);
//        $request->session()->put('location.longitude', $request->longitude);
        //$request->session()->put('location.zipcode', '140308');
        //$data = $request->session()->all();
        //dd($data);
        $businesses = $this->getBusinesses();
        return view('location.main')
                        ->with('businesses', $businesses)
                        ->with('city', $city);
    }

    public function getBusinesses() {
        return $businesses = Business::where('status', True)->get();
    }

}
