<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

//    protected function credentials(\Illuminate\Http\Request $request) {
//        return ['email' => $request->{$this->email()}, 'password' => $request->password, 'status' => True];
//    }

    protected function redirectTo1() {

        if (\Auth::user()->role == 'superadmin') {
            return 'superadmin/dashboard';
        }
        if (\Auth::user()->role == 'tradesman') {
            return 'tradesman/dashboard';
        }
        if (\Auth::user()->role == 'shop') {
            return 'shop/dashboard';
        }
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        if (auth()->attempt(['email' => $request->email, 'password' => $request->password])) {

            if (auth()->user()->status == false) {
                \Auth::logout();
                return back()->with('warning', 'Your account has not yet been activated. Please check Your email');
            }
            //dd(auth()->user()->role);
            $redirectTo = $this->redirectTo; 
            if (auth()->user()->role == 'superadmin') {
                $redirectTo = 'superadmin/dashboard';
            }
            if (auth()->user()->role == 'tradesman') {
                $redirectTo =  'tradesman/dashboard';
            }
            if (auth()->user()->role == 'shop') {
                $redirectTo =  'shop/dashboard';
            }
            return redirect($redirectTo);
        } else {
            return back()->with('warning', 'Address email or/and password are incorrect.');
        }
    }

}
