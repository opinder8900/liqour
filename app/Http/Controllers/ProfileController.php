<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PlaceOrderRequest;
use App\Http\Requests\EditProfileRequest;
use App\Order;
use App\OrderItem;
use App\State;
use App\City;
use App\User;
use App\Address;
use Illuminate\Support\Facades\Hash;
use App\Product;
use Illuminate\Support\Facades\Auth;
use ShoppingCart;
use App\Rules\MatchOldPassword;

class ProfileController extends Controller {

    public function myProfile(Request $request) {

        $user = Auth::user();

        return view('profile.my-profile', compact('user'));
    }

    public function myOrders(Request $request) {
        $user = Auth::user();
        //$orders	= Order::with("shop")->where("user_id",$user->id)->get();
        $new_orders = Order::with("shop", 'items.product')->where("user_id", $user->id)->where("status", "pending")->get();
        $past_orders = Order::with("shop")->where("user_id", $user->id)->where("status", "delivered")->get();
        $cancelled_orders = Order::with("shop")->where("user_id", $user->id)->where("status", "cancelled")->get();
        //dd($new_orders);
        return view('profile.my-orders', compact('user', 'new_orders', 'past_orders', 'cancelled_orders'));
    }

    public function myAddress(Request $request) {
        $states = State::get();
        $cities = City::get();
        $addresses = Address::with("user")->get();


        return view('profile.my-address', compact('addresses', 'states', 'cities'));
    }

    public function addAddress(Request $request) {
        $address = new Address;
        $address->full_name = $request->full_name;
        $address->address_line_1 = $request->address_line_1;
        $address->address_line_2 = $request->address_line_2;
        $address->landmark = $request->landmark;
        $address->city = $request->city;
        $address->state = $request->state;
        $address->country = $request->country;
        $address->pin_code = $request->pin_code;
        $address->phone = $request->phone;
        $address->type = $request->type;
        $address->user_id = Auth::user()->id;

        if ($address->save()) {
            $addresses = Address::with("user")->get();
            view('job.userjobs')->with('userjobs', $userjobs)->render();
            return response()->json($result);
        }


        return view('profile.my-address', compact('addresses', 'states', 'cities'));
    }

    public function changePassword(Request $request) {
        if ($request->method() == "POST") {
            $request->validate([
                'current_password' => ['required', new MatchOldPassword],
                'new_password' => ['required'],
                'new_confirm_password' => ['same:new_password'],
            ]);

            User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
            return redirect('change-password')->with('status', 'Password Changed');
        }


        return view('profile.change-password');
    }

    public function edit() {
//		if($request->method() == "POST"){
//			$request->validate([
//            'current_password' => ['required', new MatchOldPassword],
//            'new_password' => ['required'],
//            'new_confirm_password' => ['same:new_password'],
//			]);
//   
//			User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
//			return redirect('change-password')->with('status', 'Password Changed');
//		}

        $user = User::find(auth()->user()->id);
        return view('profile.edit-profile')
                ->with('user',$user);
    }
    

    public function editProfile(EditProfileRequest $request) {
		
		if($request->method() == "POST"){
			$request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
			]);
   
			User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
			return redirect('change-password')->with('status', 'Password Changed');
		}
        
  
         return view('profile.edit-profile');

    }

     public function update(EditProfileRequest $request) {
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->save();
        return redirect('my-profile')->with('success', 'Profile updated!');

    }


}
