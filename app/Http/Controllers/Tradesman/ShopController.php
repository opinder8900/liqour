<?php

namespace App\Http\Controllers\Tradesman;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\State;
use App\City;
use App\Shop;
use App\User;
use App\Business;
use App\Order;
use Session;
use Validator;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\ImageUploadTrait;
use Illuminate\Support\Facades\Hash;
use Redirect;
use App\Http\Requests\StoreUpdateRequest;
use App\Http\Requests\AddShopRequest;

use App\Http\Controllers\Controller as ExtendedController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon;


class ShopController extends ExtendedController {

    use ImageUploadTrait;
    use AuthorizesRequests;
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $shops = Shop::with("city", "state")->where("user_id", Auth::user()->id)->paginate(10);

        return view('tradesman.shops.index', compact("shops"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $states = State::all();
        $cities = City::all();
        $businesses= Business::where('status',1)->pluck('name','id')->toArray();
        return view('tradesman.shops.create', compact("states", "cities","businesses"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddShopRequest $request) {
        //~ $validatedData = $request->validate([
        //~ 'name' => 'required|min:2'
        //~ ]);
        $input['email'] = $request->input("email");
        $input['phone'] = $request->input("primary_phone");
        $rules = array('email' => 'unique:users,email',"phone"=>'unique:users,phone');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        } else {
			$getAddressData = $this->getCityAndState($request->city_id);
			
			$formattedAddress = $request->address." ".$getAddressData->city_name." ".$getAddressData->state->state_name." ".$request->zip;
			
			$latlng				= $this->getLatLong($formattedAddress);
			
            $filePath = "";
            if ($request->file('image')) {
                $filePath = $this->UserImageUpload($request->file('image'));
            }
            $open_at = date("H:i:s", strtotime($request->open_at));
            $close_at = date("H:i:s", strtotime($request->close_at));
            $shop = new Shop;
            $shop->name = $request->name;
            $shop->slug = $request->slug;
            $shop->address = $request->address;
            $shop->city_id = $request->city_id;
            $shop->state_code = $request->state_code;
            $shop->zip = $request->zip;
            $shop->primary_phone = $request->primary_phone;
            $shop->secondary_phone = $request->secondary_phone;
            $shop->open_at = $open_at;
            $shop->close_at = $close_at;
            $shop->image = $filePath;
            $shop->license_number = $request->license_number;
            $shop->latitude = $latlng["lat"];
            $shop->longitude = $latlng["lng"];
            $shop->type = $request->type;
            $shop->user_id = Auth::user()->id;
            $shop->business_id = $request->business_id;
            if ($shop->save()) {
                $user = new User;
                $user->password = Hash::make($request->password);
                $user->name = $request->name;
                $user->email = $request->email;
                $user->role = "shop";
                $user->registered_for = $request->type;
                $user->phone = $request->primary_phone;
                if ($user->save()) {
                    $updateShop = Shop::find($shop->id);
                    $updateShop->shopkeeper_id = $user->id;
                    if ($updateShop->save()) {
                        Session::flash('message', 'Successfully created shop!');
                        return redirect('tradesman/stores')->with('status', 'Shop saved!');
                    }
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$shop = Shop::find($id);
		$todaySales		=  Order::selectRaw(' sum(total) as total')->where("shop_id",$id)->whereDate('created_at', Carbon::today())->first();
		$codTodaySales		=  Order::selectRaw(' sum(total) as total')->where("shop_id",$id)->whereDate('created_at', Carbon::today())->where("payment_mode","cod")->first();
		$onlineTodaySales		=  Order::selectRaw(' sum(total) as total')->where("shop_id",$id)->whereDate('created_at', Carbon::today())->where("payment_mode","online")->first();
        return view('tradesman.shops.show',compact("todaySales","codTodaySales","onlineTodaySales"));
                        
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
          $shop = Shop::find($id);
         // dd($shop);
        return view('tradesman.shops.edit')
                        ->with('shop', $shop);
                        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateRequest $request, $id) {
		
         $validated = $request->validated();
	
        $shop = Shop::find($id);

    //  $this->authorize('update', $shop);

        $filePath = "";
        if ($request->file('image')) {
            $filePath = $this->UserImageUpload($request->file('image'));
           
            $shop->image = $filePath;
        }
			$shop->name = $request->name;
            $shop->address = $request->address;
            $shop->zip = $request->zip;
            $shop->primary_phone = $request->primary_phone;
            $shop->secondary_phone = $request->secondary_phone;
            $shop->open_at = $request->open_at;
            $shop->close_at = $request->close_at;
            $shop->license_number = $request->license_number;
            $shop->save();
        return redirect('tradesman/stores')->with('success', 'Shop updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $shop = Shop::findOrFail($id);
        //$this->authorize('delete', $product);
        $shop->delete();
        return redirect('tradesman/stores')->with('warning', 'Shop deleted!');
    }

}
