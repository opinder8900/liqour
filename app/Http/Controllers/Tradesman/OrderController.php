<?php

namespace App\Http\Controllers\Tradesman;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Shop;

class OrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payments($type) {
        //echo($type);

//        $shops = Shop::with(['orders' => function($query) {
//                        $query
//                        ->where(['payment_mode' => 'cod'])
//                        ->orderBy('created_at', 'desc');
//                    }])
//                ->where('user_id', Auth::user()->id)
//                ->get();
        $shops = Shop::where('user_id', Auth::user()->id)
                ->get()->pluck('id');
        //dd($shops);
        $orders = Order::whereIn("shop_id", $shops)
                ->where('payment_mode',$type)
                ->orderBy('created_at', 'desc')
                ->paginate(15);
        //->get();
        //dd($orders);
        return view('tradesman.order.payments')->with('orders', $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $shop = Shop::where('shopkeeper_id', Auth::user()->id)->first();
        $order = Order::with('items')
                ->where(['id' => $id, "shop_id" => $shop->id])
                ->first();
        return view('shop.order.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view('shop.order.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function invoice($id) {
        $shop = Shop::where('shopkeeper_id', Auth::user()->id)->first();
        $order = Order::with('items')
                ->where(['id' => $id, "shop_id" => $shop->id])
                ->first();
        return view('shop.order.invoice')->with('order', $order);
    }

}
