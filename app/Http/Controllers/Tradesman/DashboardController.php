<?php

namespace App\Http\Controllers\Tradesman;

use Illuminate\Routing\Controller;

use Illuminate\Http\Request;
use App\User;
use App\Shop;
use App\Order;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index(){
		$user_id 		= Auth::user()->id;
		$shopIds  		= Shop::where("user_id",$user_id)->pluck("id")->toArray();
		
		$totalShops 	=  Shop::where("user_id",$user_id)->count();
		$totalOrders 	=  Order::whereIn("shop_id",$shopIds)->count();
		$todaySales		=  Order::selectRaw(' sum(total) as total')->whereIn("shop_id",$shopIds)->whereDate('created_at', Carbon::today())->first();
		
		
		$totalSales		= Order::selectRaw(' sum(total) as total')->whereIn("shop_id",$shopIds)->first();
		
		return view('tradesman.dashboard.index',compact("totalShops","totalOrders","totalSales","todaySales"));
	}
}
