<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\User;
use App\Shop;
use App\OrderItem;
use Auth;

class CustomerController extends Controller {

    public function index() {
		$customers = User::where("role","user")->orderBy('name')->paginate();
        return view('superadmin.customer.index',compact("customers"));
                        
    }
    
    public function getOrderDetails($id) {
		$order_details = OrderItem::where("order_id",$id)->paginate();
        return view('superadmin.customer.order_details',compact("order_details"));
                        
    }
    
     public function getCustomerOrders($id) {
        $orders = Order::where("user_id",$id)->get();
       
        return view('superadmin.customer.orders',compact("orders"));
                                          
    }
     public function reports() {
        
       return view('superadmin.customer.report');
    }

}

