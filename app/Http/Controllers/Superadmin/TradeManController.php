<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\User;

class TradeManController extends Controller {

    use ImageUploadTrait;

    public function index() {
		
		$activeTradesman = User::where("status",1)->where("role","tradesman")->get();
		$inactiveTradesman = User::where("status",0)->where("role","tradesman")->get();
		//$activeTradesman = User::where("role","tradesman")->get();

        return view('superadmin.trademan.index',compact('activeTradesman','inactiveTradesman'));
    }

    public function create() {
      
        return view('superadmin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreCategoryRequest $request) {
        $validated = $request->validated();
        //$shop = Shop::where("shopkeeper_id", Auth::user()->id)->first();
        $filePath = "";
        if ($request->file('image')) {
            $filePath = $this->UserImageUpload($request->file('image'));
        }
        //dd($filePath);
        //die;
        $category = new Category;
        $category->category_id = $request->category_id;
        $category->user_id = Auth::user()->id;
        //$category->shop()->associate($shop->id);
        $category->slug = $request->slug;
        $category->name = $request->name;
        $category->status = $request->status;
        $category->image = $filePath;
        $category->description = $request->description;
        $category->save();

        // redirect
        Session::flash('message', 'Successfully created product!');
        return redirect('superadmin/categories')->with('success', 'Category created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
       return view('superadmin.trademan.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $categories = Category::where('category_id', null)->get();
        $category = Category::findOrFail($id);
        return view('superadmin.category.edit', compact('categories'))->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id) {
        $validated = $request->validated();
        $category = Category::find($id);

        if ($request->file('image')) {
            $filePath = $this->UserImageUpload($request->file('image'));
            $category->image = $filePath;
        }

        $category->slug = $request->slug;
        $category->name = $request->name;
        $category->status = $request->status;

        $category->description = $request->description;
        $category->save();
        return redirect('superadmin/categories')->with('success', 'Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = Category::findOrFail($id);

        $row->delete();

        return redirect('superadmin/categories')->with('warning', 'Category deleted!');
    }
     public function toggleStatus($id) {

        $row = User::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok','trademan'=>true]);
    }
    
     public function shopToggleStatus($id) {

        $row = User::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }
     public function getShopsByTrademan($id) {
	$shops = User::with("shops",'shops.shopKeeper')->where("id",$id)->first();
		// $shops = Shop::with("user")->where("user_id",$id)->get();
		
         return view('superadmin.trademan.show',compact("shops"));
    }

     public function destroyShops($id) {
		$row = Shop::findOrFail($id);
        $row->delete();
		
        return  Redirect::back()->withErrors(['message', 'Shop Deleted successfully']);
    }

}

