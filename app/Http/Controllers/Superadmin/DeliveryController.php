<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Business;
use App\DeliveryProfile;
use App\Shop;
use App\State;
use App\City;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\BusinessRequest;
use App\Http\Requests\AddDeliveryRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Validator;
use Redirect;

class DeliveryController extends Controller {

    use ImageUploadTrait;

    public function index() {
       $user = User::with("delivery_profile")->where("role", "delivery")->paginate(10);
       
        return view('superadmin.delivery.index',compact("user"));
    }

    public function create() {
        $states = State::all();
        $cities = City::all();
        return view('superadmin.delivery.create',compact("states","cities"));
    }

	 public function show(Request $request,$id) {
       
        return view('superadmin.delivery.show');
    }
    
	 public function statusChanged($id) {

        $row = User::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(AddDeliveryRequest $request) {
        $input['email'] = $request->input("email");
        $input['phone'] = $request->input("primary_phone");
        $rules = array('email' => 'unique:users,email',"phone"=>'unique:users,phone');
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        } else {
			$user = new User;
			$user->password = Hash::make($request->password);
			$user->name = $request->name;
			$user->email = $request->email;
			$user->role = "delivery";
			$user->registered_for = 'delivery';
			$user->phone = $request->primary_phone;
			if ($user->save()) {
				$delivery = new DeliveryProfile;
				$delivery->user_id = $user->id;
				$delivery->address = $request->address;
				$delivery->city = $user->city;
				$delivery->state = $user->state;
				$delivery->zipcode = $user->zipcode;
				$licensePath = "";
				if ($request->file('license_photo')) {
					$licensePath = $this->UserImageUpload($request->file('license_photo'));
				}
				$rcPath = "";
				if ($request->file('rc_photo')) {
					$rcPath = $this->UserImageUpload($request->file('rc_photo'));
				}
				$adhaarPath = "";
				if ($request->file('adhaar_photo')) {
					$adhaarPath = $this->UserImageUpload($request->file('adhaar_photo'));
				}
				$panPath = "";
				if ($request->file('pan_photo')) {
					$panPath = $this->UserImageUpload($request->file('pan_photo'));
				}
				$delivery->rc_photo = $rcPath;
				$delivery->license_photo = $licensePath;
				$delivery->adhaar_photo = $adhaarPath;
				$delivery->pan_photo = $panPath;
				$delivery->in_time = date("H:i:s", strtotime($request->in_time));
				$delivery->out_time = date("H:i:s", strtotime($request->out_time));
				$delivery->security_deposit = $request->security_deposit;
				if($delivery->save()){
					 Session::flash('message', 'Successfully created Delivery Boy!');
                     return redirect('superadmin/delivery')->with('status', 'Successfully created Delivery Boy!');
				}
			}
		}
			
			
			
			
    }
    /**
     * update status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id) {

        $row = Business::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = Business::findOrFail($id);
        if(!empty($row->image)){
            Storage::disk('public')->delete($row->image);
        }
        $row->delete();

        return redirect('superadmin/business')->with('warning', 'Business deleted!');
    }

    public function details(Request $request,$id) {
       
        return view('superadmin.delivery.details');
    }
    
    public function timings(Request $request,$id) {
       
        return view('superadmin.delivery.timings');
    }

}

