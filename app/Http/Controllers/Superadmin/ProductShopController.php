<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Business;
use App\Category;
use App\Brand;
use App\Product;
use App\ShopProduct;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\ShopProductRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;

class ProductShopController extends Controller {

    use ImageUploadTrait;

    public function index($shop=null) {
        $shop=Shop::findOrFail($shop);
        $shop_products=ShopProduct::where('shop_id',$shop->id)->with('product')->paginate();
        return view('superadmin.shop.product.index',compact('shop','shop_products'));
    }
    
    public function editPrice(Request $request) {
      $product = ShopProduct::findOrFail($request->input("id"));
      $product->price = $request->input("newPrice");
      if($product->save()){
		  return response()->json(['status'=>'ok']);
	  }else{
	  }
    }
    /**
    * create function.
    *
    * This function adds products to the shop
    *
    * @author Ammy T
    * @param  params
    * @return void 
    */
    function create($shop=null) {
        $shop=Shop::findOrFail($shop);
        $categories=[];
        $brands=[];
        $products=[];

        $categories = Category::where('business_id',$shop->business_id)->where('status',1)->pluck('name','id')->toArray();
        $first_category=array_key_first($categories);
        if(!empty($first_category)){
            $brands = Brand::where('category_id',$first_category)->where('status',1)->pluck('name','id')->toArray();
            $first_brand=array_key_first($brands);
        }
        if(!empty($first_brand)){
            $shopProducts=ShopProduct::where('shop_id',$shop->id)->pluck('product_id')->toArray();
            $products = Product::where('brand_id',$first_brand)->whereNotIn('id',$shopProducts)->where('status',1)->get();
        }

        return view('superadmin.shop.product.create',compact('categories','brands','products','shop'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ShopProductRequest $request) {
        $validated = $request->validated();
        $shop=Shop::findOrFail($request->shop);
        foreach($request->product as $product){
            if(!$product['selected']) continue;
            $shopProduct = ShopProduct::firstOrNew(['product_id'=>$product['product_id'],'shop_id'=>$shop->id]);
            $shopProduct->price=$product['price'];
            $shopProduct->status=1;
            $shopProduct->save();
        }
        Session::flash('message', 'Successfully added product to shop!');
        return redirect('superadmin/trademan/shops/'.$shop->user_id)->with('success', 'Product added to shop.');
    }
    /**
     * update status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id) {

        $row = ShopProduct::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = ShopProduct::findOrFail($id);
        $row->delete();
        return redirect('superadmin/shops/products/'.$row->shop_id)->with('warning', 'Product deleted!');
    }
}

