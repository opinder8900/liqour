<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Business;
use App\Brand;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\BrandRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;

class BrandController extends Controller {

    use ImageUploadTrait;

    public function index($id=null) {
        $brands = Brand::with('category')->get()->groupBy('category.name');
        $businesses = Business::where('status',1)->pluck('name','id')->toArray();
        $categories=[];
        $first_business=array_key_first($businesses);
        
        $brand=new Brand;
        if(!empty($id)){
            $brand=Brand::where('id',$id)->firstOrFail();
            $first_business=$brand->business_id;
        }
        if(!empty($first_business)){
            $categories = Category::where('status',1)->where('business_id',$first_business)->pluck('name','id');
        }
        return view('superadmin.brands.index',compact('brands','businesses','categories','brand'));
    }
    public function getByCategory($category_id=null){
        // Just makign sure correct category id
        $business = Category::findOrFail($category_id);
        $brands=Brand::where('status',1)->where('category_id',$category_id)->select('name','id')->get()->toArray();
        return response()->json($brands);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BrandRequest $request) {
        $validated = $request->validated();
        $brand = new Brand;

        if(!empty($request->id)){
            $brand=Brand::find($request->id);
        }
        $brand->business_id = $request->business_id;
        $brand->category_id = $request->category_id;
        $brand->slug = $request->slug;
        $brand->name = $request->name;
        if(empty($brand->id)){
            $brand->status=0;
        }
        if(!empty($request->status)){
            $brand->status=$request->status;
        }
        $brand->save();

        // redirect
        Session::flash('message', 'Successfully created brand!');
        return redirect('superadmin/brands')->with('success', 'Brand saved.');
    }
    /**
     * update status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id) {

        $row = Brand::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = Brand::findOrFail($id);
        $row->delete();

        return redirect('superadmin/brands')->with('warning', 'Brand deleted!');
    }

}
