<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Business;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\StoreCategoryRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;

class CategoryController extends Controller {

    use ImageUploadTrait;

    public function index($id=null) {
        $categories = Category::orderBy('name')->paginate();
        $totalCategories = Category::count();
        $businesses = Business::all()->pluck('name','id');
        $category=new Category;
        if(!empty($id)){
            $category=Category::where('id',$id)->firstOrFail();
        }
        return view('superadmin.category.index',compact('categories','businesses','category','totalCategories'));
    }
    public function getByBusiness($business_id=null){
        // Just makign sure correct businees id
        $business = Business::findOrFail($business_id);
        $categories=Category::where('business_id',$business_id)->where("status",1)->select('name','id')->get()->toArray();
        return response()->json($categories);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreCategoryRequest $request) {
        $validated = $request->validated();
        $category = new Category;

        if(!empty($request->id)){
            $category=Category::find($request->id);
        }
        $category->business_id = $request->business_id;
        $category->slug = $request->slug;
        $category->name = $request->name;
        if(empty($category->id)){
            $category->status=0;
        }
        if(!empty($request->status)){
            $category->status=$request->status;
        }
        $category->save();

        // redirect
        Session::flash('message', 'Successfully created category!');
        return redirect('superadmin/categories')->with('success', 'Category saved.');
    }
    /**
     * update status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id) {

        $row = Category::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = Category::findOrFail($id);
        $row->delete();

        return redirect('superadmin/categories')->with('warning', 'Category deleted!');
    }

}
