<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Business;
use App\Brand;
use App\Product;
use App\Shop;
use App\ShopProduct;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\StoreProductRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller {

    use ImageUploadTrait;

    public function index($id=null) {
        $products = Product::paginate();
        $businesses = Business::where('status',1)->pluck('name','id')->toArray();
        $categories=[];
        $brands=[];
        $first_business=array_key_first($businesses);
        
        $product=new Product;
        if(!empty($id)){
            $product=Product::where('id',$id)->firstOrFail();
            $first_business=$product->business_id;
            $first_category=$product->category_id;
        }
        if(!empty($first_business)){
            $categories = Category::where('status',1)->where('business_id',$first_business)->pluck('name','id')->toArray();
            if(empty($first_category)){
                $first_category=array_key_first($categories);
            }
        }
        if(!empty($first_category)){
            $brands = Brand::where('status',1)->where('category_id',$first_category)->pluck('name','id');
        }
        return view('superadmin.product.index',compact('products','businesses','categories','brands','product'));
    }
    public function getByBrand($brand_id=null,$shop_id=null){
        // Just makign sure correct brand id
        $brand = Brand::findOrFail($brand_id);
        $products=Product::where('status',1)->where('brand_id',$brand_id);
      
        if(!empty($shop_id)){
            $shopProducts=ShopProduct::where('shop_id',$shop_id)->pluck('product_id')->toArray();
            $products=$products->whereNotIn('id',$shopProducts);
        }
        $products=$products->select('name','id','size','size_label')->get()->toArray();
        return response()->json($products);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreProductRequest $request) {
        $validated = $request->validated();
        $filePath = "";
        
        $product = new Product;

        if(!empty($request->id)){
            $product=Product::find($request->id);
        }
        if ($request->hasFile('image')) {
            $filePath = $this->UserImageUpload($request->file('image'));
        }
        $product->business_id = $request->business_id;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->slug = $request->slug;
        $product->name = $request->name;
        $product->size = $request->size;
        $product->size_label = $request->size_label;
        if(empty($product->id)){
            $product->status=0;
            $product->image="";
        }
        if(!empty($request->status)){
            $product->status=$request->status;
        }

        if(!empty($filePath)){
            $product->image = $filePath;
            if(!empty($product->image)){
                Storage::disk('public')->delete($product->image);
            }
        }
        $product->save();

        // redirect
        Session::flash('message', 'Successfully created product!');
        return redirect('superadmin/products')->with('success', 'Product saved.');
    }
    /**
     * update status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id) {

        $row = Product::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = Product::findOrFail($id);
        $row->delete();

        return redirect('superadmin/products')->with('warning', 'Product deleted!');
    }

}
