<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Business;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Traits\ImageUploadTrait;
use App\Http\Requests\BusinessRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Storage;

class BusinessController extends Controller {

    use ImageUploadTrait;

    public function index($id=null) {
        $businesses = Business::paginate();
        $business=new Business;
        if(!empty($id)){
            $business=Business::where('id',$id)->firstOrFail();
        }
        return view('superadmin.business.index',compact('businesses','business'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BusinessRequest $request) {
        $validated = $request->validated();
        $filePath = "";
        if ($request->hasFile('image')) {
            $filePath = $this->UserImageUpload($request->file('image'));
        }
        $business = new Business;
        if(!empty($request->id)){
            $business=Business::find($request->id);
        }
        $business->slug = $request->slug;
        $business->name = $request->name;
        if(empty($business->id)){
            $business->status=0;
            $business->image="";
        }
        if(!empty($request->status)){
            $business->status=$request->status;
        }
        if(!empty($filePath)){
            $business->image = $filePath;
            if(!empty($business->image)){
                Storage::disk('public')->delete($business->image);
            }
        }
        $business->save();

        // redirect
        Session::flash('message', 'Successfully created business!');
        return redirect('superadmin/business')->with('success', 'Business saved.');
    }
    /**
     * update status the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id) {

        $row = Business::findOrFail($id);
        $row->status=!$row->status;
        $row->save();
        return response()->json(['status'=>'ok']);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $row = Business::findOrFail($id);
        if(!empty($row->image)){
            Storage::disk('public')->delete($row->image);
        }
        $row->delete();

        return redirect('superadmin/business')->with('warning', 'Business deleted!');
    }

}
