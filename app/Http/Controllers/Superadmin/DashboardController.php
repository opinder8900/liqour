<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\User;
use App\Shop;
use Auth;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller {

    public function index() {
	

        $totalProducts 			= Product::count();
        $totalTrdesman 			= User::where("role","tradesman")->count();
        $totalShops 			= Shop::count();
        $totalCustomers 		= User::where("role","user")->count();
        $newShops 				= Shop::with("shopKeeper")->orderBy('id', 'desc')->take(5)->get();

        $newCustomers			= User::where("role","user")->orderBy('id', 'desc')->take(5)->get();
       
        $totalOrders 			= 0;
        return view('superadmin.dashboard.index',compact("totalProducts","totalTrdesman","totalShops","totalCustomers","newShops","newCustomers"));
                        
    }
    
     //~ public function customers() {
        
        //~ return view('superadmin.customer.customer');
                                          
    //~ }
     public function reports(Request $request) {
		// $reports = Order::selectRaw('shop_id, sum(total) as total')->with("shop.user")->groupBy("shop_id")->get();
		$search = $request->input('search');
		$reports =Order::select(DB::raw('sum(orders.total) as total,shop.user_id as user_id, Trademan.name as trademan_name'))->leftjoin('shops as shop','shop.id','orders.shop_id')->leftjoin('users as Trademan','shop.user_id','Trademan.id')->groupBy('user_id');
		if($search == "today"){
			 $reports->whereDate('orders.created_at', Carbon::today());
		}else if($search == "week"){
			$reports->whereDate('orders.created_at',">", Carbon::now()->subDays(7));
		}else{
			$reports->whereDate('orders.created_at',">", Carbon::now()->subDays(30));
		}
		$reports =$reports->get();
		
        return view('superadmin.customer.report',compact("reports"));
    }

}
