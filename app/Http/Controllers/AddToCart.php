<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Shop;
use App\State;
use App\City;
use App\Product;
use App\Category;
use App\Address;
use ShoppingCart;

class AddToCart extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $items = ShoppingCart::all();
        //dd($items);
        return view('cart.index')
                        ->with('cart_total', ShoppingCart::totalPrice())
                        ->with('items', $items);
    }

    public function addToCart(Request $request) {

        $product = Product::where([
                    'id' => $request->product_id
                ])
                ->with([
                    'category' => function($query) {
                        $query->select('id', 'name', 'slug');
                    },
                    'shop_product' => function($q )use($request) {
                        $q->where("shop_id", $request->shop_id)->where('status', true);
                    }
                ])
                ->first();

        $cartItem = [
            'id' => $product->id,
            'name' => $product->name,
            'image' => $product->image,
            'qty' => 1,
            'price' => $product->shop_product->price,
            'shop_id' => $request->shop_id,
            'size'=> $product->size,
            'size_label'=> $product->size_label
        ];
        $updatedQuantity = '';
        $result['notice'] = '';

        if (ShoppingCart::isEmpty()) {
            if(!$this->checkQuantityInCart($cartItem['id'],$cartItem['qty'])){
                $result['notice'] = 'Maximum quantity of 2 litre is exceeded.';

                $collection = ShoppingCart::search(['id' => $request->product_id]);
                $collection = $collection->toArray();
                $raw_id = array_key_first($collection);
                $updatedQuantity = $collection[$raw_id]["qty"];
            }else{
                $updatedQuantity = $this->AddNewItem($cartItem);    
            }
        } else if ($request->action == "increase") {
			if ($this->getCartShopId() != $request->shop_id) {
                ShoppingCart::destroy();
                $result['notice'] = 'Your cart items has been cleared because shop is changed.';
            }
            if(!$this->checkQuantityInCart($cartItem['id'],$cartItem['qty'])){
                $result['notice'] = 'Maximum quantity of 2 litre is exceeded.';
                $collection = ShoppingCart::search(['id' => $request->product_id]);
                $collection = $collection->toArray();
                $raw_id = array_key_first($collection);
                $updatedQuantity = $collection[$raw_id]["qty"];
            }else{
                $updatedQuantity = $this->AddNewItem($cartItem);    
            }
        } else {
            //for decrease quantity
            $collection = ShoppingCart::search(['id' => $request->product_id]);
            $collection = $collection->toArray();
            $raw_id = array_key_first($collection);
            $qty = $collection[$raw_id]["qty"];
            $qty = $qty - 1;
            //delete if quantity goes in minus
            if ($qty == 0) {
                ShoppingCart::remove($raw_id);
            } else {
                $updatedQuantity = $this->updateItem($raw_id, $qty);
            }
        }
        $result["qty"] = $updatedQuantity; // get __raw_id
        //add summary
        $result['count'] = ShoppingCart::count();
        $result['total_price'] = ShoppingCart::totalPrice();
        return response()->json($result);
    }

    private function AddNewItem($cart) {


        $item = ShoppingCart::add($cart["id"], $cart["name"], $cart["qty"], $cart["price"], [
                    "shop_id" => $cart["shop_id"],
                    "image" => $cart["image"],
                    "size" => $cart["size"],
                    "size_label" => $cart["size_label"],
        ]);
        $collection = $item->toArray();
        $qty = $collection["qty"];
        return $qty;
    }

    private function updateItem($rawId, $qty) {

        $item = ShoppingCart::update($rawId, $qty);
        $collection = $item->toArray();
        $qty = $collection["qty"];
        return $qty;
    }

    /**
     * Return shop_id of cart items
     * 
     * @return Number Shop ID
     */
    private function getCartShopId() {
        $cartItems = ShoppingCart::all();
        $cartShopId = null;
        foreach ($cartItems as $item) {
            $cartShopId = $item['shop_id'];
        }
        return $cartShopId;
    }
    
    private function checkQuantityInCart($rawId, $qty){
	   $cartItems = ShoppingCart::all();
       $totalQty=0;
       $maxQty=2000;
       foreach ($cartItems as $item) {
            $size=$item['size'];
            if($item['size_label']=='lt'){
               $size=$size*1000;
            }
            $totalQty +=($item['qty'] * $size );
            if($item['id']==$rawId){
                $totalQty +=($qty * $size );
            }
       }
       return $totalQty <= $maxQty;
    }

    public function deleteItem($id) {
        if (ShoppingCart::remove($id)) {

            return redirect('/cart')->with('success', 'Item has been removed');
        }
    }

    public function removeCart() {
        ShoppingCart::destroy();
        return redirect('/')->with('success', 'Your cart is empty');
    }

    public function checkout(Request $request) {
        //dd($request->session()->get('location.id'));
        $states = State::get();
        $cities = City::get();
        //check if user is not logged in 
        //if (Auth::check() && Auth::user()->role == 'user') {
        $items = ShoppingCart::all();
        $address = Address::findOrFail($request->session()->get('location.id'));
        return view('cart.checkout')
                        ->with('states', $states)
                        ->with('cities', $cities)
                        ->with('address', $address)
                        ->with('items', $items);
//        } else {
//            
//        }
    }

}
