<?php

namespace App\Http\Middleware;
//use Illuminate\Support\Facades\Auth;
use Closure;

class CheckRole {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role) {
        //die($request->user()->role);
        //Auth::check() && 
        if ($request->user()->role != $role) {
            abort(403, 'Unauthorized action.');
            //return redirect('tradesman/register')->with('warning', 'You are not autorized to view this');
        }

        return $next($request);
    }

}
