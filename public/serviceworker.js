var staticCacheName = "pwa-v" + new Date().getTime();
var filesToCache = [
    '/assets/js/app.js',
    '/assets/images/pwa/72.png',
    '/assets/images/pwa/96.png',
    '/assets/images/pwa/128.png',
    '/assets/images/pwa/144.png',
    '/assets/images/pwa/152.png',
    '/assets/images/pwa/192.png',
    '/assets/images/pwa/384.png',
    '/assets/images/pwa/512.png',
];

// Cache on install
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                return response || fetch(event.request);
            })
            .catch(() => {
                return caches.match('offline');
            })
    )
});
