function callAjax(url,params,requestType){
	return $.ajax({
        url: url,
        type: requestType,
        data: params,
        beforeSend: function(){
          $("#loaderOuter").show();
        },  
        success: function(data, textStatus, jqXHR) {  
            $("#loaderOuter").hide();  
           
         
        },
        error: function(jqXHR, textStatus, errorThrown) {
			$("#loaderOuter").hide();
            if (typeof errorCallBackFunction === "function") {
                errorCallBackFunction(errorThrown);
            }

        },
        complete: function(jqXHR, textStatus) {
            if (typeof completeCallbackFunction === "function") {
                completeCallbackFunction();
            }
        }
	});
}
