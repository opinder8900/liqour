function beforeCategoryDelete(name) {
    if (confirm('Are you sure you want to delete "' + name + '"?')) {
        return ture
    }
    return false;
}
function beforeProductDelete(name) {
    if (confirm('Are you sure you want to delete "' + name + '"?')) {
        return ture
    }
    return false;
}
function slugify(text) {
  return text
    .toString()                     // Cast to string
    .toLowerCase()                  // Convert the string to lowercase letters
    .normalize('NFD')       // The normalize() method returns the Unicode Normalization Form of a given string.
    .trim()                         // Remove whitespace from both sides of a string
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-');        // Replace multiple - with single -
}
function replaceAsSlug(valOfThis,inValOfThat){
  let value=$('#'+valOfThis).val();
  value=slugify(value);
  $('#'+inValOfThat).val(value);
}
$(function(){
	$(document).on("click",".edit-product-price",function(){
		
		var oldPrice = $(this).parent().siblings(".price-edit").html();
		var id = $(this).attr("data-id");
		var inputHtml = "<input type='text' name='price' value='"+oldPrice+"'>";
		$(this).parent().siblings(".price-edit").html(inputHtml);
		var buttonsHtml = "<a href='javascript:void(0)' class='save-product-price' data-id = '"+id+"'> Save </a> <a href='javascript:void(0)' class='cancel-product-price' data-oldPrice = '"+oldPrice+"' data-id = '"+id+"'> Cancel </a>"
		$(this).replaceWith(buttonsHtml);
		
		
	});
		$(document).on("click",".cancel-product-price",function(){
			var oldPrice  = $(this).attr("data-oldPrice");
			var id  = $(this).attr("data-id");
			$(this).parent().siblings(".price-edit").html(oldPrice);
			var buttonsHtml = "<a href='javascript:void(0)' class='edit-product-price' data-id = '"+id+"'> Edit </a>";
			$(this).replaceWith(buttonsHtml);
			$(".save-product-price").remove();
		
		
		});
		$(document).on("click",".save-product-price",function(){
			var newPrice  = $('input[name="price"]').val();
			var id  = $(this).attr("data-id");
			var buttonsHtml = "<a href='javascript:void(0)' class='edit-product-price' data-id = '"+id+"'> Edit </a>";
			if(!$.isNumeric(newPrice)){
				alert("Please enter numeric value");
				return false;
			}
			var save = $(this);
			$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: CONFIG.site_url + '/superadmin/shops/products/edit',
            type: "POST",
            data: {"newPrice":newPrice,"id":id},
            dataType: 'json',
            success: function (response) {
               if(response.status == "ok"){
				   save.parent().siblings(".price-edit").html(newPrice);
				    save.replaceWith(buttonsHtml);
				    $(".cancel-product-price").remove();
				    
			   }
              
				},
				error: function () {
            }
			});
			//~ var buttonsHtml = "<a href='javascript:void(0)' class='edit-product-price' data-id = '"+id+"'> Edit </a>";
			//~ $(this).parent().siblings(".price-edit").html(oldPrice);
			//~ var buttonsHtml = "<a href='javascript:void(0)' class='edit-product-price'> Edit </a>";
			//~ $(this).replaceWith(buttonsHtml);
			//~ $(".save-product-price").remove();
		
		
		});
  $('.switch input[type="checkbox"]').change(function(){
    var that=$(this);
    $.ajax({url: that.data('href'),success: function(response){
		if(response.status == "ok" && response.trademan){
			location.reload();
		}
      // Do something here 
    }})
  });
  $('.brand_business,.product_business,.product_category').change(function(){
    var that=$(this);
    var targetElem=$('.brand_category');
    if(that.hasClass('brand_business')){
      targetElem=$('.brand_category');
    }else if(that.hasClass('product_business')){
      targetElem=$('.product_category');
    }else if(that.hasClass('product_category')){
      targetElem=$('.product_brand');
    }
    let emptyOpt=targetElem.find('option[value=""]').clone();
    targetElem.html('<option>Loading...</option>');
    
    let targetHtml=emptyOpt.get(0).outerHTML;
    if(that.val()){
      $.ajax({url: that.data('target-href')+'/'+that.val(),success: function(resources){
        $.each(resources,function(index,resource){
          targetHtml+='<option value="'+resource.id+'">'+resource.name+'</option>';
        })
        targetElem.html(targetHtml).trigger('change');
      }})
    }else{
      targetElem.html(targetHtml).trigger('change');
    }
  });
})

$( '.navbar-nav a' ).on( 'click', function () {
	$( '.navbar-nav' ).find( 'li.active' ).removeClass( 'active' );
	$( this ).parent( 'li' ).addClass( 'active' );
});