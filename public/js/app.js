//public/js
function getLocation() {
    console.log('getLatLng function called.')

    var latitude = jQuery.cookie('latitude');
    var longitude = jQuery.cookie('longitude');
    if (!latitude && !longitude) {
        if (navigator.geolocation) {
            try {
                var options = {enableHighAccuracy: true, timeout: 5000, maximumAge: 0};
                function successNavigation(pos) {
                    console.log('User has ALLOWED the location sharing');
                    var crd = pos.coords;
//                    jQuery.cookie('latitude', crd.latitude);
//                    jQuery.cookie('longitude', crd.longitude);
                    setLocation(crd.latitude, crd.longitude);
                }
                function errorNavigation(err) {
                    console.log('Navigation Error in common service(' + err.code + '): ' + err.message);
                    setLocation(CONFIG.default_location.latitude, CONFIG.default_location.longitude);
                }

                navigator.geolocation.getCurrentPosition(successNavigation, errorNavigation, options);
                /*
                 * Add by Jatinder Singh to resolve multiple request for address api
                 *
                 */
                navigator.geolocation.watchPosition(function (pos) {
                    console.log("started tracking your location");
                    console.log(pos)
                    var crd = pos.coords;
//                    jQuery.cookie('latitude', crd.latitude);
//                    jQuery.cookie('longitude', crd.longitude);
                    setLocation(crd.latitude, crd.longitude);
                },
                        function (error) {
                            if (error.code == error.PERMISSION_DENIED) {
                                console.log("you denied location sharing :-(");
                            }
                            console.log('Line#394: setting default location', error)
                            setLocation(CONFIG.default_location.latitude, CONFIG.default_location.longitude);

                        });
                /*********************************************************/
            } catch (err) {
                console.log("Line#402: Exception in geolocation:", err)
                setLocation(CONFIG.default_location.latitude, CONFIG.default_location.longitude);

            }

        } else {
            console.log("Line#430: Geolocation is not supported by this browser.");
            setLocation(CONFIG.default_location.latitude, CONFIG.default_location.longitude);


        }
    } else {
        console.log('location is already set')
        setLocation(latitude, longitude);
    }

}
//getLocation();
function initLocation() {
    console.log('initLocation function called.')

    var latitude = jQuery.cookie('latitude');
    var longitude = jQuery.cookie('longitude');
    if (latitude && longitude) {
        setLocation(latitude, longitude);
    }
}
function setLocation(latitude, longitude) {
    jQuery.cookie('latitude', latitude);
    jQuery.cookie('longitude', longitude);
//    $.ajax({
//        headers: {
//            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//        },
//        url: CONFIG.site_url + '/location/set',
//        type: 'post',
//        data: {
//            'latitude': latitude,
//            'longitude': longitude
//        },
//        success: function (response) {
//            console.log(response)
//            $('#location-based-content-ajx').html(response);
//        },
//        error: function () {
//        }
//    });
}

function beforeAddressDelete() {
    if (confirm('Are you sure you want to delete your address?')) {
        return true;
    }
    return false;
}
$(document).ready(function () {
    initLocation();
    $('a#locate-me-gps').click(function () {
        getLocation();
    });


    $('form.add-to-cart-form').submit(function (e) {
        var form = $(this);
        if ((form.find('input.number-pro-btn').val() == ''
                || form.find('input.number-pro-btn').val() == '0')
                && form.find('input[name=action]').val() == 'decrease'
                ) {
            return false;
        }
        var url = form.attr('action');
        e.preventDefault();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: CONFIG.site_url + '/add-to-cart',
            type: "POST",
            data: form.serialize(),
            dataType: 'json',
            success: function (response) {
                console.log(response)
                form.find("input[name=quantity]").val(response.qty);
                if (response.count) {
                    $('#cart-footer #cart-items-count').text(response.count);
                    $("#card-number").html(response.count);
                    $('#cart-footer #cart-total').text(response.total_price);
                    $("#cart-footer").removeClass('d-none');
                   // location.reload();
                } else {
                    $("#cart-footer").addClass('d-none');
                }
                if (response.notice) {
                     $("#cartModal").modal("show");
                     $("#cart-notice").html(response.notice);
                }

            },
            error: function () {
            }
        });
        return false;
    })

    //address
    $('.address-type').click(function () {
        if ($(this).attr('data-value') == 'other') {
            $('#address-form input[name=type]').val('');
            $('#address-form #other-part').removeClass('d-none');
        } else {
            $('#address-form input[name=type]').val($(this).attr('data-value'));
            $('#address-form #other-part').addClass('d-none');
        }

    });
    $('#address-form #other-part button[name=close]').click(function () {
        $('#other-part').addClass('d-none')

    });

    $('form[name=address-form-ajax]').submit(function (e) {
        var form = $(this);
        var url = form.attr('action');
        //e.preventDefault();
        $.ajax({
            url: url,
            type: "POST",
            data: form.serialize(),
            dataType: 'json',
            success: function (response) {
                console.log(response)
                window.location.reload();
            },
            error: function (ts) {

                if (ts.status != 200) {
                    $('form[name=address-form-ajax] .address-error').removeClass('d-none')
                }
            },
            beforeSend: function () {
                $('form[name=address-form-ajax] button[type=submit]').prop("disabled", true);
            },
            complete: function () {
                $('form[name=address-form-ajax] button[type=submit]').prop("disabled", false);
            }
        });
        return false;
    });
    $('.set-location-home-close').click(function () {
        $('.set-location-home-outer').addClass('d-none');
    })
    $('#change-delivery-address').click(function () {
        $('.set-location-home-outer').removeClass('d-none');
    });


})
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        zoomControl: true,
        //center: {lat: 30.704649, lng: 76.717873},
        //disableDefaultUI: true,

        fullscreenControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });
    //var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    var types = document.getElementById('type-selector');
    var strictBounds = document.getElementById('strict-bounds-selector');

    //map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

    var autocomplete = new google.maps.places.Autocomplete(input, {
        componentRestrictions: {country: "in"}
    });

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    //autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
//    autocomplete.setFields(
//            ['address_components', 'geometry', 'icon', 'name']);

    //var infowindow = new google.maps.InfoWindow();
//    var infowindowContent = document.getElementById('infowindow-content');
//    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function () {
        $('#add-address-interface-wrapper').removeClass('d-none');
        $('#saved-addresses-list').addClass('d-none');
        //infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(2);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

//        infowindowContent.children['place-icon'].src = place.icon;
//        infowindowContent.children['place-name'].textContent = place.name;
//        infowindowContent.children['place-address'].textContent = address;
        //infowindow.open(map, marker);
    });

    google.maps.event.addListener(map, 'center_changed', function () {
        $('#address-form input[name=latitude]').val(map.getCenter().lat());
        $('#address-form input[name=longitude]').val(map.getCenter().lng());
    });

    google.maps.event.addListener(map, 'idle', function () {
        try {
            getAddress(map.getCenter().lat(), map.getCenter().lng())
        } catch (e) {

        }
    });

    // Sets a listener on a radio button to change the filter type on Places
    // Autocomplete.
    function setupClickListener(id, types) {
        var radioButton = document.getElementById(id);
        radioButton.addEventListener('click', function () {
            autocomplete.setTypes(types);
        });
    }

    //setupClickListener('changetype-all', []);
//    setupClickListener('changetype-address', ['address']);
//    setupClickListener('changetype-establishment', ['establishment']);
//    setupClickListener('changetype-geocode', ['geocode']);

//    document.getElementById('use-strict-bounds')
//            .addEventListener('click', function () {
//                console.log('Checkbox clicked! New state=' + this.checked);
//                autocomplete.setOptions({strictBounds: this.checked});
//            });

    function getAddress(lat, lng) {
        jQuery.ajax({
            url: 'https://maps.googleapis.com/maps/api/geocode/json?key=' + CONFIG.google_map_api_key + '&latlng=' + lat + ',' + lng + '&sensor=true',
            type: 'get',
            success: function (data) {
                console.log(data)
                if (data.status == 'OK') {
                    var formatted_address = getAddressComponent(data.results, 'street_address', 'formatted_address');
                    if (formatted_address == null) {
                        formatted_address = getAddressComponent(data.results, 'route', 'formatted_address');
                    }
                    if (formatted_address == null) {
                        formatted_address = getAddressComponent(data.results, 'sublocality', 'formatted_address');
                    }
                    //var postal_code = getAddressComponent(data.results, 'postal_code', 'long_name');
                    //$('#address-form input[name=postal_code]').val(postal_code);
                    $('#address-form input[name=formatted_address]').val(formatted_address);
                } else {
                    console.warn('error in api')
                }
            },
            error: function () {
            }
        });

    }
    //var city = getAddressComponent(data.results, 'locality', 'long_name');
    //var city = getAddressComponent(data.results, 'street_address', 'formatted_address');
    //https://stackoverflow.com/questions/8082405/parsing-address-components-in-google-maps-upon-autocomplete-select
    function getAddressComponent(results, componentType, property) {
        var return_value = null;
        jQuery.each(results, function (index, result_item) {
            jQuery.each(result_item.types, function (index, type) {
                console.log(type, result_item.formatted_address)
                if (type == componentType) {
                    console.log(componentType, ' found')
                    console.log(result_item[property])
                    return_value = result_item[property];
                    return false;
                }

            });
        });
        return return_value;
    }
}


function onPlaceChanged() {
    var place = autocomplete.getPlace();
    console.log(place);

    jQuery.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: CONFIG.site_url + '/location/set-place',
        type: 'post',
        data: {
            'address_components': place.address_components,
            'formatted_address': place.formatted_address,
            'latitude': place.geometry.location.lat(),
            'longitude': place.geometry.location.lng()
        },
        //dataType: 'json',
        success: function (response) {
            //console.log(response)
            $('#location-based-content-ajx').html(response);
        },
        error: function () {
        }
    });


    if (place.geometry) {
        console.log(place.geometry)
//        map.panTo(place.geometry.location);
//        map.setZoom(15);
//        search();
    } else {
        document.getElementById('place-auto-picker').placeholder = 'Enter address';
    }
}

