<?php

return [
    'name' => 'LaravelPWA',
    'manifest' => [
        'name' => env('liquorestore', 'My liquorestore'),
        'short_name' => 'PWA',
        'start_url' => '/login',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation'=> 'any',
        'status_bar'=> 'black',
        'icons' => [
            '72x72' => [
                'path' => 'assets/images/pwa/72.png',
                'purpose' => 'any'
            ],
            '96x96' => [
                'path' => 'assets/images/pwa/96.png',
                'purpose' => 'any'
            ],
            '128x128' => [
                'path' => 'assets/images/pwa/128.png',
                'purpose' => 'any'
            ],
            '144x144' => [
                'path' =>'assets/images/pwa/144.png',
                'purpose' => 'any'
            ],
            '152x152' => [
                'path' => 'assets/images/pwa/152.png',
                'purpose' => 'any'
            ],
            '192x192' => [
                'path' => 'assets/images/pwa/192.png',
                'purpose' => 'any'
            ],
            '384x384' => [
                'path' => 'assets/images/pwa/384.png',
                'purpose' => 'any'
            ],
            '512x512' => [
                'path' => 'assets/images/pwa/512.png',
                'purpose' => 'any'
            ],
        ],
        'splash' => [
            '640x1136' => '/images/icons/splash-640x1136.png',
            '750x1334' => '/images/icons/splash-750x1334.png',
            '828x1792' => '/images/icons/splash-828x1792.png',
            '1125x2436' => '/images/icons/splash-1125x2436.png',
            '1242x2208' => '/images/icons/splash-1242x2208.png',
            '1242x2688' => '/images/icons/splash-1242x2688.png',
            '1536x2048' => '/images/icons/splash-1536x2048.png',
            '1668x2224' => '/images/icons/splash-1668x2224.png',
            '1668x2388' => '/images/icons/splash-1668x2388.png',
            '2048x2732' => '/images/icons/splash-2048x2732.png',
        ],
        'custom' => []
    ]
];
