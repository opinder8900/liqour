<?php

use Illuminate\Support\Facades\Route;
use App\Address;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
//Route::group(['middleware' => 'auth'], function () {
//    
//});
//Route::get('/', 'HomeController@welcome');
//Route::get('/home', 'HomeController@welcome');
//
//Route::get('{city}/{business}/shops', 'HomeController@shops')->name('city-business-shops');

Route::get('/offline', function () {    
    return view('modules/laravelpwa/offline');
});


Route::group(['middleware' => ['auth', 'CheckRole:user']], function() {
    Route::get('/', 'HomeController@welcome');
    Route::get('/home', 'HomeController@welcome');
    
    Route::post('/address/save', 'AddressController@save')->name('add-address-ajax');
    Route::post('/address/all', 'AddressController@all')->name('all-addresses-ajax');
    Route::get('/address/set/{id}', 'AddressController@set')->name('set-address');

    View::composer(['*'], function($view) {
        if (Auth::check()) {
            $addresses = Address::with("city", 'state')
                    ->where('user_id', Auth::user()->id)
                    ->get();
            $view->with('addresses', $addresses);
        }
    });

    Route::get('/{business}/shops', 'HomeController@shops')->name('business-shops');

    Route::get('/{business}/{shop}/products', 'HomeController@products')
            ->name('business-shop-products');

    Route::post('/location/set-place', 'LocationController@setPlace');
    Route::post('/location/set', 'LocationController@set');
//Route::match(['get', 'post'], '/location/set', 'LocationController@set');
    Route::post('/add-to-cart', 'AddToCart@addToCart')->name('add-to-cart');
    Route::get('/cart', 'AddToCart@index')->name('cart-index');
    Route::get('/cart/remove', 'AddToCart@removeCart')->name('cart-remove');
    Route::get('/cart/remove/{id}', 'AddToCart@deleteItem')->name('delete-cart-item');



    Route::get('/checkout', 'AddToCart@checkout')->name('checkout');
    Route::post('/place-order', 'OrderController@placeOrder')->name('place-order');
    Route::get('/thank-you', 'OrderController@thankYou')->name('thank-you');
    Route::get('/my-profile', 'ProfileController@myProfile')->name('my-profile');
    Route::get('/profile/edit', 'ProfileController@edit')->name('show-edit-profile');
    Route::post('/profile/update', 'ProfileController@update')->name('update-profile');
    Route::get('/change-password', 'ProfileController@changePassword')->name('change-password');
    Route::post('/change-password', 'ProfileController@changePassword')->name('change-password');
    Route::get('/my-orders', 'ProfileController@myOrders')->name('my-orders');
    Route::get('/my-orders/{id}/cancel', 'OrderController@cancel')
            ->name('show-cancel-order');
    Route::post('/orders/{id}/update', 'OrderController@update')
            ->name('cancel-order');
    Route::resource('/address', 'AddressController');
});


//~ Route::get('/address', 'AddressController@index')->name('my-address');
//~ Route::get('/address/add', 'AddressController@create')->name('add-address');
//~ Route::post('/address/store', 'AddressController@store')->name('store-address');
//~ Route::post('/address/store', 'AddressController@store')->name('store-address');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//for super admin
Route::get('/superadmin/categories/by_business/{business}', 'Superadmin\CategoryController@getByBusiness');
Route::get('/superadmin/brands/by_category/{category}', 'Superadmin\BrandController@getByCategory');
Route::namespace('Superadmin')->middleware(['auth', 'CheckRole:superadmin'])->group(function() {
    Route::get('superadmin/dashboard', 'DashboardController@index');
    Route::get('superadmin/customer', 'CustomerController@index');
    Route::get('superadmin/customer/orders/{customer}', 'CustomerController@getCustomerOrders');
    Route::get('superadmin/customer/orders/details/{order}', 'CustomerController@getOrderDetails');
    Route::get('superadmin/report', 'DashboardController@reports')->name("superadmin-report");
   
    Route::match(['get'], '/superadmin/business', 'BusinessController@index');
    Route::match(['get'], '/superadmin/business/{business}', 'BusinessController@index');
    Route::match(['get'], '/superadmin/business/toggle_status/{business}', 'BusinessController@toggleStatus');
    Route::match(['post', 'put'], '/superadmin/business', 'BusinessController@store');
    Route::get('/superadmin/business/destroy/{business}', 'BusinessController@destroy');

    Route::match(['get'], '/superadmin/categories', 'CategoryController@index');
    Route::match(['get'], '/superadmin/categories/{category}', 'CategoryController@index');
    Route::match(['get'], '/superadmin/categories/toggle_status/{category}', 'CategoryController@toggleStatus');
    Route::match(['post', 'put'], '/superadmin/categories', 'CategoryController@store');
    Route::get('/superadmin/categories/destroy/{category}', 'CategoryController@destroy');

    Route::match(['get'], '/superadmin/brands', 'BrandController@index');
    Route::match(['get'], '/superadmin/brands/{brand}', 'BrandController@index');
    Route::match(['get'], '/superadmin/brands/toggle_status/{brand}', 'BrandController@toggleStatus');
    Route::match(['post', 'put'], '/superadmin/brands', 'BrandController@store');
    Route::get('/superadmin/brands/destroy/{brand}', 'BrandController@destroy');

    Route::get('/superadmin/products/by_brand/{brand}', 'ProductController@getByBrand');
    Route::get('/superadmin/products/by_brand/{brand}/{shop}', 'ProductController@getByBrand');
    Route::match(['get'], '/superadmin/products', 'ProductController@index');
    Route::match(['get'], '/superadmin/products/{product}', 'ProductController@index');
    Route::match(['get'], '/superadmin/products/toggle_status/{product}', 'ProductController@toggleStatus');
    Route::match(['post', 'put'], '/superadmin/products', 'ProductController@store');
    Route::get('/superadmin/products/destroy/{product}', 'ProductController@destroy');

    Route::match(['get'], '/superadmin/trademan', 'TradeManController@index');
    Route::match(['get'], '/superadmin/trademan/shops/{trademan}', 'TradeManController@getShopsByTrademan');
    Route::match(['get'], '/superadmin/trademan/toggle_status/{trademan}', 'TradeManController@toggleStatus');
    Route::match(['get'], '/superadmin/trademan/shop/toggle_status/{shop}', 'TradeManController@shopToggleStatus');
     Route::get('/superadmin/trademan/shops/delete/{shop}', 'TradeManController@destroyShops');
    Route::match(['get'], '/superadmin/shops/products/{shop}', 'ProductShopController@index');
    Route::match(['post'], '/superadmin/shops/products/edit/', 'ProductShopController@editPrice');

    Route::resource('/superadmin/trademan/shops', 'ShopController');

    Route::get('/superadmin/shops/products/{shop}', 'ProductShopController@index');
    Route::get('/superadmin/shops/products/add/{shop}', 'ProductShopController@create');
    Route::post('/superadmin/shops/products/{shop}', 'ProductShopController@store');
    Route::get('/superadmin/shops/products/toggle_status/{shop_product}', 'ProductShopController@toggleStatus');
    Route::get('/superadmin/shops/products/destroy/{shop_product}', 'ProductShopController@destroy');
   
    Route::get('/superadmin/delivery', 'DeliveryController@index');
    Route::get('/superadmin/delivery/create', 'DeliveryController@create');
    Route::post('/superadmin/delivery/store', 'DeliveryController@store');
    Route::get('/superadmin/delivery/{id}', 'DeliveryController@show');
    Route::get('/superadmin/delivery/details/{id}', 'DeliveryController@details');
    Route::get('/superadmin/delivery/timings/{id}', 'DeliveryController@timings');
    Route::get('/superadmin/delivery/toggle_status/{id}', 'DeliveryController@statusChanged');

});

Route::namespace('Tradesman')->group(function() {
    // Controllers Within The "App\Http\Controllers\Vendor" Namespace
    Route::match(['get'], '/tradesman/register', 'RegisterController@register')
            ->name('tradesman-create');

    Route::get('tradesman/dashboard', 'DashboardController@index')
            ->middleware(['auth', 'CheckRole:tradesman']);

    Route::get('tradesman/payments/{type}', 'OrderController@payments')
            ->name('cod-or-online-payments')
            ->middleware(['auth', 'CheckRole:tradesman']);

    Route::match(
                    ['post'], '/tradesman/store', 'RegisterController@store')
            ->name('tradesman-register');

    //Route::match(['get', 'post'], '/tradesman/login', 'LoginController@login');
    //Route::get('/vendor/products', 'ProductController@index');

    Route::resource('/tradesman/stores', 'ShopController')
            ->middleware(['auth', 'CheckRole:tradesman']);
});
Route::namespace('Shop')->group(function() {
    // Controllers Within The "App\Http\Controllers\Shop" Namespace


    Route::get('shop/dashboard', 'DashboardController@index')
            ->middleware(['auth', 'CheckRole:shop']);



    Route::resource('/shop/manage-products', 'ShopProductController')
            ->middleware(['auth', 'CheckRole:shop']);
    Route::get('shop/manage-products/toggle_status/{product}/{shop}', 'ShopProductController@changeStatus')
            ->middleware(['auth', 'CheckRole:shop']);

    Route::resource('/shop/orders', 'OrderController')
            ->middleware(['auth', 'CheckRole:shop']);

    Route::get('shop/orders/invoice/{id}', 'OrderController@invoice')
            ->name('view-order-invoice')
            ->middleware(['auth', 'CheckRole:shop']);


//    Route::resource('/shop/customers', 'CustomerController')
//            ->middleware(['auth', 'CheckRole:shop']);
//
//    Route::resource('/shop/invoices', 'InvoiceController')
//            ->middleware(['auth', 'CheckRole:shop']);
});

//Route::get('/test', function() {
//    print('TEST');
//});
