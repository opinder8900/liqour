<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

                [
                    'name' => 'superadmin',
                    'email' => 'kirtitakar28@gmail.com',
                    'role' => 'superadmin',
                    'registered_for' => 'supervision',
                    'status' => '1',
                    'phone'=>'7837001317',
                    'password' => Hash::make('123123123'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
         ]);
    }
}

