<?php

use Illuminate\Database\Seeder;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([

                [
                    'city_name' => 'Amritsar',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Barnala',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Bathinda',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Faridkot',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Fatehgarh Sahib',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Ferozepur',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Gurdaspur',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Hoshiarpur',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Jalandhar',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Kapurthala',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Ludhiana',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'city_name' => 'Mansa',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Moga',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Muktsar',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Nawanshahr',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Pathankot',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Patiala',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Rupnagar',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Sangrur',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'SAS Nagar',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
                [
                    'city_name' => 'Tarn Taran',
                    'city_code' => '2801',
                    'state_code' => '28',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                
              
            ]);
    }
}
