<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shop_id');
            $table->unsignedBigInteger('user_id');
            
            $table->unsignedBigInteger('shipping_address_id');
            $table->decimal('discount', 12, 2)->nullable();
            $table->decimal('tax', 12, 2)->nullable();
            $table->decimal('total', 12, 2);

            $table->ipAddress('ip');
            $table->enum('payment_mode', [
                        'cod',
                        'online'
                    ])
                    ->default('cod');
            $table->enum('status', [
                        'pending',
                        'cancelled',
                        'accepted',
                        'rejected',
                        'dispatched',
                        'outfordelivery',
                        'delivered'])
                    ->default('pending');
            
            $table->string('payment_gateway')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('transaction_status')->nullable();
            $table->dateTime('delivered_on')->nullable();
            $table->string('delivery_notes')->nullable();
            $table->foreign('shipping_address_id')->references('id')->on('addresses');
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('shop_id')->references('id')->on('shops');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('orders');
    }

}
