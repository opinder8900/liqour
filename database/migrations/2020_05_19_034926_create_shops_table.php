<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->string('slug');
            $table->string('address');
            $table->string('city_id');
            $table->string('state_code');
            $table->string('zip');
            $table->string('primary_phone');
            $table->string('secondary_phone')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('type')->enum('type', ['liquor', 'grocery','shopping'])->default('liquor');
            $table->time('open_at')->nullable();
            $table->time('close_at')->nullable();
            $table->string('icon')->nullable();
            $table->string('image')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('license_number')->nullable();
            $table->string('shopkeeper_id')->default(0);
            $table->boolean('status')->default(0);

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
