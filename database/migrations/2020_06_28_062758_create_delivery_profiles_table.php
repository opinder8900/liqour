<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('image')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('license_photo')->nullable();
            $table->string('rc_photo')->nullable();
            $table->string('adhaar_photo')->nullable();
            $table->string('pan_photo')->nullable();
			$table->time('in_time')->nullable();
            $table->time('out_time')->nullable();
            $table->string('security_deposit')->nullable();
			$table->string('account_name')->nullable();
			$table->string('account_phone')->nullable();
			$table->string('account_number')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_profiles');
    }
}
