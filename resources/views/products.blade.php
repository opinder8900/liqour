@extends('layouts.front-header')

@section('content')
@include('layouts.lower-tabs')
<div class="f-w inner-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
                <h2 class="f-w text-center"> Products </h2>
			</div>
		</div>
	</div>
</div>

<div class="f-w bread inner-bread">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <ul class="f-w">
                    <li> <a href="{{ url('/') }}"> Home </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <a href="{{route('business-shops',['business'=>$business])}}"> {{$business}} </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <p> {{$shop->name}} </p> </li>
                </ul>
            </div>
        </div>

    </div>
</div>

<section class="front-shops-page">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">

                <div class="detail-shop">
                    <div class="listing-text">
                        <div class="list-icon">
                            <i class="material-icons">store</i>
                            <h4> {{$shop->name}}</h4>
                        </div>
                        <!--p class="mb-0"> All kind of {{$business}} </p-->
                        <div class="list-icon-p p-r">
                            <i class="material-icons">access_time</i>
                            <p class="mb-2"> Timings:   {{ date('h:i a',(strtotime($shop->open_at)))}} – {{ date('h:i a',(strtotime($shop->close_at)))}}  </p>
                        </div>
                        <div class="list-icon-p p-r">
                            <i class="material-icons">location_city</i>
                            <p> {{$shop->address}}, {{$shop->city->name}} </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-4 mt-4">

                <div class="f-w search-common">
                    <form action="" method="GET">
                        <div class="front-search-out">
                            <i class="material-icons">search</i>
                            <input  class="f-w" placeholder="Search for items in store" type="text" value="{{ app('request')->input('search')}}" name="search" required/>
                            <button class="common-btn serch-front-btn" type="submit">Search</button>
                        </div>
                        <a href="{{ route('business-shop-products',['business' => $business,'shop' => $shop->slug])}}"><i class="material-icons">close</i> Clear <span> Search </span></a>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">

                <div class="details-tabs-outer">

                    <ul class="nav nav-tabs" role="tablist">
                        @php
                        $i = 0
                        @endphp
                        @foreach($groupByCategoryProducts as $category => $products)
                        <li class="nav-item">
                            <a class="nav-link {{$i == 0?'active':''}}" data-toggle="tab" href="#{{$category}}">{{$category}}</a>
                        </li>
                        @php
                        $i++
                        @endphp
                        @endforeach
                    </ul>

                    <div class="tab-content">
                        @php
                        $i = 0

                        @endphp

                        @foreach($groupByCategoryProducts as $category => $brands)
                        <div id="{{$category}}" class="container tab-pane {{$i == 0?'active':'fade'}}"><br>
                            <div class="row">
                                @foreach($brands as $brandName => $products)
                                <div class="f-w front-brant-n">
                                    <h4>{{$brandName}}</h4>
                                </div>
                                @foreach($products as $product)
                                <div class="f-w mb-4">
                                    <div class="listing-outer details-outer">
                                        <div class="listing-img">
                                            @if($product->image)
                                            <img src="{{ URL::to('/') }}/{{ $product->image }}" class="product-image-listing">
                                            @endif
                                        </div>
                                        <div class="listing-text">
                                            <h4> {{$product->name}} </h4>
                                            <?php
                                            $cartQuantity = 0;
                                            if (!ShoppingCart::isEmpty()) {
                                                $cartQuantity = getProductFromCartById($product->id,$shop->id);
                                            }
                                            ?>
                                            <h5> Category:{{$product->category->name}} </h5>
                                            <p class="mb-0"> Rs {{$product->shop_product->price}} </p>

                                            <div class="add-to-cart-btn">
                                                <form class="add-to-cart-form" method="post">
                                                    @if($cartQuantity)
                                                    <input class="number-pro-btn" type="number" name="quantity" value="<?php echo $cartQuantity ?>" readonly="readonly">
                                                    @else
                                                    <input class="number-pro-btn" type="number" name="quantity" readonly="readonly">
                                                    @endif
													 <input type="hidden" name="shop_id" value="{{ $shop->id }}">
                                                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                                                    <input type="hidden" name="action" value="">
                                                    <input type="hidden" name="raw_id" value="">
                                                    <button class="add-pro-btn" type="submit" onclick="this.form.action.value = this.value" value="increase">
                                                        <i class="material-icons">add</i>
                                                    </button>
                                                    <button class="min-pro-btn" type="submit" onclick="this.form.action.value = this.value" value="decrease">
                                                        <i class="material-icons">remove</i>
                                                    </button>

                                                </form>
                                                <!--<a class="" href="#"> Add </a>-->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                @endforeach
                                @endforeach
                            </div>
                        </div>
                        @php
                        $i++
                        @endphp
                        @endforeach


                    </div>

                </div>

            </div>



        </div>

    </div>
</section>

<div id="cartModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <!--div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Modal Header</h4>

      </div-->
      <div class="modal-body">
          <h4 class="mt-4"> Important Note </h4>
        <p id="cart-notice"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="common-btn blue-btn" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div class="cart-fixed {{ShoppingCart::count()==0?'d-none':''}}" id="cart-footer">
    <div class="container">
        <div class="cart-fix-left">
            <i class="material-icons">shopping_cart</i>
            <div class="cart-f-cont">
                <span id="cart-items-count">{{ShoppingCart::count()}}</span> <span class="mr-2"> item(s) </span>
                <p> Rs </p>
                <span id="cart-total">{{ShoppingCart::totalPrice()}}</span>
            </div>
        </div>
        <div class="cart-fix-right">
            <a class="common-btn" href="{{route('cart-index')}}"> View Cart </a>
            <!--a class="common-btn" href="{{route('cart-remove')}}"> Empty Cart </a-->
        </div>
    </div>
</div>


@endsection
