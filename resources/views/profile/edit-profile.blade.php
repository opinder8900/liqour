@extends('layouts.front-header')
@section('body-class', 'account-page')
@section('content')

@include('layouts.lower-tabs')

<div class="my-acc-bg"></div>
<section class="my-acc-sec">

    <div class="container">

        <div class="acc-main">

            <div class="row">

                @include('layouts.sidebar')


                <div class="col-md-8">
                    <div class="my-acc-main">
                        <h4 class="f-w mb-5">Edit Profile</h4>
                        <form id="edit-profile-form" name="edit-profile-form" method="post"  action="{{ route('update-profile') }}">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <div class="form-group">
                                <label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $user->name) }}" required autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="col-md-12 col-form-label">{{ __('Phone') }}</label>
                                <div class="col-md-12">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone', $user->phone) }}" required autocomplete="phone" maxlength="10">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <input type="submit" value = "Save" id="submit" name="submit" class="common-btn blue-btn">
                        </form>
                    </div>
                </div>

            </div>	

        </div>
    </div>



</div>
</section>


@endsection


