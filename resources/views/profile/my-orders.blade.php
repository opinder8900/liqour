@extends('layouts.front-header')
@section('body-class', 'order-page')
@section('content')

@include('layouts.lower-tabs')

<div class="my-acc-bg"></div>
<section class="my-acc-sec">

    <div class="container">

        <div class="acc-main">
            <div class="row">


                @include('layouts.sidebar')


                <div class="col-md-8">

                    <div class="my-acc-main">
                        <h4 class="f-w mb-4">My Order</h4>

                        <div class="f-w">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#order" role="tab" aria-controls="home" aria-selected="true">Open Orders</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#past_order" role="tab" aria-controls="profile" aria-selected="false">Past Orders</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#cancel_order" role="tab" aria-controls="contact" aria-selected="false">Canceled Orders</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                
                                <div class="tab-pane fade show active" id="order" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="my-acc-or-main mt-4">
                                        <ul>
                                            @if($new_orders->count())
                                            @foreach($new_orders as $order)
                                            <li>
                                                <div class="my-acc-or-img">
                                                    @if( $order->shop->image)
                                                    <img src="{{ URL::to('/') }}/{{ $order->shop->image }}" class="shop-image-listing">
                                                    @endif

                                                </div>
                                                <div class="my-acc-or-text">
                                                    <h4> {{ $order->shop->name}} </h4>
                                                    <p>  {{$order->shop->address}} </p>
                                                    <p> ORDER #{{ $order->id}} | {{date('l, F jS Y h:i A',strtotime($order->created_at))}} </p>
                                                    <a class="common-btn blue-btn mt-3" href="{{route('show-cancel-order',['id' => $order->id])}}">Cancel Order</a>
                                                </div>

                                                <div class="my-acc-or-item">
                                                    <p> 
                                                        @foreach($order->items as $item)
                                                        {{$item->product->name}}, 
                                                        @endforeach
                                                    </p>
                                                    <h4> Total Paid:  {{$order->total}} </h4>
                                                </div>
                                            </li>
                                            @endforeach
                                            @endif


                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="past_order" role="tabpanel" aria-labelledby="profile-tab">
                                    
                                        <div class="my-acc-or-main mt-4">
                                            <ul>
                                                @if($past_orders->count())
                                                
                                                @foreach($past_orders as $order)
                                                <li>
                                                    <div class="my-acc-or-img">
                                                        @if( $order->shop->image)
                                                        <img src="{{ URL::to('/') }}/{{ $order->shop->image }}" class="shop-image-listing">
                                                        @endif

                                                    </div>
                                                    <div class="my-acc-or-text">
                                                        <h4> {{ $order->shop->name}} </h4>
                                                        <p>  {{$order->shop->address}} </p>
                                                        <p> ORDER #{{ $order->id}} | {{date('l, F jS Y h:i A',strtotime($order->created_at))}} </p>
                                                        <p>Delivered On ,  {{date('l, F jS Y h:i A',strtotime($order->delivered_on))}} </p>

                                                    </div>

                                                    <div class="my-acc-or-item">
                                                        <p> 
                                                            @foreach($order->items as $item)
                                                            {{$item->product->name}}, 
                                                            @endforeach
                                                        </p>
                                                        <h4> Total Paid:  {{$order->total}} </h4>
                                                    </div>
                                                </li>
                                                @endforeach
                                                @else
                                                <li>No Order Found</li>
                                                @endif


                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="cancel_order" role="tabpanel" aria-labelledby="contact-tab">
                                        
                                            <div class="my-acc-or-main mt-4">
                                                <ul>
                                                    @if($cancelled_orders->count())
                                                    @foreach($cancelled_orders as $order)
                                                    <li>
                                                        <div class="my-acc-or-img">
                                                            @if( $order->shop->image)
                                                            <img src="{{ URL::to('/') }}/{{ $order->shop->image }}" class="shop-image-listing">
                                                            @endif

                                                        </div>
                                                        <div class="my-acc-or-text">
                                                            <h4> {{ $order->shop->name}} </h4>
                                                            <p>  {{$order->shop->address}} </p>
                                                            <p> ORDER #{{ $order->id}} | {{date('l, F jS Y h:i A',strtotime($order->created_at))}} </p>
                                                            <p>Delivered On ,  {{date('l, F jS Y h:i A',strtotime($order->delivered_on))}} </p>

                                                        </div>

                                                        <div class="my-acc-or-item">
                                                            <p> 
                                                                @foreach($order->items as $item)
                                                                {{$item->product->name}}, 
                                                                @endforeach
                                                            </p>
                                                            <h4> Total Paid:  {{$order->total}} </h4>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                    @else
                                                    <li>No Order Found</li>
                                                    @endif


                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
            </section>


            @endsection
