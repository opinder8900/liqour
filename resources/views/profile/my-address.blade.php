@extends('layouts.front-header')
@section('body-class', 'account-page')
@section('content')

@include('layouts.lower-tabs')

<section>
    <div class="container">
		<div class="row mt-5">
			
			<div class="col-md-3">
			
				<ul class="my-account-sidebar">
					<li> <a href="{{route('my-profile')}}"> Account </a> </li>
					<li> <a href="{{ route('my-orders') }}"> My Orders </a> </li>
					<li> <a href="{{ route('my-address') }}"> Address </a> </li>
					<li> <a href="{{ route('change-password') }}"> Change Password </a> </li>
				</ul>
				
			</div>
			
			<div class="col-md-9">
				<a href="javascript:void(0)" id="add-new-address"  class="btn btn-primary" data-toggle="modal" data-target="#addressModal">Add New Address</a>
				<div class="my-acc-main">
					Manage Address
					
				</div>
			</div>
			
		</div>
	</div>
</section>
      

      <!--div class="modal" id="addressModal">
		  <div class="modal-dialog">
			<div class="modal-content">

			  <div class="modal-header">
				<h4 class="modal-title">Modal Heading</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  </div>

			  <div class="modal-body">
				<form id="address-form" name="address-form" class="address-form" method="post">
					<div class="row">
						<div class="col-lg-6">
							<input name="full_name" id = "full_name" class="form-control" placeholder = "Full Name" type="text">
						</div>
						<div class="col-lg-6">
							<input name="phone" id = "phone" class="form-control" placeholder = "Phone" type="text">
						</div>
						<div class="col-lg-6">
							<input name="pin_code" id = "pin_code" class="form-control" placeholder = "Pin Code" type="text">
						</div>
						<div class="col-lg-6">
							<input name="address_line_1" id = "address_line_1" class="form-control" placeholder = "Address Line 1" type="text">
						</div>
						<div class="col-lg-6">
							<input name="address_line_2" id = "address_line_2" class="form-control" placeholder = "Address Line 1" type="text">
						</div>
						<div class="col-lg-6">
							<input name="landmark" id = "landmark" class="form-control" placeholder = "Landmark" type="text">
						</div>
						<div class="col-lg-6">
							<input name="country" id = "country" class="form-control" placeholder = "Country" type="text" value="India" readonly>
						</div>
						<div class="col-lg-6">
							<select name="state" id = "state" class="form-control">
								@foreach($states as $state)
									<option value="{{$state->state_code}}">{{$state->state_name}}</option>
								@endforeach
								
							</select>
						</div>
						<div class="col-lg-6">
							<select name="city" id = "city" class="form-control">
								@foreach($cities as $city)
									<option value="{{$city->id}}">{{$city->city_name}}</option>
								@endforeach
								
							</select>
						</div>
					</div>
					<input name="default-checkbox" id="default" class="form-control" type="checkbox"> Mark this as you default address
					<input type="submit" value = "Add" id="submit" name="submit" class="btn btn-primary">
				</form>
			  </div>

			  <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			  </div>

			</div>
		  </div>
		</div-->



@endsection


