@extends('layouts.front-header')
@section('body-class', 'account-page')
@section('content')
@include('layouts.lower-tabs')
<div class="my-acc-bg"></div>
<section class="my-acc-sec">
    <div class="container">

        <div class="acc-main">

            <div class="row">

                @include('layouts.sidebar')


                <div class="col-md-8">
                    <div class="my-acc-main">
                        <h4 class="f-w mb-5">My Profile</h4>
                        <div class="f-w mb-4">
                            <label> Phone number </label>
                            <h4> {{$user->phone}} </h4>
                        </div>

                        <div class="f-w mb-4">
                            <label> Email id </label>
                            <h4> {{$user->email}} </h4>
                        </div>

                        <div class="f-w mb-4">
                            <label> Name </label>
                            <h4> {{$user->name}}</h4>
                        </div>
                        <div class="f-w">
                            <a class="common-btn blue-btn" href="{{route('show-edit-profile')}}"> Edit Profile </a>
                        </div>
                    </div>
                </div>

            </div>	

        </div>
    </div>
</section>
@endsection

