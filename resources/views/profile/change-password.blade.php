@extends('layouts.front-header')
@section('body-class', 'account-page')
@section('content')

@include('layouts.lower-tabs')

<div class="my-acc-bg"></div>
<section class="my-acc-sec">

    <div class="container">

    <div class="acc-main">

        <div class="row">

            @include('layouts.sidebar')
          

            <div class="col-lg-8">
            <div class="my-acc-main">
            <h4 class="f-w mb-5">Change Password</h4>
            <div class="f-w">
                    <form method="POST" action="{{ route('change-password') }}">
                        @csrf 
   
                         @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                         @endforeach 
  
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">Current Password</label>
  
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">New Password</label>
  
                            <div class="col-md-12">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">New Confirm Password</label>
    
                            <div class="col-md-12">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
   
                        <div class="form-group mb-0 mt-4 row">
                            <div class="col-md-12">
                                <button type="submit" class="common-btn blue-btn">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
               
            </div>
            </div>

        </div>

    </div>
    </div>
</section>


@endsection


