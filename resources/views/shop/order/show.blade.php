@extends('shop/layouts/app')
@section('content')


<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-lg-12">
                <h2>  Order & Account Information  </h2>
            </div>

            <div class="col-lg-12 mt-5">

                <div class="table tr-hover bb-none-th">
                    <table>
                        <tr>
                            <th>Order Date</th>
                            <td>Feb 23, 2020, 12:40:12 AM</td>
                        </tr>
                        <tr>
                            <th>Order Status</th>
                            <td>{{$order->status}}</td>
                        </tr>
                        <tr>
                            <th>Customer Name</th>
                            <td>{{$order->user->name}}</td>
                        </tr>
                    </table>
                </div>

                <div class="f-w mt-5">
                    <h2>   Address Information   </h2>
                    <ul class="f-w mt-3 bk-ul">
                        <li> <p> {{$order->shipping_address->full_name}} </p> </li>
                        <li> <p> {{$order->shipping_address->address_line_1}} </p> </li>
                        <li> <p> {{$order->shipping_address->address_line_2}} </p> </li>
                        <li> <p> {{$order->shipping_address->city}}, {{$order->shipping_address->state}}, {{$order->shipping_address->pin_code}} </p> </li>
                        <li> <p> {{$order->shipping_address->country}} </p> </li>
                        <li> <p> {{$order->shipping_address->phone}} </p> </li>
                    </ul>
                </div>

                <div class="f-w mt-5">
                    <h2>    Payment & Shipping Method    </h2>
                    <ul class="f-w mt-3 bk-ul">
                        <li> <p> Cash On Delivery </p> </li>
                    </ul>
                </div>

                <div class="f-w mt-5">
                    <h2>  Items Ordered </h2>
                    <div class="table mt-3">
                        <table>
                            <tr>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Sub Toatal</th>
                            </tr>
                            @foreach($order->items as $item)

                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->quantity}}</td>
                            <td>{{$item->price}}</td>
                            <td>{{$item->sub_total}}</td>
                        </tr>

                        @endforeach
                        </table>
                    </div>
                </div>
                 <a href="{{ route('view-order-invoice', ['id'=>$order->id]) }}">Print Invoice</a>
				@if($order->status == "pending" || $order->status == "accepted")
                <div class="f-w mt-5">
                    <h2>  Select Status </h2>
                    <form action="{{ route('orders.update', $order->id) }}" method="post" >
						{{ method_field('PUT') }}
						{{ csrf_field() }}
                    <div class="f-w mt-3">
                        <div class="sel-ord-st">
                            <select class="form-control" name="order_status">
								<option value="accepted"> Accept </option>
                                <option value="cancelled"> Cancel </option>
                                
                                <option value="delivered"> Deliver</option>
                            </select>
                        </div>
                    </div>
                    <div class="f-w mt-5">
                       
                        <input class="common-btn blue-btn" type="submit" value="Accept Order">

                    </div>
                    </form>
                </div>
                @endif
               
            </div>
        </div>
    </div>
</div>
@endsection
