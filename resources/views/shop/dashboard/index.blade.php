@extends('shop/layouts/app')

@section('content')

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-md-12">
                <h2>Shop dashboard</h2>
            </div>
            <div class="col-md-12 mt-5">

                <div class="row">
                    
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Today's Orders</h4> 
                            <i class="material-icons">shopping_cart</i>
                            <h2 class="mb-0 mt-4"> {{$todayOrders}} </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Today's Sale</h4> 
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4"> 
							@if($todaySales->total)  
								{{$todaySales->total}}
                            @else 
								0
                            @endif  </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Last Week Sale</h4> 
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4">{{$lastWeekSales->total}}</h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">In Stock Products</h4>
                            <i class="material-icons">list_alt</i>
                            <h2 class="mb-0 mt-4"> {{$inStockProducts}} </h2>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

@endsection
