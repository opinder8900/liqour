@extends('shop/layouts/app')

@section('content')
@if($customers)

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">

            <div class="col-lg-12">
                <h2> Customers List </h2>
            </div>

            <div class="col-lg-12 mt-5">

                <table>
                    <tr><th>Name</th><th>Phone</th></tr>
                    @foreach($customers as $customer)
                    <tr>
                        <td>{{$customer->name}}</td>
                        <td>{{$customer->phone}}</td>
                    </tr>

                    @endforeach
                </table>
                @endif
                @endsection

            </div>

        </div>
    </div>
</div>