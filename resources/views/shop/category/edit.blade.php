@extends('shop/layouts/app')
@section('content')
@if($errors->any())
@foreach($errors->all() as $err)
{{$err}}
@endforeach
@endif
<form action="{{ route('categories.update', $category->id) }}" method="post"  enctype="multipart/form-data">

    {{ method_field('PUT') }}
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',$category->name)}}" required autocomplete="name" autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label for="slug" class="col-md-12 col-form-label">{{ __('Slug') }}</label>
        <div class="col-md-12">
            <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror" name="slug" value="{{ old('slug',$category->slug) }}" required autocomplete="name" autofocus>
            @error('slug')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>
        <div class="col-md-6">
            <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" required1 autocomplete="image" autofocus>
            @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        @if($category->image)
        <img src="{{ URL::to('/') }}/{{ $category->image }}" class="cat-image-parent">
        @endif
    </div>
    <div class="form-group row">
        <div class="col-md-6">
            <label>
                @if($category->status)
                <input type="checkbox" value="1" checked="checked"  name="status">
                @else
                <input type="checkbox" value="1"  name="status"> 
                @endif
                Status
            </label>
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
        <div class="col-md-6">
            <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description">{{ old('description',$category->description) }}</textarea>
            @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">
                {{ __('Update') }}
            </button>
        </div>
    </div>
</form>

@endsection
