@extends('shop/layouts/app')
@section('content')
@if($errors->any())
@foreach($errors->all() as $err)
{{$err}}
@endforeach
@endif

<div class="f-w">
<div class="container-fluid">

<div class="row mt-80">
    <div class="col-md-12">
        <h2>Shop dashboard</h2>
    </div>
</div>

<form action="{{ action('Shop\CategoryController@store') }}" method="post"  enctype="multipart/form-data">
    
    <div class="col-md-12 select-cate-pro mt-5">
        <h4>Add under Category</h4>
        <ul id="tree1">
            @foreach($categories as $key=> $category)
            <li>
                <label>
					<input type="radio" name="category_id" value="{{ $category->id }}">
					<span> {{ $category->name }} </span>
				</label>
                @if(count($category->childs))
                @include('shop.product.categoryChildControl',['product_category_id' => null,'childs' => $category->childs])
                @endif
            </li>
            @endforeach
        </ul>
    </div>
    <div class="form-group">
        <label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>
        <div class="col-md-12">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
   <div class="form-group">
        <label for="slug" class="col-md-12 col-form-label">{{ __('Slug') }}</label>
        <div class="col-md-12">
            <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror" name="slug" value="{{ old('slug') }}" required autocomplete="name" autofocus>
            @error('slug')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group">
        <label for="image" class="col-md-12 col-form-label">{{ __('Image') }}</label>
        <div class="col-md-12">
            <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" required1 autocomplete="image" autofocus>
            @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>
                <input type="checkbox" value="1"  name="status"> Status
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-md-12 col-form-label">{{ __('Description') }}</label>
        <div class="col-md-12">
            <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description">{{ old('description') }}</textarea>
            @error('description')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group mb-5">
        <div class="col-md-12">
            {{ csrf_field() }}
            <button type="submit" class="common-btn green-btn">
                {{ __('Save') }}
            </button>
        </div>
    </div>
</form>

</div>
</div>

@endsection
