@extends('shop/layouts/app')
@section('content')

<div class="f-w mt-80">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Category List</h2>
		</div>
		<div class="col-md-6 text-right">
            <a class="common-btn green-btn" href="{{ action('Shop\CategoryController@create') }}"> 
				<i class="material-icons">add_circle_outline</i> <span> Add Category </span>
			</a>
		</div>
	</div>  

    <div class="row">

        <div class="col-md-12 mt-4">
            <ul id="tree1" class="categories-list">
                @foreach($categories as $category)
                <li>
                    <div class="cate-list-outer">

                        <div class="cate-list-block">
                            @if($category->image)
                            <img src="{{ URL::to('/') }}/{{ $category->image }}" class="cat-image-parent">
                            @endif
                        </div>

                        <div class="cate-list-block">
                            <h4>
                            {{ $category->name }}
                            </h4>
                        </div>

                        <div class="cate-list-block">
                            @if($category->status)
                            <span>Active</span>
                            @else
                            <span>Inactive</span>
                            @endif
                        </div>

                        <div class="cate-list-block">
                            <a href="{{ route('categories.edit', $category->id) }}">Edit</a>
                        </div>

                        @if(count($category->childs) == 0)
                        <form class="cate-delete" method="post" action="{{ route('categories.destroy', $category->id) }}" onsubmit="return beforeCategoryDelete('{{$category->name}}')">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <div class="form-group">
                                <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                            </div>
                        </form>
                        @endif
                        

                        @if(count($category->childs))
                        @include('shop.category.manageChild',['childs' => $category->childs])
                        @endif
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>

</div>
</div>


@endsection
