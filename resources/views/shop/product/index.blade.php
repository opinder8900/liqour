@extends('shop/layouts/app')

@section('content')

<script>
    $(document).ready(function(){

        $('.shop-pro-stock input:checkbox').change(function(){
        if($(this).is(':checked')) 
            $(this).parent().parent().addClass('selected'); 
        else 
            $(this).parent().parent().removeClass('selected')
        });

    });
</script>

<div class="f-w">
    <div class="container-fluid">
        <form action="" method="GET">
            <div class="row mt-80">
                <div class="col-lg-4">
                    <h2> Products List </h2>
                </div>
                <div class="col-lg-2 text-right">
                    <div class="search-common back-search p-0">
                        <div class="inner-search-rl">
                            <select  data-target-href="{{url('superadmin/brands/by_category')}}" class="form-control product_category" name="category_id">
                                <option value="">All Categories</option>
                                @foreach($categories as $id=>$name)
                                <option value="{{$id}}" @if(app('request')->input('category_id') == $id) selected="selected" @endif >{{$name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 text-right">
                    <div class="search-common back-search p-0">
                        <div class="inner-search-rl">
                            <select class="form-control product_brand" name="brand_id">
                                <option value="">All Brands</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 text-right">
                    <div class="search-common back-search">
                        <div class="inner-search-rl">
                            <input class="form-control" type="text" value="{{ app('request')->input('search')}}" name="search" placeholder="Search Product"/>
                            <button class="common-btn serch-front-btn" type="submit">Search</button>
                        </div>
                        <a href="{{ action('Shop\ShopProductController@index') }}"> <i class="material-icons">close</i> Clear Result</a>
                    </div>
                </div>
            </div>
        </form>
        

        <div class="table mt-5 custom-table">
            <table>
                <tr>

                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Actions</th>

                </tr>
                @if( $shop_products )
                @foreach($shop_products as $shop_product)	
                <tr>


                    <td>
                        @if($shop_product->product->image)
                        <img src="{{ URL::to('/') }}/{{ $shop_product->product->image }}" class="product-image-listing">
                        @endif
                    </td>
                    <td>{{$shop_product->product->name}}</td>
                    <td>{{$shop_product->price}}</td>
                    <td>{{$shop_product->product->category->name}}</td>
                    <td>
                        @if($shop_product->status)
                        <span>Active</span>
                        @else
                        <span>Inactive</span>
                        @endif
                    </td>
					 <td> 
                                <label class="switch">
                                    <input type="checkbox" data-href="{{ url('shop/manage-products/toggle_status/'.$shop_product->product_id.'/'.$shop->id) }}" @if($shop_product->status) checked="checked" @endif >
                                    <span class="slider round"></span>
                                </label>
                            </td>
                    <!--td>
                        <form method="post" action="{{ route('manage-products.update', $shop_product->product->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            @if($shop_product->status)
                            <input type="hidden" name="action" value="0">
                            <input type="submit" class="" value="Disable">
                            @else
                            <input type="hidden" name="action" value="1">
                            <input type="submit" class="" value="Enable">
                            @endif
                        </form>
                    </td-->
                </tr>
                @endforeach

                @endif
            </table>
            {{ $shop_products->links() }}
        </div>

    </div>
</div>

@endsection
