<ul>
@foreach($childs as $key => $child)
	<li>
	    <label><input type="radio" name="category_id" value="{{ $child->id }}" {{ $child->id ==$product_category_id?'checked="checked"':'' }}>{{ $child->name }}</label>
	@if(count($child->childs))
            @include('shop.product.categoryChildControl',['product_category_id' => $product_category_id, 'childs' => $child->childs])
        @endif
	</li>
@endforeach
</ul>
