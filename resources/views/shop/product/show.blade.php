@extends('shop/layouts/app')

@section('content')

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-lg-12">
                <h2>Show page</h2>
            </div>

            <div class="col-lg-12 mt-5">
                <table class="table">
                    <tr>
                        
                        <th>Name</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Image</th>
                        <th>In Stock</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                    <tr>
                        
                        <td>{{$product->name}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->category->name}}</td>
                        <td>
                            @if($product->status)
                            <span>Active</span>
                            @else
                            <span>Inactive</span>
                            @endif
                        </td>
                        <td>
                            @if($product->image)
                            <img src="{{ URL::to('/') }}/{{ $product->image }}" class="product-image-listing">
                            @endif
                        </td>
                        <td>{{$product->in_stock_quantity}}</td>
                        <td>{{$product->created_at}}</td>

                        <td>
                            <div class="dropdown dropleft">
                            <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <!--a href="{{ route('products.show', $product->id) }}">
                                    <img class="" src="{{ URL::asset('assets/images/eye.svg') }}"> 
                                    <span> Show </span>
                                </a-->
                                <a href="{{ route('products.edit', $product->id) }}">
                                    <img class="" src="{{ URL::asset('assets/images/edit.svg') }}">    
                                    <span> Edit </span>
                                </a>
                                <form method="post" action="{{ route('products.destroy', $product->id) }}" onsubmit="return beforeProductDelete('{{$product->name}}')">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <div class="form-group delete-shop">
                                        <img class="" src="{{ URL::asset('assets/images/delete.svg') }}">
                                        <input type="submit" class="" value="Delete">
                                    </div>
                                </form>
                            </div>
                            </div>
                        </td>

                    </tr>
                </table>

                @endsection

            </div>
        </div>
    </div>
</div>