<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <!--<script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>-->

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/gif" sizes="16x16"> 
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>E-theka</title>
         @laravelPWA
        <!--Global JS variables-->
        <script>
            var CONFIG = {
            site_url: "{{ url('/') }}",
                    google_map_api_key: "{{ config('app.google_map_api_key') }}",
                    default_location:{
                    latitude : {{ config('app.default_latitude') }},
                            longitude : {{ config('app.default_longitude') }}
                    },
                    user:{
                    is_logged_in: "{{Auth::guest() ? false:true}}"
                    }
            }
        </script>

        <style>
            #map {
                height: 100%;
            }

        </style>
        <!-- Scripts -->

        <!--Please contact jatinder if you want to add app.js here-->
        <!--<script src="{{ asset('js/front-app.js') }}" defer></script>-->

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet">

        <!-- Styles -->
        <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">





    </head>
    <body class="@yield('body-class')">
        <div id="app">
            <div class="f-w front-header">
                <nav class="">
                    <div class="container">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            E-theka
                        </a>
                        <div id="change-delivery-address" class="loc-header-main">
                            @if(Session::has('location'))
                            <div class="main-loc-head">
                                @if(Session::has('location'))
                                <h4>{{ Session::get('location.type') }}</h4>
                                <p>{{ Session::get('location.display') }}</p>
                                @endif
                            </div>
                            <button class="loc-head-btn"> <i class="material-icons">expand_more</i> </button>
                            @else
                            <p class="cho-head-dl-add">Choose Delivery Address</p>
                            <button class="loc-head-btn"> <i class="material-icons">expand_more</i> </button>
                            @endif
                        </div>

                        <div class=" justify-content-end" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <!--div class="heaer-btns">
                                <a class="common-btn" href="#">{{ __('E-theka for Partners') }}</a>
                                <a class="common-btn" href="#">{{ __('E-theka for Business') }}</a>
                            </div-->
                            <div class="header-cart">
                                <a href="{{ route('cart-index') }}">
                                    <i class="material-icons">shopping_cart</i>
                                    @if(!ShoppingCart::isEmpty())
                                    <span id="card-number">{{ShoppingCart::count()}}</span>
                                    @endif
                                </a>
                            </div>
                            <ul class="my-acc-usr-ul">
                                <!-- Authentication Links -->
                                @guest
                                <div class="dropdown front-head-user">
                                    <a class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">account_circle</i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <ul>
                                            <li class="">
                                                <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>
                                            </li>
                                            <li class="">
                                                <a class="" href="{{ route('register') }}">{{ __('Register') }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                @else
                                <li class="dropdown">
                                    <a id="dropdownMenuButton" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        @foreach(explode(' ', Auth::user()->name) as $firstname)
                                        {{ $firstname }}
                                        @break
                                        @endforeach
                                        <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">

                                        <a class="dropdown-item" href="{{ route('my-profile') }}">
                                            {{ __('My Profile') }}
                                        </a>

                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                                    </div>

                                </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="f-w p-r z-2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            @include('superadmin/flash/flash-message')
                        </div>
                    </div>
                </div>
            </div>

            @include('location/address')
            @yield('content')
            @include('layouts.front-footer')

            <!-- Scripts -->


            <script src="{{ asset('js/jquery-3.5.1.min.js') }}" ></script>
            <script src="{{ asset('js/jquery.cookie.js') }}" ></script>
            <script src="{{ asset('js/app.js') }}"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>            
            <script src="https://maps.googleapis.com/maps/api/js?key={{config('app.google_map_api_key')}}&libraries=places&callback=initMap" async defer></script>
        </div>
    </body>

</html>
