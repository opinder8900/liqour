@extends('layouts.front-header')

@section('content')

<div class="my-acc-bg"></div>
<section class="my-acc-sec">

    <div class="container">

        <div class="acc-main">

            <div class="row">

            @include('layouts.sidebar')

            <div class="col-md-8">
                <div class="my-acc-main">
                    <h4 class="f-w mb-5">Cancel Order</h4>

                        <div class="cancel-my-o-p p-r f-w">
                            <div class="f-w my-acc-or-img">
                                @if( $order->shop->image)
                                <img src="{{ URL::to('/') }}/{{ $order->shop->image }}" class="shop-image-listing">
                                @endif
                            </div>
                            <div class="my-acc-or-text">
                                <h4> {{ $order->shop->name}} </h4>
                                <p>  {{$order->shop->address}} </p>
                                <p> ORDER #{{ $order->id}} | {{date('l, F jS Y h:i A',strtotime($order->created_at))}} </p>
                            </div>
                        </div>

                        <div class="my-acc-or-item">
                            <p> 
                                @foreach($order->items as $item)
                                {{$item->product->name}}, 
                                @endforeach
                            </p>
                            <h4> Total Paid:  {{$order->total}} </h4>
                        </div>

                        <div>
                            <form name="cancel-order-form" method="post" action="{{ route('cancel-order',['id' => $order->id]) }}">
                                {{csrf_field()}}
                                <label>Reason</label>
                                <textarea class="form-control mb-4" name="reason" required="required"></textarea>

                                <div class="f-w add-new-add-outer">
                                    <input type="submit" value = "Cancel" id="submit" name="submit" class="common-btn blue-btn">
                                </div>

                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
