@extends('layouts.front-header')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-80">

            <h2>Order Placed</h2>
            <p>Thanks your order has been placed</p>
            <a class="btn btn-success" href="{{ url('/') }}">Continue Shopping</a>
        </div>
    </div>
</div>
@endsection
