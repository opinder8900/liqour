@extends('layouts.error')
@section('content')

            <h2>Unauthorized</h2>
            <p>You are not autorized to view this page</p>
            @if(Auth::user()->role == 'superadmin')
            <a class="btn btn-success" href="{{ url('superadmin/dashboard') }}">Home</a>
            @elseif(Auth::user()->role == 'tradesman')
            <a class="btn btn-success" href="{{ url('tradesman/dashboard') }}">Home</a>
            @elseif(Auth::user()->role == 'shop')
            <a class="btn btn-success" href="{{ url('shop/dashboard') }}">Home</a>
            @else
            <a class="btn btn-success" href="{{ url('/') }}">Home</a>
            @endif
@endsection
