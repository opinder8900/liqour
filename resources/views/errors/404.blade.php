@extends('layouts.error')
@section('content')

<h2>Not Found</h2>
<p>You are not autorized to view this page</p>
{{ $exception->getMessage() }}
@endsection
