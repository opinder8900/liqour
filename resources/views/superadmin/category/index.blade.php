@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 catrgory-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Add Category</h2>
		</div>
	</div>  

    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            @include('superadmin/layouts/tabs')
        </div>
    </div>
    <form action="{{ url('superadmin/categories') }}" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ old('id',$category->id)}}">
        {{ csrf_field() }}
    
        <div class="cate-input-main">
            <div class="row">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Business</label>
                    <select class="form-control @error('business_id') is-invalid @enderror" name="business_id">
                        <option value="">Select Business</option>
                        @foreach($businesses as $id=>$name)
                        <option value="{{$id}}" @if($category->business_id==$id) selected="selected" @endif >{{$name}}</option>
                        @endforeach
                    </select>
                    @error('business_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Category</label>
                    <input type="text"  onkeyup="replaceAsSlug('category_name','category_slug')" id="category_name" name="name" required value="{{ old('name',$category->name)}}" class="form-control @error('name') is-invalid @enderror" placeholder="Category Name">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Slug</label>
                    <input type="text" name="slug" required value="{{ old('slug',$category->name)}}" class="form-control @error('slug') is-invalid @enderror" placeholder="Slug name"  id="category_slug">
                    @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-12 sa-cat-tabs mt-4">
                    <input class="common-btn" type="submit" value="{{empty($category->id) ? 'Add' : 'Save'}} Category">
                    @if($category->id)
                    <a href="{{url('superadmin/categories')}}" class="common-btn">Cancel</a>
                    @endif
                </div>
            </div>
        </div>
    </form>
    <div class="row mb-3">
		<div class="col-md-6">
			<h2> Categories List</h2>
			<h2> Total - {{$totalCategories}}</h2>
			
		</div>
    </div>

    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">
            <div class="table custom-table">
                <table>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Business </th>
                            <th> Slug </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                        <tr>
                            <td> {{$category->name}} </td>
                            <td> {{$category->business->name}} </td>
                            <td> {{$category->slug}} </td>
                            <td> 
                                <label class="switch">
                                    <input type="checkbox" data-href="{{ url('superadmin/categories/toggle_status/'.$category->id) }}" @if($category->status) checked="checked" @endif >
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="{{ url('superadmin/categories/'.$category->id) }}"> 
                                            <img class="" src="{{ URL::asset('assets/images/edit.svg') }}"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="{{ url('superadmin/categories/destroy/'.$category->id) }}" onclick="return beforeCategoryDelete('{{$category->name}}')"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>    
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $categories->links() }}
            </div>
        </div>
    </div>

</div>
</div>


@endsection
