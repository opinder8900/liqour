@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Tradesman</h2>
		</div>
	</div>


	<div class="row mt-5 mb-80">
        <div class="col-lg-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Enable</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Disable</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Pending</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

					<div class="table custom-table mt-5">
						<table>
							<thead>
								<tr>
									<th> Name </th>
									<th> Address </th>
									<th> Phone </th>
									<th> Email </th>
									<th> Registered For </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>

								@if($activeTradesman)
								@foreach($activeTradesman as $trademan)
									<tr>
										<td> {{$trademan->name }}</td>
										<td> {{$trademan->address }}</td>
										<td> {{$trademan->phone }}</td>
										<td> {{$trademan->email }}</td>
										<td> {{$trademan->registered_for }}</td>
										<td>
											<label class="switch">
												<input type="checkbox"data-href="{{ url('superadmin/trademan/toggle_status/'.$trademan->id) }}" @if($trademan->status) checked="checked" @endif>
												<span class="slider round"></span>
											</label> 
										</td>
										<td>
												<div class="dropdown dropleft">
													<div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
													</div>
													<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<a href="{{ url('superadmin/trademan/shops/'.$trademan->id) }}"> 
															<i class="material-icons"> store_mall_directory </i> 
															<span> Shops </span> 
														</a>
														<a href="#"> 
															<i class="material-icons"> delete </i> 
															<span> Delete </span> 
														</a>
													</div>
												</div>
										</td>
									</tr>
								@endforeach
								
							@endif
						</table>
					</div>

				</div><!--tab 1 end-->
				<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					
				<div class="table custom-table mt-5">
						<table>
							<thead>
								<tr>
									<th> Name </th>
									<th> Address </th>
									<th> Phone </th>
									<th> Email </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>

						<tbody>
					@if($inactiveTradesman)
						@foreach($inactiveTradesman as $trademan)
							<tr>
								<td> {{$trademan->name }}</td>
								<td> {{$trademan->address }}</td>
								<td> {{$trademan->phone }}</td>
								<td> {{$trademan->email }}</td>
								<td>
									<label class="switch">
										<input type="checkbox"data-href="{{ url('superadmin/trademan/toggle_status/'.$trademan->id) }}" @if($trademan->status) checked="checked" @endif>
										<span class="slider round"></span>
									</label> 
								</td>
								<td>
										<div class="dropdown dropleft">
											<div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
											</div>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a href="{{ action('Superadmin\ShopController@index') }}"> 
													<i class="material-icons"> store_mall_directory </i> 
													<span> Shops </span> 
												</a>
												<a href="#"> 
													<i class="material-icons"> delete </i> 
													<span> Delete </span> 
												</a>
											</div>
										</div>
								</td>
							</tr>
						@endforeach
						
					@endif
				
				</tbody>
						</table>
					</div>

				</div><!--tab 2 end-->
				<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
					<div class="table custom-table mt-5">
						<table>
							<thead>
								<tr>
									<th> Name </th>
									<th> Address </th>
									<th> Phone </th>
									<th> Email </th>
									<th> Status </th>
									<th> Actions </th>
								</tr>
							</thead>


							<tbody>
								<tr>
									<td> Richard B. Wicklund </td>
									<td> 3315 Stanley Avenue Manhattan, NY 10016 </td>
									<td> 516-525-8963 </td>
									<td> RichardBWicklund@rhyta.com </td>
									<td>
										<label class="switch">
											<input type="checkbox">
											<span class="slider round"></span>
										</label>
									</td>
									<td>
										<div class="dropdown dropleft">
											<div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
											</div>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a href="#"> 
													<i class="material-icons"> check_circle_outline </i> 
													<span> Accept </span> 
												</a>
												<a href="#"> 
													<i class="material-icons"> delete </i> 
													<span> Delete </span> 
												</a>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div><!--tab 3 end-->
			</div>
		</div>
	</div>

    <div class="row mt-5 mb-80">
        <div class="col-lg-12 sa-cat-tabs">
		
		</div>
	</div>

</div>
</div>


@endsection
