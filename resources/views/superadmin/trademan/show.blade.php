@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Shops</h2>
			<p>Trademan - <a href="{{url('superadmin/trademan')}}">{{$shops->name}}</a></p>
		</div>
	</div>  

    <div class="row mt-5 mb-80">
        <div class="col-lg-12 sa-cat-tabs">
		<div class="table custom-table">

			<table>
				<thead>
					<tr>
						<th> Name </th>
						<th> Address </th>
						<th> Phone </th>
						<th> Email </th>
						<th> licence </th>
						<th> Status </th>
						<th> Actions </th>
					</tr>
				</thead>
				
				<tbody>
					@if($shops)
						@foreach($shops->shops as $shop)
							<tr>
									<td>{{$shop->name}} </td>
									<td> {{$shop->address}}</td>
									<td> {{$shop->primary_phone}} </td>
									<td> {{$shop->shopKeeper->email}}  </td>
									<td>  {{$shop->license_number}}  </td>
									<td>
									<label class="switch">
										<input type="checkbox"data-href="{{ url('superadmin/trademan/shop/toggle_status/'.$shop->shopKeeper->id) }}" @if($shop->shopKeeper->status) checked="checked" @endif>
										<span class="slider round"></span>
									</label> 
									</td>
									<td>
										<div class="dropdown dropleft">
											<div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
											</div>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a href="{{ url('superadmin/shops/products/add/'.$shop->id) }}"> 
													<i class="material-icons"> add_circle </i> 
													<span> Add Products </span> 
												</a>
												<a href="{{ url('superadmin/shops/products/'.$shop->id) }}"> 
													<i class="material-icons"> visibility </i> 
													<span> View Products </span> 
												</a>
												<a href="{{ url('superadmin/trademan/shops/delete/'.$shop->id) }}"> 
													<i class="material-icons"> delete </i> 
													<span> Delete </span> 
												</a>
											</div>
										</div>
									</td>
								</tr>
						@endforeach
					@endif
				
				
				</tbody>
			</table>
                

            </div>
        </div>
    </div>

</div>
</div>


@endsection
