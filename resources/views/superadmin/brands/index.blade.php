@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 brand-tab-page">
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-6">
            <h2>Add Brand</h2>
        </div>
    </div>  

    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            @include('superadmin/layouts/tabs')
        </div>
    </div>
    <form action="{{ url('superadmin/brands') }}" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ old('id',$brand->id)}}">
        {{ csrf_field() }}
    
        <div class="cate-input-main">
            <div class="row">
                <div class="col-lg-3 sa-cat-tabs">
                    <label>Select Business</label>
                    <select data-target-href="{{url('superadmin/categories/by_business')}}" class="form-control brand_business @error('business_id') is-invalid @enderror" name="business_id">
                        <option value="">Select Business</option>
                        @foreach($businesses as $id=>$name)
                        <option value="{{$id}}" @if($brand->business_id==$id) selected="selected" @endif >{{$name}}</option>
                        @endforeach
                    </select>
                    @error('business_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-3 sa-cat-tabs">
                    <label>Select Category</label>
                    <select class="form-control brand_category @error('category_id') is-invalid @enderror" name="category_id">
                        <option value="">Select Category</option>
                        @foreach($categories as $id=>$name)
                        <option value="{{$id}}" @if($brand->category_id==$id) selected="selected" @endif >{{$name}}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-3 sa-cat-tabs">
                    <label>Enter Brand</label>
                    <input type="text" onkeyup="replaceAsSlug('brand_name','brand_slug')" id="brand_name"  name="name" required value="{{ old('name',$brand->name)}}" class="form-control @error('name') is-invalid @enderror" placeholder="Brand Name">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-3 sa-cat-tabs">
                    <label>Enter Slug</label>
                    <input type="text" name="slug" required value="{{ old('slug',$brand->name)}}" class="form-control @error('slug') is-invalid @enderror" placeholder="Slug name" id="brand_slug">
                    @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-12 sa-cat-tabs mt-4">
                    <input class="common-btn" type="submit" value="{{empty($brand->id) ? 'Add' : 'Save'}} Brand">
                    @if($brand->id)
                    <a href="{{url('superadmin/brands')}}" class="common-btn">Cancel</a>
                    @endif
                </div>
            </div>
        </div>
    </form>
    @foreach($brands as $category_name => $grouped_brands)
    <div class="row mb-3">
        <div class="col-md-6">
            <h2> {{Str::title($category_name)}}</h2>
        </div>
    </div>

    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">
            <div class="table custom-table">
                <table>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Slug </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                        @foreach($grouped_brands as $brand)
                        <tr>
                            <td> {{$brand->name}} </td>
                            <td> {{$brand->slug}} </td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" data-href="{{ url('superadmin/brands/toggle_status/'.$brand->id) }}" @if($brand->status) checked="checked" @endif >
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="{{ url('superadmin/brands/'.$brand->id) }}"> 
                                            <img class="" src="{{ URL::asset('assets/images/edit.svg') }}"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="{{ url('superadmin/brands/destroy/'.$brand->id) }}" onclick="return beforeCategoryDelete('{{$brand->name}}')"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    <tbody>
                </table>
            </div>
        </div>
    </div>
    @endforeach
</div>
</div>


@endsection
