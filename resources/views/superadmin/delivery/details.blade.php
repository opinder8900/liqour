
@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">

    <div class="row mb-3">

      <div class="col-md-6">
        <h2> delivery Details </h2>
      </div>

      <div class="col-md-6">

        <div class="row">

          <div class="col-lg-4">
            <label >Start</label>
            <input type="date" name="bday" max="3000-12-31" min="1000-01-01" class="form-control">
          </div>

          <div class="col-lg-4">
            <label >End</label>
            <input type="date" name="bday" min="1000-01-01"max="3000-12-31" class="form-control">
          </div>

          <div class="col-lg-4">
            <label class="w-100">&nbsp;</label>
            <button class="common-btn w-100"> Submit </button>
          </div>

        </div>

      </div>
    
    </div>

    <div class="row mb-5">
      <div class="col-lg-12">
          <h4 class="mb-3"> Monday </h4>
          <h6 class="float-left mb-3"> Total Delivery: 10 </h6>
          <h6 class="float-right mb-3"> Total Amount: 10,000 </h6>
        <div class="table custom-table">
          <table>
            <tr> 
              <th> # </th>
              <th> Shop </th>
              <th> Customer </th>
              <th> Pick Up Time </th>
              <th> Delivery Time </th>
              <th> Amount </th>
            </tr>
            <tr>
              <td> 1 </td>
              <td> tekha 1, Sector 71, mohali </td>
              <td> Deepak, sector 70, mohali </td>
              <td> 11:30 </td>
              <td> 11:50 </td>
              <td> 500 </td>
            </tr>
            <tr>
              <td> 2 </td>
              <td> tekha 2, Sector 71, mohali </td>
              <td> Aman, sector 80, mohali </td>
              <td> 12:05 </td>
              <td> 12:25 </td>
              <td> 700 </td>
            </tr>
          </table>
        </div>
      </div>
    </div>

</div>
</div>

@endsection
