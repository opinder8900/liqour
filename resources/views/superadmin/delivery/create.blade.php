@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> delivery boys details </h2>
        </div>
        <div class="col-md-6">
			
		</div>
    </div>

    <div class="row mt-5 mb-80">
		<div class="col-md-12">
			<div class="common-white-bg">
              <form action="{{ action('Superadmin\DeliveryController@store') }}" method="post" enctype="multipart/form-data" id="shop_form">
					 {{ csrf_field() }}
                    <div class="row">
                        
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('Full Name') }}</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" required="required" id="name"> 
                            
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        
                        <div class="col-md-4 mb-4">
                            <label for="shop_slug" class="col-form-label">{{ __('Email') }}</label>
                            <input type="text" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" required="required" id="email" >
                            
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-md-4 mb-4">
                            <label for="shop_slug" class="col-form-label">{{ __('Password') }}</label>
                            <input type="password" name="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" required="required" id="password" >
                            
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('Phone Number') }}</label>
                            <input type="text" name="primary_phone" value="{{ old('primary_phone') }}" class="form-control @error('primary_phone') is-invalid @enderror" required="required">
                            @error('primary_phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                      
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class=" col-form-label">{{ __('Other Phone Number') }}</label>
                            <input type="text" name="secondary_phone" value="{{ old('secondary_phone') }}" class="form-control @error('secondary_phone') is-invalid @enderror" required="required">
                            @error('secondary_phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                       
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class=" col-form-label">{{ __('Address') }}</label>
                            <input type="text" name="address" value="{{ old('address') }}" class="form-control @error('address') is-invalid @enderror" required="required">
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('City') }}</label>
                             <select id="city" class="form-control @error('city') is-invalid @enderror select-state" name="city" value="{{ old('city') }}" required>
                                    <option value = "" selected="selected">Select City</option>
                                    @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->city_name}}</option>
                                    @endforeach
                                </select>
                                @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                        </div>
                        
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('State') }}</label>
                            <select id="state" class="form-control @error('state') is-invalid @enderror" name="state" value="{{ old('state') }}" required>
                                    
                                    <option value = "" selected="selected">Select State</option>
                                    @foreach($states as $state)
                                    <option value="{{$state->state_code}}">{{$state->state_name}}</option>
                                    @endforeach
                                </select>
                                @error('state')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                        </div>
                        
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('Zip Code') }}</label>
                            <input type="text" name="zipcode" value="{{ old('zipcode') }}" class="form-control @error('zipcode') is-invalid @enderror" required="required" maxlength="6">
                            @error('zipcode')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                      
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class=" col-form-label">{{ __('license Photo') }}</label>
                            <input type="file" name="license_photo" id="license_photo" value="{{ old('license_photo') }}" class="form-control">
                            @error('license_photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                      
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class=" col-form-label">{{ __('Vehicle RC Photo') }}</label>
                            <input type="file" name="rc_photo" id="rc_photo" value="{{ old('rc_photo') }}" class="form-control">
                            @error('rc_photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                       
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('Aadhar Card Photo') }}</label>
                            <input type="file" name="adhaar_photo" id="adhaar_photo" value="{{ old('adhaar_photo') }}" class="form-control">
                            @error('adhaar_photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                      
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('PAN Card Photo') }}</label>
                            <input type="file" name="pan_photo" id="pan_photo" value="{{ old('pan_photo') }}" class="form-control">
                            @error('pan_photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class=" col-form-label">{{ __('In Time') }}</label>
                            <input type="text" name="in_time" id="in_time" value="{{ old('in_time') }}" class="form-control @error('in_time') is-invalid @enderror timepicker">
                            @error('in_time')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                     
                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class=" col-form-label">{{ __('Out Time') }}</label>
                            <input type="text" name="out_time" id="out_time" value="{{ old('out_time') }}" class="form-control @error('out_time') is-invalid @enderror timepicker">
                            @error('out_time')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-md-4 mb-4">
                            <label for="shop_name" class=" col-form-label">{{ __('Security Deposit') }}</label>
                            <input type="text" name="security_deposit" id="security_deposit" value="{{ old('security_deposit') }}" class="form-control @error('security_deposit') is-invalid @enderror timepicker">
                            @error('security_deposit')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-md-12 mb-4">
                            <input type="submit" name="submit" class="common-btn"> 
                        </div>

                    </div>

                </form>
            </div>
		</div>
    </div>

    <div class="row">
		<div class="col-md-6">
			<h2> Account details </h2>
        </div>
        <div class="col-md-6">
			
		</div>
    </div>

    <div class="row mt-5 mb-80">
		<div class="col-md-12">
			<div class="common-white-bg">
              <form action="{{ action('Tradesman\ShopController@store') }}" method="post" enctype="multipart/form-data" id="shop_form">

                    <div class="row">
                        
                        <div class="col-md-6 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('Name') }}</label>
                            <input type="text" name="account_name" value="{{ old('account_name') }}" class="form-control @error('account_name') is-invalid @enderror" required="required" id="account_name"> 
                            
                            @error('account_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-md-6 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('Phone Number') }}</label>
                            <input type="text" name="account_phone" value="{{ old('account_phone') }}" class="form-control @error('account_phone') is-invalid @enderror" required="required" id="account_phone"> 
                            
                            @error('account_phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-md-6 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('Account Number') }}</label>
                            <input type="text" name="account_number" value="{{ old('account_number') }}" class="form-control @error('account_number') is-invalid @enderror" required="required" id="account_number"> 
                            
                            @error('account_number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-md-6 mb-4">
                            <label for="shop_name" class="col-form-label">{{ __('IFSC Code') }}</label>
                            <input type="text" name="ifsc_code" value="{{ old('ifsc_code') }}" class="form-control @error('ifsc_code') is-invalid @enderror" required="required" id="ifsc_code"> 
                            
                            @error('ifsc_code')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="col-md-6 mb-4">
                            <input type="submit" name="name" class="common-btn"> 
                        </div>

                    </div>

                </form>
            </div>
		</div>
    </div>

</div>
</div>

@endsection
