@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> delivery boys </h2>
        </div>

        <div class="col-md-4">
                <input type="text" placeholder="Search Delivery Boy" class="form-control">
                <button class="common-btn serch-front-btn" type="submit">Search</button>
        </div>
        <div class="col-md-2">
			<a href="{{url('superadmin/delivery/create')}}" class="common-btn w-100 text-center"> Add delivery boy </a>
        </div>
    </div>
        

    <div class="row mt-5">
		<div class="col-md-12">
			<div class="table custom-table">
                <table>
                    <tr>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Phone </th>
                        <th> Address </th>
                        <th> Status </th>
                        <th> Actions </th>
                    </tr>
                    <tr>
						@foreach($user as $row)
                        <td> {{$row->name}} </td>
                        <td> {{$row->email}}</td>
                        <td> {{$row->phone}} </td>
                        <td> {{$row->delivery_profile->address}} </td>
						<td>
							<label class="switch">
								<input type="checkbox" data-href="{{ url('superadmin/delivery/toggle_status/'.$row->id) }}" @if($row->status) checked="checked" @endif >
								<span class="slider round"></span>
							</label> 
						</td>
                        <td>
                            <div class="dropdown dropleft">
                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="{{url('superadmin/delivery/'.$row->id)}}"> 
                                        <i class="material-icons"> visibility </i> 
                                        <span> View </span> 
                                    </a>
                                    <a href="{{url('superadmin/delivery/details'.$row->id)}}"> 
                                        <i class="material-icons"> local_shipping </i> 
                                        <span> Delivery </span> 
                                    </a>
                                    <a href="{{url('superadmin/delivery/timings'.$row->id)}}"> 
                                        <i class="material-icons"> access_time </i> 
                                        <span> Timings </span> 
                                    </a>
                                    <a href="#"> 
                                        <i class="material-icons"> edit </i> 
                                        <span> Edit </span> 
                                    </a>
                                    <a href="#"> 
                                        <i class="material-icons"> delete </i> 
                                        <span> Delete </span> 
                                    </a>
                                </div>
                            </div>
                        </td>
                        @endforeach
                    </tr>
                </table>
            </div>
		</div>
    </div>

</div>
</div>

@endsection
