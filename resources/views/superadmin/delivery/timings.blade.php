@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row mb-5">

        <div class="col-md-6">
        <h2> timings Details </h2>
        </div>

        <div class="col-md-6">

        <div class="row">

            <div class="col-lg-4">
            <label >Start</label>
            <input type="date" name="bday" max="3000-12-31" min="1000-01-01" class="form-control">
            </div>

            <div class="col-lg-4">
            <label >End</label>
            <input type="date" name="bday" min="1000-01-01"max="3000-12-31" class="form-control">
            </div>

            <div class="col-lg-4">
            <label class="w-100">&nbsp;</label>
            <button class="common-btn w-100"> Submit </button>
            </div>

        </div>

        </div>

        </div>

        <div class="row mb-5">
        <div class="col-lg-12">
        <div class="table custom-table">
            <h4 class="mb-3"> Last Week </h4>
            <table>
            <tr> 
                <th> Day </th>
                <th> Check In </th>
                <th> Check Out </th>
                <th> Total Time </th>
            </tr>
            <tr>
                <td> Monday </td>
                <td> 10:00 am </td>
                <td> 09:00 pm </td>
                <td> 11 Hours </td>
            </tr>
            <tr>
                <td> Tuesday </td>
                <td> 10:00 am </td>
                <td> 09:00 pm </td>
                <td> 11 Hours </td>
            </tr>
            </table>
        </div>
        </div>
        
    </div>

</div>
</div>

@endsection

