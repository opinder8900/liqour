
@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> List of Orders </h2>
        </div>
      
    </div>
    
    <div class="row mt-5">
		<div class="col-md-12">
			<div class="table custom-table">
                <table>
                    <tr>
                        <th> Discount </th>
                        <th> Tax </th>
                        <th> Total </th>
                        <th> Payment Mode </th>
                        <th> Placed On </th>
                       
                        <th> Actions </th>
                    </tr>
                       @if($orders)
							@foreach($orders as $order)
								 <tr>
									<td> @if($order->discount) {{$order->discount}} @else 0 @endif</td>
									<td> {{$order->tax}}</td>
									<td> {{$order->total}}</td>
									<td> {{$order->payment_mode}}</td>
									<td> {{date("l jS \of F Y h:i:s A",strtotime($order->created_at))}}</td>
									
									
									
							<td>
								<div class="dropdown dropleft">
									<div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
									</div>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a href="{{ url('superadmin/customer/orders/details/'.$order->id) }}"> 
											<i class="material-icons"> add_circle </i> 
											<span> Order Details </span> 
										</a>
										
									</div>
								</div>
							</td>
						</tr>
								
							@endforeach
						@endif
                                   
                  
                </table>
            </div>
		</div>
    </div>
    
</div>
</div>

@endsection

