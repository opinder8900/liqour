@extends('superadmin/layouts/app')
@section('content')


<div class="f-w mt-80">
<div class="container-fluid">
    
    <div class="row">

		<div class="col-md-6">
			<h2> Reports </h2>
		</div>
		
		<div class="col-md-6 report-sp-tr-out">
			<form action="" method="GET">
				<select  class="form-control " name="search">
					<option value="today">Today</option>
					<option value="week">Week</option>
					<option value="month">Month</option>
				</select>                    
				<button class="common-btn" type="submit">Search</button>                        
			</form>
		</div>
		
		<div class="col-md-12 mt-5">
			<div class="table custom-table">
				<table class="">
					<thead>
					<tr>
						<th>Trademan Name</th>
						<th>Sale</th>
					</tr>
					</thead>
					<tbody>
					@if($reports)
						@foreach($reports as $report)
							<tr>
									<td>{{$report->trademan_name}}</td>
									<td>{{$report->total}}</td>
							</tr>
						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>

	</div>
	
</div>
</div>

@endsection
