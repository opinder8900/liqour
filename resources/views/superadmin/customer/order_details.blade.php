
@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> List of Orders </h2>
        </div>
       
    </div>
    
    <div class="row mt-5">
		<div class="col-md-12">
			<div class="table custom-table">
                <table>
                    <tr>
                        <th> Product Name </th>
                        <th> Price </th>
                        <th> Quantity </th>
                        <th> SubTotal </th>
                        
                        
                    </tr>
                       @if($order_details)
							@foreach($order_details as $order)
								 <tr>
									<td>{{$order->name}} </td>
									<td> {{$order->price}}</td>
									<td> {{$order->quantity}}</td>
									<td> {{$order->sub_total}}</td>
									
									
									
							
						</tr>
								
							@endforeach
						@endif
                                   
                  
                </table>
            </div>
		</div>
    </div>
    
</div>
</div>

@endsection


