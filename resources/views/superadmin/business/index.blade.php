@extends('superadmin/layouts/app')
@section('content')
<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Type of Business</h2>
		</div>
	</div>  

    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            @include('superadmin/layouts/tabs')
        </div>
    </div>
    <form action="{{ url('superadmin/business') }}" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ old('id',$business->id)}}">
        {{ csrf_field() }}
        <div class="cate-input-main">
            <div class="row">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Type Your Business</label>
                    <input type="text" onkeyup="replaceAsSlug('business_name','business_slug')" id="business_name" name="name" required value="{{ old('name',$business->name)}}" class="form-control @error('name') is-invalid @enderror" placeholder="Enter Type of Your Business">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label class="d-block">Upload Image @if($business->image) <img class="pro-img float-right mr-2" src="{{ URL::asset($business->image) }}"> @endif</label>
                    <input type="file" name="image" accept="image/*" class="form-control" placeholder="Upload Image">
                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Slug</label>
                    <input type="text" name="slug" required value="{{ old('slug',$business->slug)}}" class="form-control @error('slug') is-invalid @enderror" placeholder="Slug Name" id="business_slug">
                    @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-12 sa-cat-tabs mt-4">
                    <input class="common-btn" type="submit" value="{{empty($business->id) ? 'Add' : 'Save'}} Type">
                    @if($business->id)
                    <a href="{{url('superadmin/business')}}" class="common-btn">Cancel</a>
                    @endif
                </div>
            </div>
        </div>
    </form>
    <div class="row mb-3">
		<div class="col-md-6">
			<h2> Business List</h2>
		</div>
    </div>

    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">
            <div class="table custom-table">
                <table>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Images </th>
                            <th> Slug </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($businesses as $business)
                        <tr>
                            <td> {{$business->name}} </td>
                            <td> @if($business->image) <img class="pro-img" src="{{ URL::asset($business->image) }}"> @endif </td>
                            <td> {{$business->slug}} </td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" data-href="{{ url('superadmin/business/toggle_status/'.$business->id) }}" @if($business->status) checked="checked" @endif >
                                    <span class="slider round"></span>
                                </label> 
                            </td>
                            <td>
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="{{ url('superadmin/business/'.$business->id) }}"> 
                                            <img class="" src="{{ URL::asset('assets/images/edit.svg') }}"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="{{ url('superadmin/business/destroy/'.$business->id) }}" onclick="return beforeCategoryDelete('{{$business->name}}')"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>    
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $businesses->links() }}    
            </div>
                
        </div>
    </div>

</div>
</div>


@endsection
