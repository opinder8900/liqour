<ul>
    @foreach($childs as $child)
    <li>
        <div class="cate-list-block">
            @if($child->image)
            <img src="{{ URL::to('/') }}/{{ $child->image }}" class="cat-image-child">
            @endif
        </div>

        <div class="cate-list-block">
            <h4>
            {{ $child->name }}
            </h4>
        </div>

        <div class="cate-list-block">
            @if($child->status)
            <span>Active</span>
            @else
            <span>Inactive</span>
            @endif
        </div>

        @if(count($child->childs))
        @include('superadmin.category.manageChild',['childs' => $child->childs])
        @endif

        <div class="cate-list-block">
            <a href="{{ route('categories.edit', $child->id) }}">Edit</a>
        </div>

        <div class="cate-list-block">
            @if(count($child->childs) == 0)
            <form class="cate-delete" method="post" action="{{ route('categories.destroy', $child->id) }}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <div class="form-group">
                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                </div>
            </form>
            @endif
        </div>

    </li>
    @endforeach
</ul>
