@extends('superadmin/layouts/app')
@section('content')

<script>
    $(document).ready(function(){
	
        $(document).on('click','.add-pro-price input:checkbox', function(){
            $(this).closest('tr').toggleClass("checked");
        });
        $(document).on('change','.shop_category',function(){
            $('.shop_brand_container').html('<li> <label> </label> <span> Loading brands... </span> </li>');
            $.ajax({url: "{{url('superadmin/brands/by_category')}}/"+$(this).val(),success: function(brands){
                let html='';
                $.each(brands,function(index, brand){
                    html +='<li> <label> <input class="shop_brand" type="radio" '+(index==0 ? 'checked="checked"' : '')+' name="brand" value="'+brand.id+'"> </label> <span> '+brand.name+' </span> </li>';
                });
                $('.shop_brand_container').html(html);
                if(!brands.length){
                    $('.shop_product_container').html('');
                }
                $('.shop_brand:checked').trigger('change');
            }});
        });
        $(document).on('change','.shop_brand',function(){
            $('.shop_product_container').html('<tr><td> <label> </label></td><td colspan="2"> <span> Loading products... </span> </td></tr>');
            $.ajax({url: "{{url('superadmin/products/by_brand')}}/"+$(this).val()+"/{{$shop->id}}",success: function(products){
                let html='';
                $.each(products,function(index, product){
                    html +='<tr> <td> <input type="hidden" name="product['+index+'][product_id]" value="'+product.id+'"> <label> <input type="hidden" name="product['+index+'][selected]" value="0"> <input type="checkbox" name="product['+index+'][selected]" value="1"> </label> </td> <td> <span> '+product.name+' <span> '+product.size+''+product.size_label+' </span> </span> </td> <td> <input class="form-control" type="number" required min="0" max="1000000" name="product['+index+'][price]" value="" placeholder="Enter Price"> </td> </tr>';
                });
                $('.shop_product_container').html(html);
            }});
        });
    });
</script>

<div class="f-w mt-80 add-products-page">
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-6">
            <h2>{{Str::title($shop->name)}}</h2>
        </div>
    </div>  

    <div class="row mt-2">
        <div class="col-md-6">
            <h4>Add Products</h4>
        </div>
    </div>
     <form action="{{ url('superadmin/shops/products/'.$shop->id) }}" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ old('id',$shop->id)}}">
        {{ csrf_field() }}
    
        <div class="row mt-5">

            <div class="col-md-3">
                <div class="pro-add-main">
                    <h4 class="f-w mb-4">Select Category</h4>
                    <ul class="add-pro-ul shop_category_container">
                        @if(count($categories) <=0 )
                        <li>
                            <label>
                                
                            </label>
                            <span> No categories found </span>
                        </li>
                        @endif
                        @foreach($categories as $id=>$name)
                        <li>
                            <label>
                                <input class="shop_category" type="radio" @if($loop->first) checked="checked" @endif name="category" value="{{$id}}">
                            </label>
                            <span> {{Str::title($name)}} </span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="pro-add-main">
                    <h4 class="f-w mb-4">Select Brands</h4>
                    <ul class="add-pro-ul shop_brand_container">
                        @if(count($brands) <=0 )
                        <li>
                            <label>
                                
                            </label>
                            <span> No brands found </span>
                        </li>
                        @endif
                        @foreach($brands as $id=>$name)
                        <li>
                            <label>
                                <input class="shop_brand" type="radio" @if($loop->first) checked="checked" @endif name="brand" value="{{$id}}">
                            </label>
                            <span> {{Str::title($name)}} </span>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="col-md-6">
                <div class="pro-add-main">
                    <h4 class="f-w mb-4">Add Products</h4>
                    <div class="table add-pro-price">
                        <table class="shop_product_container">
                            @if(count($products) <=0 )
                            <tr>
                                <td>
                                    <label> </label>
                                </td>
                                <td colspan="2"><span> No products to add to this class </span></td>
                            </tr>
                            @endif
                            @foreach($products as $index=>$product)
                            <tr>
                                <td>
                                    <input type="hidden" name="product[{{$index}}][product_id]" value="{{$product->id}}">
                                    <label>
                                        <input type="hidden" name="product[{{$index}}][selected]" value="0">
                                        <input type="checkbox" class="shop_product" name="product[{{$index}}][selected]" value="1">
                                    </label>
                                </td>
                                <td>
                                    <span> {{Str::title($product->name)}} <span> {{$product->size}}{{$product->size_label}} </span> </span>
                                </td>
                                <td>
                                    <input class="form-control" type="number" required min="0" max="1000000" name="product[{{$index}}][price]" value="" placeholder="Enter Price">
                                </td>
                            </tr>
                            @endforeach
                            
                        </table>
                    </div>
                </div>
            </div>

            <div class="add-pro-submit">
                <input class="common-btn" type="submit">
                <a href="{{ url('/superadmin/trademan/shops/'.$shop->user_id)}}" class="common-btn">Cancel</a>
            </div>

        </div>
    </form>
</div>
</div>


@endsection
