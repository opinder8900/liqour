@extends('superadmin/layouts/app')
@section('content')

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-6">
            <h2>{{$shop->name}} Products</h2>
        </div>
    </div>  

    <div class="row mt-5 mb-80">
        <div class="col-lg-12 sa-cat-tabs">
        <div class="table custom-table">

            <table>
                <thead>
                    <tr>
                        <th> Name </th>
                        <th> Slug </th>
                        <th> Price </th>
                        <th> Status </th>
                        <th> Actions </th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($shop_products as $shop_product)
                        <tr>
                                <td>{{$shop_product->product->name}} </td>
                                <td> {{$shop_product->product->slug}}</td>
                                <td class="price-edit"> {{$shop_product->price}} </td>
                                <td>
                                <label class="switch">
                                    <input type="checkbox" data-href="{{ url('superadmin/shops/products/toggle_status/'.$shop_product->id) }}" @if($shop_product->status) checked="checked" @endif>
                                    <span class="slider round"></span>
                                </label> 
                                </td>
                                
                                <td>
									 <a href="javascript:void(0)" class="edit-product-price" data-id="{{$shop_product->id}}"> 
                                       Edit
                                    </a>
                                    <a href="{{ url('superadmin/shops/products/destroy/'.$shop_product->id) }}" onclick="return beforeCategoryDelete('{{$shop_product->product->name}}')"> 
                                        <i class="material-icons"> delete </i> 
                                    </a>
                                </td>
                            </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $shop_products->links() }} 
            </div>
        </div>
    </div>

</div>
</div>


@endsection
