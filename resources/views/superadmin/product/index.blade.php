@extends('superadmin/layouts/app')

@section('content')

<div class="f-w mt-80 product-tab-page">
    <div class="container-fluid">

    <div class="row">
		<div class="col-md-6">
			<h2>Products List</h2>
		</div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            @include('superadmin/layouts/tabs')
        </div>
    </div>
    <form action="{{ url('superadmin/products') }}" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ old('id',$product->id)}}">
        {{ csrf_field() }}
        <div class="cate-input-main">
            <div class="row">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Business</label>
                    <select data-target-href="{{url('superadmin/categories/by_business')}}" class="form-control product_business @error('business_id') is-invalid @enderror" name="business_id">
                        <option value="">Select Business</option>
                        @foreach($businesses as $id=>$name)
                        <option value="{{$id}}" @if($product->business_id==$id) selected="selected" @endif >{{$name}}</option>
                        @endforeach
                    </select>
                    @error('business_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Category</label>
                    <select data-target-href="{{url('superadmin/brands/by_category')}}" class="form-control product_category @error('category_id') is-invalid @enderror" name="category_id">
                        <option value="">Select Category</option>
                        @foreach($categories as $id=>$name)
                        <option value="{{$id}}" @if($product->category_id==$id) selected="selected" @endif >{{$name}}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Brand</label>
                    <select class="form-control product_brand @error('brand_id') is-invalid @enderror" name="brand_id">
                        <option value="">Select Brand</option>
                        @foreach($brands as $id=>$name)
                        <option value="{{$id}}" @if($product->brand_id==$id) selected="selected" @endif >{{$name}}</option>
                        @endforeach
                    </select>
                    @error('brand_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Name of Product</label>
                    <input type="text" onkeyup="replaceAsSlug('product_name','product_slug')" id="product_name"  name="name" required value="{{ old('name',$product->name)}}" class="form-control @error('name') is-invalid @enderror" placeholder="Enter Product Name">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Slug</label>
                    <input type="text" name="slug" required value="{{ old('slug',$product->slug)}}" class="form-control @error('slug') is-invalid @enderror" placeholder="Slug Name" id="product_slug">
                    @error('slug')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Size</label>
                    <div class="input-group">
                      <input type="text" name="size" required value="{{ old('size',$product->size)}}" class="form-control  @error('size') is-invalid @enderror" placeholder="Size" aria-label="Size" aria-describedby="basic-addon2">
                      <div class="input-group-append">
                        <select class="btn btn-outline-secondary" name="size_label">
                            <option value="ml" @if($product->size_label=='ml') selected="selected" @endif >ml</option>
                            <option value="lt" @if($product->size_label=='lt') selected="selected" @endif >ltr</option>
                            <option value="gm" @if($product->size_label=='gm') selected="selected" @endif >gm</option>
                            <option value="kg" @if($product->size_label=='kg') selected="selected" @endif >kg</option>
                        </select>
                      </div>
                      @error('size')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                </div>
                <div class="col-lg-4 sa-cat-tabs mt-5">
                    <label class="d-block">Upload Image @if($product->image) <img class="pro-img float-right mr-2" src="{{ URL::asset($product->image) }}"> @endif</label>
                    
                    <input type="file" class="form-control" accept="image/*"  name="image" placeholder="Upload Image">
                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="col-lg-4 sa-cat-tabs mt-5">
                    <input class="common-btn mt-4" type="submit" value="{{empty($product->id) ? 'Add' : 'Save'}} Product">
                    @if($product->id)
                    <a href="{{url('superadmin/products')}}" class="common-btn">Cancel</a>
                    @endif
                </div>
            </div>

        </div>
    </form>

    <div class="row mb-3">
		<div class="col-md-6">
			<h2>Products</h2>
		</div>
    </div>


    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">

            <div class="table custom-table">

                <table>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Category </th>
                            <th> Brand </th>
                            <th> Slug </th>
                            <th> Image </th>
                            <th> Size </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($products as $product)
                        <tr>
                            <td> {{$product->name}} </td>
                            <td> {{$product->category->name}} </td>
                            <td> {{$product->brand->name}} </td>
                            <td> {{$product->slug}} </td>
                            <td> @if($product->image) <img class="pro-img" src="{{ URL::asset($product->image) }}"> @endif </td>
                            <td> {{$product->size}} {{$product->size_label}} </td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" data-href="{{ url('superadmin/products/toggle_status/'.$product->id) }}" @if($product->status) checked="checked" @endif >
                                    <span class="slider round"></span>
                                </label> 
                            </td>
                            <td>
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="{{ url('superadmin/products/'.$product->id) }}"> 
                                            <img class="" src="{{ URL::asset('assets/images/edit.svg') }}"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="{{ url('superadmin/products/destroy/'.$product->id) }}" onclick="return beforeCategoryDelete('{{$product->name}}')"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>    
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }} 

            </div>
        </div>
    </div>



    

    </div>
</div>

@endsection
