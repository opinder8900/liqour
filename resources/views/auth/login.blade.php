@extends('layouts.front-header')
@section('body-class', 'login-page')
@section('content')
<div class="back-login-bg"></div>
<div class="f-w back-login-main col-lg-12 d-flex h-100vh">
<div class="container">
    <div class="row justify-content-center h-100vh pt-5 pb-5">
        <div class="col-md-8 login-outer my-auto">

                <div class="col-md-12 heading-common">
					<h2>
						{{ __('Login') }}
					</h2>
					<p>Please share your details to initiate the onboarding process.</p>
				</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-12">
                                <div class="input-icon-outer">
                                    <i class="material-icons">email</i>
                                    <input id="email" placeholder="Enter you email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                </div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-12">
                                <div class="input-icon-outer">
                                    <i class="material-icons">lock</i>
                                    <input id="password" placeholder="Enter password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="common-btn blue-btn">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>

                    <div class="f-w login-page-r">
                        <p> Don't have any account yet? click here to </p>
                        <a class="" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </div>

                </div>
            </div>
        </div>

        <!--div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>

                @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
            </div>
        </div-->
    </form>
</div>
</div>


@endsection
