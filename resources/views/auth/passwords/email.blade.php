@extends('layouts.front-header')
@section('body-class', 'login-page')
@section('content')

<div class="back-login-bg"></div>
<div class="f-w back-login-main col-lg-12 h-100vh">
<div class="container">
    <div class="row justify-content-center h-100vh pt-5 pb-5">
        <div class="col-md-8 login-outer my-auto">

        <div class="col-md-12 heading-common mb-5">
            <h2>
                {{ __('Reset Password') }}
            </h2>
        </div>

        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="form-group">
            <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

            <div class="col-md-12">
                <div class="input-icon-outer mb-4">
                    <i class="material-icons">email</i>
                    <input id="email" placeholder="Enter your email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                </div>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group mb-0">
            <div class="col-md-12">
                <button type="submit" class="common-btn blue-btn blue-bg w-100">
                    {{ __('Send Password Reset Link') }}
                </button>
            </div>
        </div>

        <div class="col-md-12 f-w login-page-r">
            <p> Back to login. Click here </p>
            <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>
        </div>
    </form>

</div>
</div>
</div>
</div>

@endsection
