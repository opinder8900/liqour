@extends('layouts.front-header')
@section('body-class', 'register-page')
@section('content')

<div class="back-login-bg"></div>
<div class="f-w back-login-main col-lg-12 d-flex h-100vh">

    <div class="container">
        <div class="row justify-content-center h-100vh pt-5 pb-5">
            <div class="col-md-8 login-outer my-auto">

                <div class="col-md-12 heading-common mb-5">
                    <h2>
                        {{ __('Register') }}
                    </h2>
                    <p>Please share your details to initiate the onboarding process.</p>
                </div>

                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group">
                        <label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">person</i>
                                <input id="name" placeholder="Enter your name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            </div>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-md-12 col-form-label">{{ __('E-Mail Address') }}</label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">email</i>
                                <input id="email" type="email" placeholder="Enter your email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                            </div>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="col-md-12 col-form-label">{{ __('Phone') }}</label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">local_phone</i>
                                <input id="name" placeholder="Enter your phone No:" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus maxlength="10">
                            </div>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-12 col-form-label">{{ __('Password') }}</label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">lock</i>
                                <input id="password" placeholder="Create you password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            </div>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-12 col-form-label">{{ __('Confirm Password') }}</label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-4">
                                <i class="material-icons">lock</i>
                                <input id="password-confirm" placeholder="Confirm your password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                    </div>

                    <div class="form-group mb-0 mt-4">
                        <div class="col-md-12">
                            <button type="submit" class="common-btn blue-btn blue-bg w-100">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
                <div class="col-md-12 f-w login-page-r">
                    <p> Already have an account. Click here </p>
                    <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
