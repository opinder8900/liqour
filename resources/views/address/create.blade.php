@extends('layouts.front-header')
@section('content')
<div class="my-acc-bg"></div>
<section class="my-acc-sec">
    <div class="container">
        <div class="acc-main">
            <div class="row">
                @include('layouts.sidebar')
                <div class="col-md-8">
                    <div class="my-acc-main">
                        <h4 class="f-w mb-5">Add New Address</h4>
                        @if($errors->any())
                        <p class="text-danger">Please fix errors below.</p>
                        @foreach($errors->all() as $err)
                        {{$err}}
                        @endforeach
                        @endif
                        <form id="address-form" name="address-form" class="address-form" method="post"  action="{{ action('AddressController@store') }}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="full_name" class="col-md-12 col-form-label">{{ __('Full Name') }}</label>
                                <div class="col-md-12">
                                    <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" value="{{ old('full_name') }}" required autocomplete="full_name" autofocus>
                                    @error('full_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="col-md-12 col-form-label">{{ __('Phone') }}</label>
                                <div class="col-md-12">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" maxlength="10">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="pin_code" class="col-md-12 col-form-label">{{ __('Pin Code') }}</label>
                                <div class="col-md-12">
                                    <input id="phone" type="text" class="form-control @error('pin_code') is-invalid @enderror" name="pin_code" value="{{ old('pin_code') }}" required autocomplete="pin_code" maxlength="6">
                                    @error('pin_code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address_line_1" class="col-md-12 col-form-label">{{ __('Address Line 1') }}</label>
                                <div class="col-md-12">
                                    <input id="phone" type="text" class="form-control @error('address_line_1') is-invalid @enderror" name="address_line_1" value="{{ old('address_line_1') }}" required>
                                    @error('address_line_1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address_line_2" class="col-md-12 col-form-label">{{ __('Address Line 2') }}</label>
                                <div class="col-md-12">
                                    <input id="phone" type="text" class="form-control @error('address_line_2') is-invalid @enderror" name="address_line_2" value="{{ old('address_line_2') }}">
                                    @error('address_line_2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="landmark" class="col-md-12 col-form-label">{{ __('landmark') }}</label>
                                <div class="col-md-12">
                                    <input id="phone" type="text" class="form-control @error('landmark') is-invalid @enderror" name="landmark" value="{{ old('landmark') }}" required>
                                    @error('landmark')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="country" class="col-md-12 col-form-label">{{ __('Country') }}</label>
                                <div class="col-md-12">
                                    <input name="country" id = "country" class="form-control" placeholder = "Country" type="text" value="India" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="state" class="col-md-12 col-form-label">{{ __('State') }}</label>
                                <div class="col-md-12">
                                    <select name="state" id = "state" class="form-control">
                                        @foreach($states as $state)
                                        <option value="{{$state->state_code}}">{{$state->state_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="city" class="col-md-12 col-form-label">{{ __('City') }}</label>
                                <div class="col-md-12">
                                    <select name="city" id = "city" class="form-control">
                                        @foreach($cities as $city)
                                        <option value="{{$city->id}}">{{$city->city_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="f-w add-new-add-outer mb-4">
                                <input name="default" id="default" class="" type="checkbox"> <span> Mark this as you default address </span>
                            </div>

                            <div class="f-w add-new-add-outer">
                                <input type="submit" value = "Add" id="submit" name="submit" class="common-btn blue-btn">
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>





    </div>
</section>


@endsection



