@extends('layouts.front-header')
@section('body-class', 'account-page')
@section('content')

@include('layouts.lower-tabs')

<div class="my-acc-bg"></div>
<section class="my-acc-sec">
    <div class="container">

        <div class="acc-main">
            <div class="row">
                @include('layouts.sidebar')

                <div class="col-md-8">
                    <div class="my-acc-main">
                        <h4 class="f-w mb-5">Delivery Address</h4>
                        @if($userAddresses)
                        @foreach($userAddresses as $address)
                        <div class="add-list">
                            <i class="material-icons">location_on</i>
                            <h4> {{$address->type}} </h4>
                            <p> {{$address->door_flat_number}}, {{$address->area}}, {{$address->city}} {{$address->state}} {{$address->pin_code}}, {{$address->country}} </p>
                            <p> Landmark: {{$address->landmark}} </p>
                            <form method="post" action="{{ route('address.destroy', $address->id) }}" onsubmit="return beforeAddressDelete()">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="form-group delete-shop">
                                    <input class="common-btn blue-btn" type="submit" class="" value="Delete">
                                </div>
                            </form>
                        </div>
                        @endforeach
                        @endif
                        <div class="">
                            <a href="#" class="common-btn blue-btn"> Add New Address </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

    @endsection


