@extends('tradesman/layouts/app')

@section('content')


<section class="f-w">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h2> Today's Sale </h2>
            </div>
		</div>
		
		<div class="row">
            <div class="col-md-12 mt-4">
                <div class="row">
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Sale</h4>
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4"> {{$todaySales->total}} </h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Cash On Delivary</h4> 
                            <i class="material-icons">list_alt</i>
                            <h2 class="mb-0 mt-4"> {{$codTodaySales->total}} </h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Online Payment</h4> 
                            <i class="material-icons">credit_card</i>
                            <h2 class="mb-0 mt-4"> @if($onlineTodaySales->total) {{$onlineTodaySales->total}} @else 0 @endif </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
        
        <div class="row mt-80">
            <div class="col-lg-12">
                <h2> Last Week </h2>
            </div>
		</div>
		
		<div class="row">
            <div class="col-md-12 mt-4">
                <div class="row">
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Sale</h4>
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4"> {{$todaySales->total}} </h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Cash On Delivary</h4> 
                            <i class="material-icons">list_alt</i>
                            <h2 class="mb-0 mt-4"> {{$codTodaySales->total}} </h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Online Payment</h4> 
                            <i class="material-icons">credit_card</i>
                            <h2 class="mb-0 mt-4"> @if($onlineTodaySales->total) {{$onlineTodaySales->total}} @else 0 @endif </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row mt-80">
            <div class="col-lg-12">
                <h2> Last Month </h2>
            </div>
		</div>
		
		<div class="row">
            <div class="col-md-12 mt-4">
                <div class="row">
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Sale</h4>
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4"> {{$todaySales->total}} </h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Cash On Delivary</h4> 
                            <i class="material-icons">list_alt</i>
                            <h2 class="mb-0 mt-4"> {{$codTodaySales->total}} </h2>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Online Payment</h4> 
                            <i class="material-icons">credit_card</i>
                            <h2 class="mb-0 mt-4"> @if($onlineTodaySales->total) {{$onlineTodaySales->total}} @else 0 @endif </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>


	</div>
</section>


@endsection
