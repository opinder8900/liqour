

@extends('tradesman/layouts/app')
@section('content')
@if($errors->any())
@foreach($errors->all() as $err)
<div class="error-msg">
    <span class="alert alert-danger">
        {{$err}}
    </span>
</div>
@endforeach
@endif
<div class="f-w mt-80">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="col-md-6 mb-5">

                <div class="login-outer">

                    <h2 class="mb-5"> Create Shop </h2>

                    <form action="{{ action('Tradesman\ShopController@store') }}" method="post" enctype="multipart/form-data" id="shop_form">
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Shop name') }}</label>
                            <div class="col-md-12">

                                <input type="text" name="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" required="required" id="name"> 
                                
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_slug" class="col-md-12 col-form-label">{{ __('Shop slug') }}</label>
                            <div class="col-md-12">

                                <input type="text" name="slug" value="{{ old('slug') }}" class="form-control @error('slug') is-invalid @enderror" required="required" id="slug" >
                                
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Business') }}</label>
                            <div class="col-md-12">
                                <select class="form-control @error('business_id') is-invalid @enderror" name="business_id" value="{{ old('business_id') }}" required>
                                    <option value = "">Select Business</option>
                                    @foreach($businesses as $id=>$name)
                                    <option value="{{$id}}">{{$name}}</option>
                                    @endforeach
                                </select>
                                @error('business_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Shop Email') }}</label>
                            <div class="col-md-12">
                                <input type="text" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" required="required">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Password') }}</label>
                            <div class="col-md-12">
                                <input type="password" name="password" value="{{ old('password') }}" class="form-control @error('password') is-invalid @enderror" required="required">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Address') }}</label>
                            <div class="col-md-12">
                                <input type="text" name="address" value="{{ old('address') }}" class="form-control @error('address') is-invalid @enderror" required="required">
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('State') }}</label>
                            <div class="col-md-12">
                                <select id="state_code" class="form-control @error('state_code') is-invalid @enderror" name="state_code" value="{{ old('state_code') }}" required>
                                    
                                    <option value = "" selected="selected">Select State</option>
                                    @foreach($states as $state)
                                    <option value="{{$state->state_code}}">{{$state->state_name}}</option>
                                    @endforeach
                                </select>
                                @error('state_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Cities') }}</label>
                            <div class="col-md-12">
                                <select id="city_id" class="form-control @error('city_id') is-invalid @enderror select-state" name="city_id" value="{{ old('city_id') }}" required>
                                    <option value = "" selected="selected">Select City</option>
                                    @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->city_name}}</option>
                                    @endforeach
                                </select>
                                @error('state_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Zip Code') }}</label>
                            <div class="col-md-12">
                                <input type="text" name="zip" value="{{ old('zip') }}" class="form-control @error('zip') is-invalid @enderror" required="required" maxlength="6">
                                @error('zip')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Primary Phone') }}</label>
                            <div class="col-md-12">
                                <input type="text" name="primary_phone" value="{{ old('primary_phone') }}" class="form-control @error('primary_phone') is-invalid @enderror" required="required" maxlength="10" id="primary_phone">
                                @error('primary_phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Secondary Phone') }}</label>
                            <div class="col-md-12">
                                <input type="text" name="secondary_phone" value="{{ old('secondary_phone') }}" class="form-control @error('secondary_phone') is-invalid @enderror">
                                @error('secondary_phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('type') }}</label>
                            <div class="col-md-12">
                                <select id="type" class="form-control @error('type') is-invalid @enderror " name="type" value="{{ old('type') }}" required>
                                    <option value = "liquor" selected="selected">Liquor</option>
                                    <option value = "grocery" >Grocery</option>
                                    <option value = "shopping" >Shopping</option>
                                </select>
                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Open At') }}</label>
                            <div class="col-md-12">
                                <input type="text" name="open_at" id="open_at" value="{{ old('open_at') }}" class="form-control @error('open_at') is-invalid @enderror timepicker">
                                @error('open_at')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Close At') }}</label>
                            <div class="col-md-12">
                                <input type="text" id="close_at" name="close_at" value="{{ old('close_at') }}" class="form-control @error('close_at') is-invalid @enderror timepicker">
                                @error('close_at')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('Image') }}</label>
                            <div class="col-md-12">
                                <input type="file" name="image" value="{{ old('image') }}" class="form-control @error('image') is-invalid @enderror">
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shop_name" class="col-md-12 col-form-label">{{ __('License Number') }}</label>
                            <div class="col-md-12">
                                <input type="text" name="license_number" value="{{ old('license_number') }}" class="form-control @error('license_number') is-invalid @enderror">
                                @error('license_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group mb-0">
                            <div class="col-md-12">
                                {{ csrf_field() }}
                                <button type="submit" class="common-btn green-btn">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	 function timeToInt(time) {
            var now = new Date();
            var dt = (now.getMonth() + 1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + time;
            var d = new Date(dt);
            console.log(d);
            return d;
        }

        function checkDates() {
            if (($('#open_at').val() == '') || ($('#close_at').val() == '')) return true;

            var start = timeToInt($('#open_at').val());
            var end = timeToInt($('#close_at').val());
            console.log($('#open_at').val());
            console.log(start, end);
            if ((start == -1) || (end == -1)) {
                return false;
            }

            if (start >= end) {
                return false;
            }
            return true;
        }
        

    $(function(){
		
        $('.timepicker').timepicker({'scrollDefault': 'now'});

		 //$('#primary_phone').inputmask('(999)-999-9999');
    $(document).on("keyup","#name",function(){
		var shopName = $(this).val();
		var slug     = shopName.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
		$("#slug").val(slug);
    });
    
    $('.timepicker').on('change', function() {
                $('#shop_form').bootstrapValidator('revalidateField', 'open_at');
                $('#shop_form').bootstrapValidator('revalidateField', 'close_at');

            });

            $('#shop_form').bootstrapValidator({

                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {

                    open_at: {
                        validators: {
                            notEmpty: {
                                message: 'Please select open time'
                            },
                            callback: {
                                message: "Start time should be lower than end time",
                                callback: function(value, validator, $field) {
                                    return checkDates();
                                }
                            }
                        }
                    },

                    close_at: {
                        validators: {
                            notEmpty: {
                                message: 'Please select close time'
                            },
                            callback: {
                                message: "Close time should be greater than end time",
                                callback: function(value, validator, $field) {
                                    return checkDates();
                                }
                            }
                        }
                    },

                }

            });
});
   

</script>
@endsection

