@extends('tradesman/layouts/app')
@section('content')
@if($errors->any())
@foreach($errors->all() as $err)
<div class="error-msg">
<span class="alert alert-danger">
{{$err}}
</span>
</div>
@endforeach
@endif

<div class="f-w mt-80">
<div class="container-fluid">
	<div class="row justify-content-center">

    <div class="col-md-6 mb-5">

<div class="login-outer">

    <h2 class="mb-5"> Edit Shop </h2>
    
<form action="{{ route('stores.update', $shop->id) }}" method="post" enctype="multipart/form-data">
    {{ method_field('PUT') }}
  
    <div class="form-group">
        <label for="name" class="col-md-12 col-form-label">{{ __('Name') }}</label>
        <div class="col-md-12">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name', $shop->name) }}" required autocomplete="name" autofocus>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="slug" class="col-md-12 col-form-label">{{ __('Slug') }}</label>
        <div class="col-md-12">
            <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror" name="slug" value="{{ old('slug', $shop->slug) }}" required autocomplete="slug" autofocus>
            @error('slug')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="price" class="col-md-12 col-form-label">{{ __('Address') }}</label>
        <div class="col-md-12">
            <input id="text" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address',$shop->address) }}" required autocomplete="price" autofocus>
            @error('price')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="in_stock_quantity" class="col-md-12 col-form-label">{{ __('Zip') }}</label>
        <div class="col-md-12">
            <input id="zip" type="number" class="form-control @error('zip') is-invalid @enderror" name="zip" value="{{ old('zip',$shop->zip) }}" required autocomplete="zip" autofocus>
            @error('in_stock_quantity')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="image" class="col-md-12 col-form-label">{{ __('Image') }}</label>
        <div class="col-md-12">
            <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" autocomplete="image" autofocus>
            @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="col-md-12">
        @if($shop->image)
        <img src="{{ URL::to('/') }}/{{ $shop->image }}" class="cat-image-parent mt-2">
        @endif
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <label>
                @if($shop->status)
                <input type="checkbox" value="1" checked="checked"  name="status">
                @else
                <input type="checkbox" value="1"  name="status"> 
                @endif
                Status
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-md-12 col-form-label">{{ __('Primary Phone') }}</label>
        <div class="col-md-12">
            <input id="primary_phone" type="text" class="form-control @error('primary_phone') is-invalid @enderror" name="primary_phone" value="{{ old('primary_phone',$shop->primary_phone) }}">
            @error('primary_phone')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-md-12 col-form-label">{{ __('Secondary Phone') }}</label>
        <div class="col-md-12">
            <input id="secondary_phone" type="text" class="form-control @error('secondary_phone') is-invalid @enderror" name="secondary_phone" value="{{ old('secondary_phone',$shop->secondary_phone) }}">
            @error('secondary_phone')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-md-12 col-form-label">{{ __('Open At') }}</label>
        <div class="col-md-12">
            <input id="open_at" class="form-control @error('open_at') is-invalid @enderror" name="open_at" value="{{ old('open_at',$shop->open_at) }}">
            @error('open_at')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-md-4 col-form-label">{{ __('Close At') }}</label>
        <div class="col-md-12">
            <input id="close_at" type="text" class="form-control @error('close_at') is-invalid @enderror" name="close_at" value="{{ old('close_at',$shop->close_at) }}">
            @error('close_at')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-md-4 col-form-label">{{ __('License Number') }}</label>
        <div class="col-md-12">
            <input id="license_number" type="text" class="form-control @error('license_number') is-invalid @enderror" name="license_number" value="{{ old('license_number',$shop->license_number) }}">
            @error('license_number')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group mb-0">
        <div class="col-md-12">
            {{ csrf_field() }}
            <button type="submit" class="common-btn blue-btn w-100">
                {{ __('Save') }}
            </button>
        </div>
    </div>
</form>

</div>
</div>
</div>
</div>
</div>

@endsection
