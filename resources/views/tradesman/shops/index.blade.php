@extends('tradesman/layouts/app')

@section('content')
<div class="f-w">
<div class="container-fluid">

	<div class="row mt-80">
		<div class="col-md-6">
			<h2>Shops List</h2>
		</div>
		<div class="col-md-6 text-right">

			<a class="common-btn green-btn" href="{{ action('Tradesman\ShopController@create') }}"> <i class="material-icons">add_circle_outline</i> <span> Add Shop </span> </a>

		</div>
	</div>  
    
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="table  custom-table">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Primary Phone</th>
                    <th>Seconday Phone</th>
                    <th>Open At</th>
                    <th>Close At</th>
                    <th>License Number</th>
                    <th>Actions</th>

                </tr>
                @if( $shops )
                @foreach($shops as $shop)	
                <tr>
                    <td>{{$shop->name}}</td>
                    <td>{{$shop->address}}</td>
                    <td>{{$shop->city->city_name}}</td>
                    <td>{{$shop->state->state_name}}</td>
                    <td>{{$shop->primary_phone}}</td>
                    <td>{{$shop->secondary_phone}}</td>
                    <td>{{$shop->open_at}}</td>
                    <td>{{$shop->close_at}}</td>
                    <td>{{$shop->license_number}}</td>
                    <td>
                        <div class="dropdown dropleft">
                            <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="more-icon" src="{{ URL::asset('assets/images/more.svg') }}">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a href="{{ route('stores.show', $shop->id) }}"> 
								<img class="" src="{{ URL::asset('assets/images/eye.svg') }}"> 
								<span> Show </span> 
							</a>
                            <a href="{{ route('stores.edit', $shop->id) }}"> 
								<img class="" src="{{ URL::asset('assets/images/edit.svg') }}"> 
								<span> Edit </span> 
							</a>
                            <form method="post" action="{{ route('stores.destroy', $shop->id) }}" onsubmit="return beforeProductDelete('{{$shop->name}}')">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <div class="form-group delete-shop">
                                <img class="" src="{{ URL::asset('assets/images/delete.svg') }}">
                                <input type="submit" class="" value="Delete">
                            </div>
                            </form>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
            {{$shops->links()}}
            </div>
        </div>

</div>
</div>


@endsection
