@extends('tradesman/layouts/app')

@section('content')

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-lg-12">
                <h2> Orders List </h2>
            </div>

            <div class="col-lg-12 mt-5">
                <div class="table custom-table">
                    <table class="">
                        <tr>

                            <th>Order ID</th>
                            <th>Bill-to Name</th>
                            <th>Purchase Date</th>
                            <th>total</th>
                            <th>Status</th>
                            <th>Payment Mode</th>
                            <th>Delivered On</th>
                            <th>Created At</th>
    <!--                        <th>Actions</th>-->

                        </tr>
                        @if( $orders )
                        @foreach($orders as $order)
                        <tr>

                            <td>{{$order->id}}</td>
                            <td>{{$order->user->name}}</td>
                            <td>{{$order->tax}}</td>
                            <td>{{$order->total}}</td>
                            <td>
                                {{$order->status}}
                            </td>
                            <td>
                                {{$order->payment_mode}}
                            </td>
                            <td>{{$order->delivered_on}}</td>
                            <td>{{$order->created_at}}</td>

                            <!--<td>-->
                                <!--<a href="{{ route('orders.show', $order->id) }}">Show</a>-->
                                <!--a href="{{ route('orders.edit', $order->id) }}">Edit</a>
                                <            <form method="post" action="{{ route('orders.destroy', $order->id) }}" onsubmit="return beforeProductDelete('{{$order->name}}')">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                                    </div>
                                </form>-->
                            <!--</td>-->

                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
                {{ $orders->links() }}

            </div>
        </div>
    </div>
</div>
@endsection