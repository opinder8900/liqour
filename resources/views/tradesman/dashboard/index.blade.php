@extends('tradesman/layouts/app')

@section('content')

<section class="f-w">
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                <h2> dashboard </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 mt-5">
                <div class="row">
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Shops</h4>
                            <i class="material-icons">store_mall_directory</i>
                            <h2 class="mb-0 mt-4"> {{$totalShops}} </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Orders</h4> 
                            <i class="material-icons">list_alt</i>
                            <h2 class="mb-0 mt-4"> {{$totalOrders}} </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Today's sales</h4> 
                            <i class="material-icons">local_grocery_store</i>
                            <h2 class="mb-0 mt-4"> {{$todaySales->total}}  </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Sale</h4> 
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4"> {{$totalSales->total}} </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection
