@extends('layouts.front-header')

@section('content')
<div class="f-w inner-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="f-w text-center"> payment </h2>
			</div>
		</div>
	</div>
</div>

<div class="f-w bread inner-bread">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <ul class="f-w">
                    <li> <a href="{{ url('/') }}"> Home </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <a href="#"> Products </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <a href="{{route('cart-index')}}"> Cart </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <p> Payment </p> </li>
                </ul>
            </div>
        </div>

    </div>
</div>

<div class="f-w m-550-outer">
<div class="container">
<div class="row justify-content-center mb-80 mt-80">

        <div class="col-md-6">

            <h2>Order Summary</h2>

            <div class="table mt-5">
                    <!--tr>
                        <th> Items </th>
                        <th> Name </th>
                        <th> Quantity </th>
                        <th> Price </th>
                        <th> Total </th>
                    </tr-->
                    @foreach($items as $item)
                    <div class="cart-items-outer">
                        <div class="cart-item-img"> <img src="{{ URL::asset($item->image) }}"> </div>
                        <div class="cart-item-content">
                            <h4 class="mb-3"> {{$item->name}} </h4>
                            <p class="mb-0"> Quantity: <span> {{$item->qty}} </span> </p>
                            <p class="mb-0"> Price: <span> {{$item->price}} </span> </p>
                            <!--td> {{$item->total}} </td-->
                        </div>
                    </div>
                    @endforeach
            </div>
        
        </div>

        <div class="col-md-6">
            <h2 class="f-w mb-4"> Delivery address </h2>
            @if(count($addresses) == 0)
            <!--form id="address-form" name="address-form" class="address-form" method="post"  action="{{ action('AddressController@store') }}">
                <input type="hidden" name="redirect_url" value="{{Request::url()}}"/>
                {{csrf_field()}}
                <div class="form-group">
                    <label for="full_name" class="col-md-12 col-form-label">{{ __('Full Name') }}</label>
                    <div class="col-md-12">
                        <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" value="{{ old('full_name') }}" required autocomplete="full_name" autofocus>
                        @error('full_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="phone" class="col-md-12 col-form-label">{{ __('Phone') }}</label>
                    <div class="col-md-12">
                        <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" maxlength="10">
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="pin_code" class="col-md-12 col-form-label">{{ __('Pin Code') }}</label>
                    <div class="col-md-12">
                        <input id="phone" type="text" class="form-control @error('pin_code') is-invalid @enderror" name="pin_code" value="{{ old('pin_code') }}" required autocomplete="pin_code" maxlength="6">
                        @error('pin_code')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="address_line_1" class="col-md-12 col-form-label">{{ __('Address Line 1') }}</label>
                    <div class="col-md-12">
                        <input id="phone" type="text" class="form-control @error('address_line_1') is-invalid @enderror" name="address_line_1" value="{{ old('address_line_1') }}" required>
                        @error('address_line_1')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="address_line_2" class="col-md-12 col-form-label">{{ __('Address Line 2') }}</label>
                    <div class="col-md-12">
                        <input id="phone" type="text" class="form-control @error('address_line_2') is-invalid @enderror" name="address_line_2" value="{{ old('address_line_2') }}">
                        @error('address_line_2')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="landmark" class="col-md-12 col-form-label">{{ __('landmark') }}</label>
                    <div class="col-md-12">
                        <input id="phone" type="text" class="form-control @error('landmark') is-invalid @enderror" name="landmark" value="{{ old('landmark') }}" required>
                        @error('landmark')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="country" class="col-md-12 col-form-label">{{ __('Country') }}</label>
                    <div class="col-md-12">
                        <input name="country" id = "country" class="form-control" placeholder = "Country" type="text" value="India" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label for="state" class="col-md-12 col-form-label">{{ __('State') }}</label>
                    <div class="col-md-12">
                        <select name="state" id = "state" class="form-control">
                            @foreach($states as $state)
                            <option value="{{$state->state_code}}">{{$state->state_name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="city" class="col-md-12 col-form-label">{{ __('City') }}</label>
                    <div class="col-md-12">
                        <select name="city" id = "city" class="form-control">
                            @foreach($cities as $city)
                            <option value="{{$city->id}}">{{$city->city_name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="f-w add-new-add-outer mb-4">
                    <input name="default" id="default" class="" type="checkbox"> <span> Mark this as you default address </span>
                </div>

                <div class="f-w add-new-add-outer">
                    <input type="submit" value = "Add" id="submit" name="submit" class="common-btn blue-btn">
                </div>

            </form-->
        </div>

</div>
</div>
</div>



<div class="f-w">
    <div class="col-lg-12">
        <div class="row">

            @else
            <div class="checkout-add-main">
                @if($errors->any())
                @foreach($errors->all() as $err)
                {{$err}}
                @endforeach
                @endif


                @if(count($items))
                <form action="{{ route('place-order') }}" method="post">
                    <div class="checkout-add-inner">
                        
                        <h4 class="p-r"> <i class="material-icons">location_on</i> {{$address->type}}</h4>
                        {{$address->door_flat_number}},
                        {{$address->area}},
                        {{$address->landmark}},
                        {{$address->city}},
                        {{$address->state}},
                        {{$address->country}}
                    </div>

                    <div class="f-w checkout-add-payment">

                        <h2 class="mt-4 mb-4"> Payment </h2>

                        {{ csrf_field() }}
                        <label>
                            <input type="radio" name="payment_mode" value="cod"> COD
                        </label>

                        <div class="f-w mt-4">
                            <input class="common-btn blue-btn" type="submit" name="checkout" value="Place Order">
                        </div>

                    </div>

                </form>
                @endif

            </div>

        </div>
    </div>
</div>


                    @endif

            @endsection
