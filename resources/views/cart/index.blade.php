@extends('layouts.front-header')

@section('content')
@include('layouts.lower-tabs')
<div class="f-w inner-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="f-w text-center"> cart </h2>
			</div>
		</div>
	</div>
</div>

<div class="f-w bread inner-bread">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <ul class="f-w">
                    <li> <a href="{{ url('/') }}"> Home </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <a href="#"> Products </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <p> Cart </p> </li>
                </ul>
            </div>
        </div>

    </div>
</div>

<div class="f-w mb-5 m-550-outer">
    <div class="container">
        <div class="row mt-80 cart-headin-brn mb-5">
            <div class="col-md-6">
                <div class="list-icon">
                    <i class="material-icons">store</i>
                    <h4> Shop Name Here </h4>
                </div>
            </div>
            <div class="col-md-6 text-right">
                <a href="#" class="common-btn blue-btn"> Continue Shopping </a>
            </div>
        </div>
        <div class="row justify-content-center cart-col-outer">

                <div class="col-md-6">
                    <!--h2>Cart</h2-->
                    @if(count($items))
                    <div class="table">
                    <!--tr>
                        <th> Items </th>
                        <th> Name </th>
                        <th> Quantity </th>
                        <th> Price </th>
                        <th> Total </th>
                        <th> Action </th>
                    </tr-->
                    @foreach($items as $item)
                    <div class="cart-items-outer">
                        <div class="cart-item-img"> <img src="{{ URL::asset($item->image) }}"> </div>
                        <div class="cart-item-content">
                            <h4 class="m-0"> {{$item->name}} </h4>
                            <p class="m-0"> Quantity: <span> {{$item->qty}} </span> </p>
                            <p class="m-0"> Price: <span> {{$item->price}} </span> </p>
                            <!--p> {{$item->total}} </p-->
                        </div>
                        <div class="cart-item-delete"> 
                            <a class="text-danger" href="{{ route('delete-cart-item',['id' =>$item->__raw_id]) }}"> 
                                <span> <i class="material-icons">delete</i> </span> 
                            </a>
                        </div>
                    </div>
                    @endforeach
                    </div>
                </div>  

                <div class="col-lg-6">

                    <div class="table no-border cart-table">
                        <table>
                            <tr>
                                <th> Subtotal </th>
                                <td> {{$cart_total}} </td>
                            </tr>
                            <tr>
                                <th> Tax </th>
                                <td> 0.00 </td>
                            </tr>
                            <tr>
                                <th> Total </th>
                                <td> {{$cart_total}} </td>
                            </tr>
                        </table>
                        <div class="scheck-out-btn">
                            @if(count($items))
                            <form action="{{ action('AddToCart@checkout') }}" method="get">
                                <button class="common-btn blue-btn w-100" type="submit" name="page" value="checkout">Checkout</button>
                            </form>
                            @endif
                        </div>

                    </div>
                </div>

                </div>
                @else
                <p>Your cart is empty.</p>
                <a class="btn btn-success" href="{{ url('/') }}">Continue Shopping</a>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection
