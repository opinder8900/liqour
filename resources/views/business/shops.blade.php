@extends('layouts.front-header')

@section('content')
@include('layouts.lower-tabs')
<div class="f-w inner-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
                <h2 class="f-w text-center"> Shops in {{$city}} </h2>
			</div>
		</div>
	</div>
</div>

<div class="f-w bread inner-bread">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <ul class="f-w">
                    <li> <a href="{{ url('/') }}"> Home </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <p> Shops </p> </li>
                </ul>
            </div>
        </div>

    </div>
</div>
<section class="front-shops-page">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">

                <div class="f-w search-common">
                    <form action="" method="GET">
                        <div class="front-search-out">
                            <i class="material-icons">search</i>
                            <input  class="f-w" placeholder="Search for stores" type="text" value="{{ app('request')->input('search')}}" name="search" required/>
                            <button class="common-btn serch-front-btn" type="submit">Search</button>
                        </div>
                        <a href="{{ route('business-shops',['business'=>$business]) }}"> <i class="material-icons">close</i> Clear <span> Search </span></a>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">


            @foreach($shops as $shop)
            @php
				$checkOpen = in_range(strtotime($shop->open_at), strtotime($shop->close_at)) ? "out-of-time" : "";
				
            @endphp
            <div class="col-lg-6 mt-5 {{$checkOpen }}">
                <a href="{{ route('business-shop-products',['business' => $business,'shop' => $shop->slug])}}">
                    <div class="listing-outer">
                        <div class="listing-img">
                            @if($shop->image)
                            <img src="{{ URL::to('/') }}/{{ $shop->image }}" class="shop-image-listing">
                            @endif
                        </div>
                        <div class="listing-text">
                            <h4> {{$shop->name}}</h4>
                            <!--p class="mb-0"> All kind of liquor </p-->
                            <p class="mb-0"> Timings: {{ date('h:i a',(strtotime($shop->open_at)))}} – {{ date('h:i a',(strtotime($shop->close_at)))}}    </p>
                            <p class="mb-2"> {{$shop->address}}, {{$shop->city->name}} </p>

<!--
                            <h6> Lat: {{$shop->latitude }} </h6>
                            <h6> Long: {{$shop->longitude }}  </h6>
-->
                           
                            <h6> Distance: {{round($shop->distance,2) }} km </h6>

                        </div>
                    </div>
                </a>
               
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
