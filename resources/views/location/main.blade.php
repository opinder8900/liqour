<div class="row text-center">
    @foreach($businesses as $business)
    <div class="col-md-4">
        <a class="bus-home-outer" href="{{ route('city-business-shops',['city' => $city, 'business' => $business->slug]) }}">
            <div class="cat-img-home">
                <img src="{{ URL::asset($business->image) }}">
            </div>
            <div class="cat-img-home">
                <h4> {{$business->name}} </h4>
            </div>
        </a>
    </div>
    @endforeach
</div>