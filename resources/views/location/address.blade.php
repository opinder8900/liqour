@if (!Auth::guest())
<div class="set-location-home-outer {{Session::has('location') ? 'd-none':''}}">
    <div class="set-location-left-side">
        <i class="material-icons set-location-home-close">highlight_off</i>
        <div class="pac-card" id="pac-card1">
            <div>
                <div id="title">
                </div>
                <div id="type-selector" class="pac-controls">
 
                </div>

            </div>
            <div id="pac-container" class="pac-loc-city">
                <img class="loc-icon" src="{{ URL::asset('assets/images/loc-ion.svg') }}">
                <input class="form-control mb-4" id="pac-input" type="text" placeholder="Enter your city" autocomplete="off">
            </div>
        </div>
        <p>Lat:{{Session::get('location.latitude')}}</p>
        <p>Long:{{Session::get('location.longitude')}}</p>
        <div id="add-address-interface-wrapper" class="d-none">
            <div id="left-map-wrapper" style="width:300px;height:300px;">
                <div id="map"></div>
            </div>
            <div id="location-form-wrapper">
                <form name="address-form-ajax" id="address-form" method="post"  action="{{ route('add-address-ajax') }}">
                    {{csrf_field()}}
                    <div class="d-none">
                        <input type="text" name="latitude">
                        <input type="text" name="longitude">

                    </div>
                    <!--input type="text" class="form-control mb-3" name="formatted_address" readonly-->
                    <div class="input-icon-outer">
                        <i class="material-icons">home</i>
                        <input type="text" class="form-control mb-3" name="door_flat_number" required placeholder="Door/Flat No.">
                    </div>
                    <div class="input-icon-outer">
                        <i class="material-icons">apartment</i>
                        <input type="text" class="form-control mb-3" name="area" required placeholder="Your Area">
                    </div>
                    <div class="input-icon-outer">
                        <i class="material-icons">location_city</i>
                        <input type="text" class="form-control mb-3" name="landmark" required placeholder="Landmark">
                    </div>

                    <div class = "btn-group mb-3 p-r w-100">
                        <div class="set-loc-home-tb-btn">
                            <button type="button" class="btn btn-default address-type address-type-first" data-value="home">Home</button>
                            <button type="button" class="btn btn-default address-type address-type-middle" data-value="office">Office</button>
                            <button type="button" class="btn btn-default address-type address-type-last" data-value="other">Other</button>
                        </div>
                        <div  class="d-none" id="other-part">
                            <!--p> Other </p-->
                            <input class="form-control" type="text" name="type" value="home" placeholder="Dad’s home, my man cave" maxlength="8" required>
                            <button type="button" name="close">Close</button>
                        </div>
                    </div>
                    <div class="d-none text-danger address-error">
                        Unable to save address. Please refresh page and try again.
                    </div>
                    <button type="submit" class="common-btn blue-btn w-100 mb-80" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing Order">Save Address & Proceed</button>
                    
                </form>

            </div>

        </div>
        <div id="saved-addresses-list">
            <div class="front-brant-n sav-head-main">
                <p>Saved Addresses</p>
            </div>
            @if(count($addresses))
            <div id="saved-addresses" class="">
                @foreach($addresses  as $address)
                <div class="save-add-list">
                    <a href="{{route('set-address',['id' => $address->id])}}">
                        <div>
                            <div class="first-loc-icon p-r">
                                <i class="material-icons">location_on</i>
                                <h6> {{$address->type}} </h6>
                            </div>
                            <p> {{$address->door_flat_number}} </p>
                            <p> {{$address->area}} </p>
                            <p> {{$address->landmark}} </p>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
</div>
@endif