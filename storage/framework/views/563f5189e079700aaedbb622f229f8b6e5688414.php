<?php $__env->startSection('content'); ?>

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-lg-12">
                <h2> Orders List </h2>
            </div>

            <div class="col-lg-12 mt-5">
                <div class="table custom-table">
                    <table class="">
                        <tr>

                            <th>Order ID</th>
                            <th>Bill-to Name</th>
                            <th>Purchase Date</th>
                            <th>total</th>
                            <th>Status</th>
                            <th>Payment Mode</th>
                            <th>Delivered On</th>
                            <th>Created At</th>
    <!--                        <th>Actions</th>-->

                        </tr>
                        <?php if( $orders ): ?>
                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>

                            <td><?php echo e($order->id); ?></td>
                            <td><?php echo e($order->user->name); ?></td>
                            <td><?php echo e($order->tax); ?></td>
                            <td><?php echo e($order->total); ?></td>
                            <td>
                                <?php echo e($order->status); ?>

                            </td>
                            <td>
                                <?php echo e($order->payment_mode); ?>

                            </td>
                            <td><?php echo e($order->delivered_on); ?></td>
                            <td><?php echo e($order->created_at); ?></td>

                            <!--<td>-->
                                <!--<a href="<?php echo e(route('orders.show', $order->id)); ?>">Show</a>-->
                                <!--a href="<?php echo e(route('orders.edit', $order->id)); ?>">Edit</a>
                                <            <form method="post" action="<?php echo e(route('orders.destroy', $order->id)); ?>" onsubmit="return beforeProductDelete('<?php echo e($order->name); ?>')">
                                    <?php echo e(csrf_field()); ?>

                                    <?php echo e(method_field('DELETE')); ?>

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                                    </div>
                                </form>-->
                            <!--</td>-->

                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </table>
                </div>
                <?php echo e($orders->links()); ?>


            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('tradesman/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/tradesman/order/payments.blade.php ENDPATH**/ ?>