<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Shops</h2>
			<p>Trademan - <a href="<?php echo e(url('superadmin/trademan')); ?>"><?php echo e($shops->name); ?></a></p>
		</div>
	</div>  

    <div class="row mt-5 mb-80">
        <div class="col-lg-12 sa-cat-tabs">
		<div class="table custom-table">

			<table>
				<thead>
					<tr>
						<th> Name </th>
						<th> Address </th>
						<th> Phone </th>
						<th> Email </th>
						<th> licence </th>
						<th> Status </th>
						<th> Actions </th>
					</tr>
				</thead>
				
				<tbody>
					<?php if($shops): ?>
						<?php $__currentLoopData = $shops->shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
									<td><?php echo e($shop->name); ?> </td>
									<td> <?php echo e($shop->address); ?></td>
									<td> <?php echo e($shop->primary_phone); ?> </td>
									<td> <?php echo e($shop->shopKeeper->email); ?>  </td>
									<td>  <?php echo e($shop->license_number); ?>  </td>
									<td>
									<label class="switch">
										<input type="checkbox"data-href="<?php echo e(url('superadmin/trademan/shop/toggle_status/'.$shop->shopKeeper->id)); ?>" <?php if($shop->shopKeeper->status): ?> checked="checked" <?php endif; ?>>
										<span class="slider round"></span>
									</label> 
									</td>
									<td>
										<div class="dropdown dropleft">
											<div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
											</div>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<a href="<?php echo e(url('superadmin/shops/products/add/'.$shop->id)); ?>"> 
													<i class="material-icons"> add_circle </i> 
													<span> Add Products </span> 
												</a>
												<a href="<?php echo e(url('superadmin/shops/products/'.$shop->id)); ?>"> 
													<i class="material-icons"> visibility </i> 
													<span> View Products </span> 
												</a>
												<a href="<?php echo e(url('superadmin/trademan/shops/delete/'.$shop->id)); ?>"> 
													<i class="material-icons"> delete </i> 
													<span> Delete </span> 
												</a>
											</div>
										</div>
									</td>
								</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				
				
				</tbody>
			</table>
                

            </div>
        </div>
    </div>

</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/trademan/show.blade.php ENDPATH**/ ?>