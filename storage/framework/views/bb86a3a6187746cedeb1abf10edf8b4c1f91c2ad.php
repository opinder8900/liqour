<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-80">

            <h2>Order Placed</h2>
            <p>Thanks your order has been placed</p>
            <a class="btn btn-success" href="<?php echo e(url('/')); ?>">Continue Shopping</a>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/order/thankyou.blade.php ENDPATH**/ ?>