<?php $__env->startSection('content'); ?>
<section>
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="inner-heading f-w mb-5">
                    <h2> Restaurants in Mohali </h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-5">

                <div class="f-w search-common">
                    <form action="" method="GET">
                        <div class="front-search-out">
                            <i class="material-icons">search</i>
                            <input  class="f-w" placeholder="Search for stores" type="text" value="<?php echo e(app('request')->input('search')); ?>" name="search" required/>
                            <button class="common-btn serch-front-btn" type="submit">Search</button>
                        </div>
                        <a href="<?php echo e(route('liquor-shops',['city' => $city])); ?>"> <i class="material-icons">close</i> Clear <span> Search </span></a>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">


            <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-lg-6 mb-5">
                <a href="<?php echo e(route('liquor-shop-products',['city'=> $city,'liquor_or_grocery' => 'liquor','shop_slug' => $shop->slug])); ?>">
                    <div class="listing-outer">
                        <div class="listing-img">
                            <?php if($shop->image): ?>
                            <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($shop->image); ?>" class="shop-image-listing">
                            <?php endif; ?>
                        </div>
                        <div class="listing-text">
                            <h4> <?php echo e($shop->name); ?></h4>
                            <p class="mb-0"> All kind of liquor </p>
                            <p class="mb-2"> Timings: <?php echo e($shop->open_at); ?> – <?php echo e($shop->close_at); ?> midnight (Mon-Sun)  </p>
                            <h6> <?php echo e($shop->address); ?>, <?php echo e($shop->city->name); ?> </h6>
                        </div>
                    </div>
                </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/liquor/shops.blade.php ENDPATH**/ ?>