<?php $__env->startSection('content'); ?>


<div class="f-w mt-80">
<div class="container-fluid">
    
    <div class="row">

		<div class="col-md-6">
			<h2> Reports </h2>
		</div>
		
		<div class="col-md-6 report-sp-tr-out">
			<form action="" method="GET">
				<select  class="form-control " name="search">
					<option value="today">Today</option>
					<option value="week">Week</option>
					<option value="month">Month</option>
				</select>                    
				<button class="common-btn" type="submit">Search</button>                        
			</form>
		</div>
		
		<div class="col-md-12 mt-5">
			<div class="table custom-table">
				<table class="">
					<thead>
					<tr>
						<th>Trademan Name</th>
						<th>Sale</th>
					</tr>
					</thead>
					<tbody>
					<?php if($reports): ?>
						<?php $__currentLoopData = $reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $report): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
									<td><?php echo e($report->trademan_name); ?></td>
									<td><?php echo e($report->total); ?></td>
							</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	
</div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/customer/report.blade.php ENDPATH**/ ?>