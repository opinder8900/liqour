<?php $__env->startSection('content'); ?>

<script>
    $(document).ready(function(){

        $('.shop-pro-stock input:checkbox').change(function(){
        if($(this).is(':checked')) 
            $(this).parent().parent().addClass('selected'); 
        else 
            $(this).parent().parent().removeClass('selected')
        });

    });
</script>

<div class="f-w">
    <div class="container-fluid">
        <form action="" method="GET">
            <div class="row mt-80">
                <div class="col-lg-4">
                    <h2> Products List </h2>
                </div>
                <div class="col-lg-2 text-right">
                    <div class="search-common back-search p-0">
                        <div class="inner-search-rl">
                            <select  data-target-href="<?php echo e(url('superadmin/brands/by_category')); ?>" class="form-control product_category" name="category_id">
                                <option value="">All Categories</option>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($id); ?>" <?php if(app('request')->input('category_id') == $id): ?> selected="selected" <?php endif; ?> ><?php echo e($name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 text-right">
                    <div class="search-common back-search p-0">
                        <div class="inner-search-rl">
                            <select class="form-control product_brand" name="brand_id">
                                <option value="">All Brands</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 text-right">
                    <div class="search-common back-search">
                        <div class="inner-search-rl">
                            <input class="form-control" type="text" value="<?php echo e(app('request')->input('search')); ?>" name="search" placeholder="Search Product"/>
                            <button class="common-btn serch-front-btn" type="submit">Search</button>
                        </div>
                        <a href="<?php echo e(action('Shop\ShopProductController@index')); ?>"> <i class="material-icons">close</i> Clear Result</a>
                    </div>
                </div>
            </div>
        </form>
        

        <div class="table mt-5 custom-table">
            <table>
                <tr>

                    <th>Image</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Actions</th>

                </tr>
                <?php if( $shop_products ): ?>
                <?php $__currentLoopData = $shop_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
                <tr>


                    <td>
                        <?php if($shop_product->product->image): ?>
                        <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($shop_product->product->image); ?>" class="product-image-listing">
                        <?php endif; ?>
                    </td>
                    <td><?php echo e($shop_product->product->name); ?></td>
                    <td><?php echo e($shop_product->price); ?></td>
                    <td><?php echo e($shop_product->product->category->name); ?></td>
                    <td>
                        <?php if($shop_product->status): ?>
                        <span>Active</span>
                        <?php else: ?>
                        <span>Inactive</span>
                        <?php endif; ?>
                    </td>
					 <td> 
                                <label class="switch">
                                    <input type="checkbox" data-href="<?php echo e(url('shop/manage-products/toggle_status/'.$shop_product->product_id.'/'.$shop->id)); ?>" <?php if($shop_product->status): ?> checked="checked" <?php endif; ?> >
                                    <span class="slider round"></span>
                                </label>
                            </td>
                    <!--td>
                        <form method="post" action="<?php echo e(route('manage-products.update', $shop_product->product->id)); ?>">
                            <?php echo e(csrf_field()); ?>

                            <?php echo e(method_field('PUT')); ?>

                            <?php if($shop_product->status): ?>
                            <input type="hidden" name="action" value="0">
                            <input type="submit" class="" value="Disable">
                            <?php else: ?>
                            <input type="hidden" name="action" value="1">
                            <input type="submit" class="" value="Enable">
                            <?php endif; ?>
                        </form>
                    </td-->
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php endif; ?>
            </table>
            <?php echo e($shop_products->links()); ?>

        </div>

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('shop/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/shop/product/index.blade.php ENDPATH**/ ?>