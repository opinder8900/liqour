<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title><?php echo e(config('app.name', 'Laravel')); ?></title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet"> 

        <!-- Styles -->
        <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('assets/css/backend-style.css')); ?>" rel="stylesheet">
        <!--Global JS variables-->
        <script>
        var CONFIG = {
            'site_url': '<?php echo e(url('/')); ?>'
        }
        </script>

            <script src="<?php echo e(asset('js/jquery-3.5.1.min.js')); ?>"></script>
            <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>




        </head>
        <body>
            <div id="app">
                <div class=""></div>
                <div class="back-login-main">
                    <div class="f-w front-header">
                        <nav class="navbar navbar-expand-md">
                            <div class="container">
                                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                                    <?php echo e(config('app.name', 'Laravel')); ?>

                                </a>
                                <?php if(session('status')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session('status')); ?>

                                </div>
                                <?php endif; ?>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <!-- Left Side Of Navbar -->

                                    <!-- Right Side Of Navbar -->
                                    <ul class="">
                                        <!-- Authentication Links -->
                                        <?php if(auth()->guard()->guest()): ?>
                                        <li class="nav-item">
                                            <a class="common-btn white-btn" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                                        </li>

                                        <?php else: ?>
                                        <li class="nav-item dropdown">
                                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                <?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->role); ?><span class="caret"></span>
                                            </a>

                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                                   onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                                                    <?php echo e(__('Logout')); ?>

                                                </a>

                                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                                    <?php echo csrf_field(); ?>
                                                </form>
                                            </div>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                    </nav>
                                </div>
                            </div>

                            <?php echo $__env->yieldContent('content'); ?>

                            

                            <!-- Scripts -->
                            <script src="<?php echo e(asset('js/jquery-3.5.1.min.js')); ?>" ></script>
                            <script src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>
                            <script src="<?php echo e(asset('js/jquery.cookie.js')); ?>" ></script>
                            <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
                            </body>

                            </html>
<?php /**PATH /var/www/html/liquorstore/resources/views/layouts/app.blade.php ENDPATH**/ ?>