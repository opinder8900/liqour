<?php $__env->startSection('content'); ?>



<div class="f-w bread">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                
            </div>
        </div>

    </div>
</div>

<section>
    <div class="container">
		<div class="row mt-5">
		
     <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        
        <div class="col-md-9">
		
			<div class="my-acc-main">
				Add New Address
				
			</div>
			  <?php if($errors->any()): ?>
                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php echo e($err); ?>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
				<form id="address-form" name="address-form" class="address-form" method="post"  action="<?php echo e(route('address.update', $address->id)); ?>">
					 <?php echo e(csrf_field()); ?>

					  <?php echo e(method_field('PUT')); ?>

					<input name="full_name" id = "full_name" class="form-control" placeholder = "Full Name" type="text" value="<?php echo e(old('full_name', $address->full_name)); ?>">
					<input name="phone" id = "phone" class="form-control" placeholder = "Phone" type="text" value="<?php echo e(old('phone', $address->phone)); ?>">
					<input name="pin_code" id = "pin_code" class="form-control" placeholder = "Pin Code" type="text" value="<?php echo e(old('pin_code', $address->pin_code)); ?>">
					<input name="address_line_1" id = "address_line_1" class="form-control" placeholder = "Address Line 1" type="text" value="<?php echo e(old('address_line_1', $address->address_line_1)); ?>">
					<input name="address_line_2" id = "address_line_2" class="form-control" placeholder = "Address Line 1" type="text" value="<?php echo e(old('address_line_2', $address->address_line_2)); ?>">
					<input name="landmark" id = "landmark" class="form-control" placeholder = "Landmark" type="text" value="<?php echo e(old('landmark', $address->landmark)); ?>">
					<input name="country" id = "country" class="form-control" placeholder = "Country" type="text" value="India" readonly>
					<select name="state" id = "state" class="form-control" value="<?php echo e(old('phone', $address->phone)); ?>">
						<?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($state->state_code); ?>"><?php echo e($state->state_name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
					</select>
					<select name="city" id = "city" class="form-control">
						<?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($city->id); ?>" ><?php echo e($city->city_name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						
					</select>
					<select name="type" id = "type" class="form-control">
						<option value="shipping">Shipping</option>
						<option value="billing">Billing</option>
					</select>
					
					<input name="default-checkbox" id="default" class="form-control" type="checkbox"> Mark this as you default address
					<input type="submit" value = "Add" id="submit" name="submit" class="btn btn-primary">
				</form>
        </div>
        
    </div>

	
      


    </div>
</section>


<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/address/edit.blade.php ENDPATH**/ ?>