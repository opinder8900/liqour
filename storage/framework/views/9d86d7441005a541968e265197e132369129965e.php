<?php $__env->startSection('body-class', 'welcome-home'); ?>
<?php $__env->startSection('content'); ?>

<?php echo $__env->make('layouts.lower-tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="banner f-w">
    <div class="container">
        <div class="banner-inner">
            <h1> Many needs, one app </h1>
            <h2 class="mt-3"> Then get it delivered in under 40 minutes. </h2>

            <!--div class="f-w banr-serch-out">
                <div class="baner-search f-w mt-5">
                    <input id="place-auto-picker" value="<?php echo e(Session::get('location.display')); ?>" type="text" name="" placeholder="Search location" readonly>
                    <input type="submit" name="" value="Search">
                    <a href="javascript:void(0);" id="locate-me-gps"> <img src="<?php echo e(URL::asset('assets/images/locate.svg')); ?>"> <span> Locate Me </span> </a>
                    <img class="loc-icon" src="<?php echo e(URL::asset('assets/images/loc-ion.svg')); ?>">
                </div>
            </div>
            <ul class="f-w mt-3">
                <li> <a href="#"> mohali </a> </li>
                <li> <span> / </span> </li>
                <li> <a href="#"> Patiala </a> </li>
            </ul-->
        </div>
    </div>
</div>


<section class="bus-main-loc">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="f-w common-heading">
                    <h2> What do you want to get done? </h2>
                </div>
            </div>
        </div>
        <?php if(!Session::has('location')): ?>
        <div id="location-based-content-ajx">Please wait we are detecting your location...</div>
        <?php elseif(count($businesses)==0): ?>
        <div class="text-warning">We did not find any business near to your location</div>
        <?php endif; ?>
        <div class="row text-center">
            <?php $__currentLoopData = $businesses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $business_slug => $businessItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $businessItem; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $business): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4">
                <a class="bus-home-outer" href="<?php echo e(route('business-shops',['business' => $business->slug])); ?>">
                    <div class="cat-img-home">
                        <img src="<?php echo e(URL::asset($business->image)); ?>">
                    </div>
                    <div class="cat-img-home">
                        <h4> <?php echo e($business->name); ?> </h4>
                    </div>
                </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>

    </div>
</section>

<section class="app-dow-bg pr pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="iphone-img">
                    <img class="" src="<?php echo e(URL::asset('assets/images/app.png')); ?>">
                </div>
                <div class="iphone-text">
                    <h2 class="f-w mb-0"> Download our android app </h2>
                    <ul class="f-w app-dow-ul mt-4">
                        <li> 
                            <img class="" src="<?php echo e(URL::asset('assets/images/google.png')); ?>">
                        </li>
                    </ul>
                    <div class="phone-app-link">
                        <input type="text" placeholder="Enter Phone Number">
                        <input class="common-btn white-btn mt-3" type="submit" value="Get app link">
                    </div>
                </div>

                <!--h2 class="f-w mb-0 mt-5"> Coming Soon for iphone </h2>
                <ul class="f-w app-dow-ul mt-4">
                    <li> 
                    <img class="" src="<?php echo e(URL::asset('assets/images/capple.png')); ?>">    
                    </li> 
                </ul-->
                <!--div class="baner-search f-w mt-4">
                    <input type="text" name="" placeholder="Mobile Number">
                    <input type="submit" name="" value="Get App Link">
                </div-->
            </div>
        </div>

    </div>
</section>

<section>
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <div class="f-w common-heading">
                    <h2> Consider it Etheka </h2>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6">
                <div class="con-left-img f-w mb-3">
                    <img class="" src="<?php echo e(URL::asset('assets/images/h1.svg')); ?>">
                </div>
                <div class="con-left-text">
                    <h4> No minimum order </h4>
                    <p>
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                    </p>
                </div>
            </div>

            <div class="col-md-6 mt-40-767">
                <div class="con-left-img f-w mb-3">
                    <img class="" src="<?php echo e(URL::asset('assets/images/h2.svg')); ?>">
                </div>
                <div class="con-left-text">
                    <h4> Delivered in 45 mins </h4>
                    <p>
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                    </p>
                </div>
            </div>

            <div class="col-md-6 mt-40-767">
                <div class="con-left-img f-w mb-3">
                    <img class="" src="<?php echo e(URL::asset('assets/images/e3.jpg')); ?>">
                </div>
                <div class="con-left-text">
                    <h4> Free delivery for new users </h4>
                    <p>
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                    </p>
                </div>
            </div>

            <div class="col-md-6 mt-40-767">
                <div class="con-left-img f-w mb-3">
                    <img class="" src="<?php echo e(URL::asset('assets/images/e4.jpg')); ?>">
                </div>
                <div class="con-left-text">
                    <h4> Available 24x7 </h4>
                    <p>
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                    </p>
                </div>
            </div>

        </div>

    </div>
</section>

<section class="testi-outer-main">
    <div class="container">

        <div class="row mb-2">
            <div class="col-lg-12">
                <div class="f-w common-heading">
                    <h2> What ours client’s say </h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 mt-3">
                <div class="testi-inner">
                    <div class="testi-user mb-4">
                        <div class="testi-img mb-4">
                            <img class="" src="<?php echo e(URL::asset('assets/images/ellipse-1.png')); ?>">
                        </div>
                        <div class="testi-name">
                            <h4> Surya V </h4>
                            <h6> 26, Delhi </h6>
                        </div>
                    </div>
                    <p>
                        E-Theka has been extremely useful with reference to getting things done in an unknown city. Delhi is sometimes too cumbersome to deal with - E-Theka  makes this easier.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 mt-3">
                <div class="testi-inner">
                    <div class="testi-user mb-4">
                        <div class="testi-img">
                            <img class="" src="<?php echo e(URL::asset('assets/images/ellipse-2.png')); ?>">
                        </div>
                        <div class="testi-name">
                            <h4> Mark smith </h4>
                            <h6> 26, Delhi </h6>
                        </div>
                    </div>
                    <p>
                        E-Theka has been extremely useful with reference to getting things done in an unknown city. Delhi is sometimes too cumbersome to deal with - E-Theka  makes this easier.
                    </p>
                </div>
            </div>
        </div>

    </div>
</section>

<!--<div class="flex-center position-ref full-height">
    <?php if(Route::has('login')): ?>
    <div class="top-right links">
        <?php if(auth()->guard()->check()): ?>
        <a href="<?php echo e(url('/home')); ?>">Home</a>
        <?php else: ?>
        <a href="<?php echo e(route('login')); ?>">Login</a>

        <?php if(Route::has('register')): ?>
        <a href="<?php echo e(route('register')); ?>">Register</a>
        <?php endif; ?>
        <?php endif; ?>

        <a href="<?php echo e(route('tradesman-create')); ?>">tradesman register</a>
    </div>
    <?php endif; ?>

    <div class="content">
        <div class="title m-b-md">
            Laravel
        </div>

    </div>
</div>-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/welcome.blade.php ENDPATH**/ ?>