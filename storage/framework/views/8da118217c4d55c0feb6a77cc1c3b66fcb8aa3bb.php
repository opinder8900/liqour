<?php $__env->startSection('content'); ?>

<script>
    $(document).ready(function(){
	
        $(document).on('click','.add-pro-price input:checkbox', function(){
            $(this).closest('tr').toggleClass("checked");
        });
        $(document).on('change','.shop_category',function(){
            $('.shop_brand_container').html('<li> <label> </label> <span> Loading brands... </span> </li>');
            $.ajax({url: "<?php echo e(url('superadmin/brands/by_category')); ?>/"+$(this).val(),success: function(brands){
                let html='';
                $.each(brands,function(index, brand){
                    html +='<li> <label> <input class="shop_brand" type="radio" '+(index==0 ? 'checked="checked"' : '')+' name="brand" value="'+brand.id+'"> </label> <span> '+brand.name+' </span> </li>';
                });
                $('.shop_brand_container').html(html);
                if(!brands.length){
                    $('.shop_product_container').html('');
                }
                $('.shop_brand:checked').trigger('change');
            }});
        });
        $(document).on('change','.shop_brand',function(){
            $('.shop_product_container').html('<tr><td> <label> </label></td><td colspan="2"> <span> Loading products... </span> </td></tr>');
            $.ajax({url: "<?php echo e(url('superadmin/products/by_brand')); ?>/"+$(this).val()+"/<?php echo e($shop->id); ?>",success: function(products){
                let html='';
                $.each(products,function(index, product){
                    html +='<tr> <td> <input type="hidden" name="product['+index+'][product_id]" value="'+product.id+'"> <label> <input type="hidden" name="product['+index+'][selected]" value="0"> <input type="checkbox" name="product['+index+'][selected]" value="1"> </label> </td> <td> <span> '+product.name+' <span> '+product.size+''+product.size_label+' </span> </span> </td> <td> <input class="form-control" type="number" required min="0" max="1000000" name="product['+index+'][price]" value="" placeholder="Enter Price"> </td> </tr>';
                });
                $('.shop_product_container').html(html);
            }});
        });
    });
</script>

<div class="f-w mt-80 add-products-page">
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-6">
            <h2><?php echo e(Str::title($shop->name)); ?></h2>
        </div>
    </div>  

    <div class="row mt-2">
        <div class="col-md-6">
            <h4>Add Products</h4>
        </div>
    </div>
     <form action="<?php echo e(url('superadmin/shops/products/'.$shop->id)); ?>" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo e(old('id',$shop->id)); ?>">
        <?php echo e(csrf_field()); ?>

    
        <div class="row mt-5">

            <div class="col-md-3">
                <div class="pro-add-main">
                    <h4 class="f-w mb-4">Select Category</h4>
                    <ul class="add-pro-ul shop_category_container">
                        <?php if(count($categories) <=0 ): ?>
                        <li>
                            <label>
                                
                            </label>
                            <span> No categories found </span>
                        </li>
                        <?php endif; ?>
                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <label>
                                <input class="shop_category" type="radio" <?php if($loop->first): ?> checked="checked" <?php endif; ?> name="category" value="<?php echo e($id); ?>">
                            </label>
                            <span> <?php echo e(Str::title($name)); ?> </span>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="pro-add-main">
                    <h4 class="f-w mb-4">Select Brands</h4>
                    <ul class="add-pro-ul shop_brand_container">
                        <?php if(count($brands) <=0 ): ?>
                        <li>
                            <label>
                                
                            </label>
                            <span> No brands found </span>
                        </li>
                        <?php endif; ?>
                        <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li>
                            <label>
                                <input class="shop_brand" type="radio" <?php if($loop->first): ?> checked="checked" <?php endif; ?> name="brand" value="<?php echo e($id); ?>">
                            </label>
                            <span> <?php echo e(Str::title($name)); ?> </span>
                        </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>

            <div class="col-md-6">
                <div class="pro-add-main">
                    <h4 class="f-w mb-4">Add Products</h4>
                    <div class="table add-pro-price">
                        <table class="shop_product_container">
                            <?php if(count($products) <=0 ): ?>
                            <tr>
                                <td>
                                    <label> </label>
                                </td>
                                <td colspan="2"><span> No products to add to this class </span></td>
                            </tr>
                            <?php endif; ?>
                            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>
                                    <input type="hidden" name="product[<?php echo e($index); ?>][product_id]" value="<?php echo e($product->id); ?>">
                                    <label>
                                        <input type="hidden" name="product[<?php echo e($index); ?>][selected]" value="0">
                                        <input type="checkbox" class="shop_product" name="product[<?php echo e($index); ?>][selected]" value="1">
                                    </label>
                                </td>
                                <td>
                                    <span> <?php echo e(Str::title($product->name)); ?> <span> <?php echo e($product->size); ?><?php echo e($product->size_label); ?> </span> </span>
                                </td>
                                <td>
                                    <input class="form-control" type="number" required min="0" max="1000000" name="product[<?php echo e($index); ?>][price]" value="" placeholder="Enter Price">
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </table>
                    </div>
                </div>
            </div>

            <div class="add-pro-submit">
                <input class="common-btn" type="submit">
                <a href="<?php echo e(url('/superadmin/trademan/shops/'.$shop->user_id)); ?>" class="common-btn">Cancel</a>
            </div>

        </div>
    </form>
</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/shop/product/create.blade.php ENDPATH**/ ?>