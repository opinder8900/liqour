<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-6">
            <h2><?php echo e($shop->name); ?> Products</h2>
        </div>
    </div>  

    <div class="row mt-5 mb-80">
        <div class="col-lg-12 sa-cat-tabs">
        <div class="table custom-table">

            <table>
                <thead>
                    <tr>
                        <th> Name </th>
                        <th> Slug </th>
                        <th> Price </th>
                        <th> Status </th>
                        <th> Actions </th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php $__currentLoopData = $shop_products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop_product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                                <td><?php echo e($shop_product->product->name); ?> </td>
                                <td> <?php echo e($shop_product->product->slug); ?></td>
                                <td class="price-edit"> <?php echo e($shop_product->price); ?> </td>
                                <td>
                                <label class="switch">
                                    <input type="checkbox" data-href="<?php echo e(url('superadmin/shops/products/toggle_status/'.$shop_product->id)); ?>" <?php if($shop_product->status): ?> checked="checked" <?php endif; ?>>
                                    <span class="slider round"></span>
                                </label> 
                                </td>
                                
                                <td>
									 <a href="javascript:void(0)" class="edit-product-price" data-id="<?php echo e($shop_product->id); ?>"> 
                                       Edit
                                    </a>
                                    <a href="<?php echo e(url('superadmin/shops/products/destroy/'.$shop_product->id)); ?>" onclick="return beforeCategoryDelete('<?php echo e($shop_product->product->name); ?>')"> 
                                        <i class="material-icons"> delete </i> 
                                    </a>
                                </td>
                            </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <?php echo e($shop_products->links()); ?> 
            </div>
        </div>
    </div>

</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/shop/product/index.blade.php ENDPATH**/ ?>