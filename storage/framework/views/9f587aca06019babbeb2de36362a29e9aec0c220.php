<?php $__env->startSection('content'); ?>

<div class="f-w mt-80">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Category List</h2>
		</div>
		<div class="col-md-6 text-right">
            <a class="common-btn green-btn" href="<?php echo e(action('Shop\CategoryController@create')); ?>"> 
				<i class="material-icons">add_circle_outline</i> <span> Add Category </span>
			</a>
		</div>
	</div>  

    <div class="row">

        <div class="col-md-12 mt-4">
            <ul id="tree1" class="categories-list">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li>
                    <div class="cate-list-outer">

                        <div class="cate-list-block">
                            <?php if($category->image): ?>
                            <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($category->image); ?>" class="cat-image-parent">
                            <?php endif; ?>
                        </div>

                        <div class="cate-list-block">
                            <h4>
                            <?php echo e($category->name); ?>

                            </h4>
                        </div>

                        <div class="cate-list-block">
                            <?php if($category->status): ?>
                            <span>Active</span>
                            <?php else: ?>
                            <span>Inactive</span>
                            <?php endif; ?>
                        </div>

                        <div class="cate-list-block">
                            <a href="<?php echo e(route('categories.edit', $category->id)); ?>">Edit</a>
                        </div>

                        <?php if(count($category->childs) == 0): ?>
                        <form class="cate-delete" method="post" action="<?php echo e(route('categories.destroy', $category->id)); ?>" onsubmit="return beforeCategoryDelete('<?php echo e($category->name); ?>')">
                            <?php echo e(csrf_field()); ?>

                            <?php echo e(method_field('DELETE')); ?>

                            <div class="form-group">
                                <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                            </div>
                        </form>
                        <?php endif; ?>
                        

                        <?php if(count($category->childs)): ?>
                        <?php echo $__env->make('shop.category.manageChild',['childs' => $category->childs], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php endif; ?>
                    </div>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    </div>

</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('shop/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/shop/category/index.blade.php ENDPATH**/ ?>