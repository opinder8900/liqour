
<div class="col-md-4">
            
    <ul class="my-account-sidebar">
        <li> <a href="<?php echo e(route('my-profile')); ?>"> <i class="material-icons">person</i> <span> Account </span> </a> </li>
        <li> <a href="<?php echo e(route('my-orders')); ?>"> <i class="material-icons">shopping_cart</i> <span> My Orders </span> </a> </li>
        <li> <a href="<?php echo e(route('address.index')); ?>"> <i class="material-icons">home</i> <span> Address </span> </a> </li>
        <li> <a href="<?php echo e(route('change-password')); ?>"> <i class="material-icons">lock</i> <span> Change Password </span> </a> </li>
    </ul>
    
</div>
            
<?php /**PATH /var/www/html/liquorstore/resources/views/layouts/sidebar.blade.php ENDPATH**/ ?>