<?php $__env->startSection('content'); ?>
<?php if($customers): ?>

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">

            <div class="col-lg-12">
                <h2> Customers List </h2>
            </div>

            <div class="col-lg-12 mt-5">

                <table>
                    <tr><th>Name</th><th>Phone</th></tr>
                    <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($customer->name); ?></td>
                        <td><?php echo e($customer->phone); ?></td>
                    </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
                <?php endif; ?>
                <?php $__env->stopSection(); ?>

            </div>

        </div>
    </div>
</div>
<?php echo $__env->make('shop/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/shop/customer/index.blade.php ENDPATH**/ ?>