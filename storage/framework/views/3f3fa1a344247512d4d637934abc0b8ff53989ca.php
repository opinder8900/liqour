<ul>
    <?php $__currentLoopData = $childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li>
        <div class="cate-list-block">
            <?php if($child->image): ?>
            <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($child->image); ?>" class="cat-image-child">
            <?php endif; ?>
        </div>

        <div class="cate-list-block">
            <h4>
            <?php echo e($child->name); ?>

            </h4>
        </div>

        <div class="cate-list-block">
            <?php if($child->status): ?>
            <span>Active</span>
            <?php else: ?>
            <span>Inactive</span>
            <?php endif; ?>
        </div>

        <?php if(count($child->childs)): ?>
        <?php echo $__env->make('shop.category.manageChild',['childs' => $child->childs], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?>

        <div class="cate-list-block">
            <a href="<?php echo e(route('categories.edit', $child->id)); ?>">Edit</a>
        </div>

        <div class="cate-list-block">
            <?php if(count($child->childs) == 0): ?>
            <form class="cate-delete" method="post" action="<?php echo e(route('categories.destroy', $child->id)); ?>">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('DELETE')); ?>

                <div class="form-group">
                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                </div>
            </form>
            <?php endif; ?>
        </div>

    </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
<?php /**PATH /var/www/html/liquorstore/resources/views/shop/category/manageChild.blade.php ENDPATH**/ ?>