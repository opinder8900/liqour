<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> List of Orders </h2>
        </div>
       
    </div>
    
    <div class="row mt-5">
		<div class="col-md-12">
			<div class="table custom-table">
                <table>
                    <tr>
                        <th> Product Name </th>
                        <th> Price </th>
                        <th> Quantity </th>
                        <th> SubTotal </th>
                        
                        
                    </tr>
                       <?php if($order_details): ?>
							<?php $__currentLoopData = $order_details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								 <tr>
									<td><?php echo e($order->name); ?> </td>
									<td> <?php echo e($order->price); ?></td>
									<td> <?php echo e($order->quantity); ?></td>
									<td> <?php echo e($order->sub_total); ?></td>
									
									
									
							
						</tr>
								
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
                                   
                  
                </table>
            </div>
		</div>
    </div>
    
</div>
</div>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/customer/order_details.blade.php ENDPATH**/ ?>