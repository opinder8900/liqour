<?php $__env->startSection('content'); ?>

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-md-12">
                <h2> dashboard</h2>
            </div>
            <div class="col-md-12 mt-5">
                <div class="row">
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Trademan</h4>
                            <i class="material-icons">person</i>
                            <h2 class="mb-0 mt-4"> <?php echo e($totalTrdesman); ?> </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Shops</h4> 
                            <i class="material-icons">store_mall_directory</i>
                            <h2 class="mb-0 mt-4"> <?php echo e($totalShops); ?> </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Products</h4> 
                            <i class="material-icons">local_grocery_store</i>
                            <h2 class="mb-0 mt-4"> <?php echo e($totalProducts); ?> </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Total Customer</h4> 
                            <i class="material-icons">group</i>
                            <h2 class="mb-0 mt-4"> <?php echo e($totalCustomers); ?> </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            
            <div class="col-md-12 mt-5">
                
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="mb-5">
                            <h2> New Shops  </h2>
                        </div>
                        <div class="table custom-table">
                            <table>
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> Phone </th>
                                        <th> Email </th>
                                        <th> Address </th>
                                        <th> licence </th>
                                        <th> Status </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php if($newShops): ?>
										<?php $__currentLoopData = $newShops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											 <tr>
												<td> <?php echo e($shop->name); ?></td>
												<td> <?php echo e($shop->primary_phone); ?></td>
												<td> <?php echo e($shop->shopKeeper->email); ?> </td>
												<td> <?php echo e($shop->address); ?> </td>
												<td> <?php echo e($shop->license_number); ?> </td>
												<td>
                                        <label class="switch">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="dropdown dropleft">
                                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                                </div>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="<?php echo e(url('superadmin/shops/products')); ?>"> 
                                                        <i class="material-icons"> add_circle </i> 
                                                        <span> Add Products </span> 
                                                    </a>
                                                    <a href="#"> 
                                                        <i class="material-icons"> visibility </i> 
                                                        <span> View Products </span> 
                                                    </a>
                                                    <a href="#"> 
                                                        <i class="material-icons"> delete </i> 
                                                        <span> Delete </span> 
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
											
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endif; ?>
                                   
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <!--div class="col-lg-12 mt-80">
                        <div class="mb-5">
                            <h2> New Customers  </h2>
                        </div>
                        <div class="table custom-table">
                            <table>
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> Email </th>
                                        <th> Phone </th>
                                        <th> Address </th>
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php if($newCustomers): ?>
										<?php $__currentLoopData = $newCustomers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											 <tr>
												<td> <?php echo e($customer->name); ?></td>
												<td> <?php echo e($customer->email); ?></td>
												<td> <?php echo e($customer->phone); ?> </td>
												<td>  </td>
												<td></td>
												<td>
                                        <label class="switch">
                                            <input type="checkbox">
                                            <span class="slider round"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="dropdown dropleft">
                                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                                </div>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="<?php echo e(url('superadmin/shops/products')); ?>"> 
                                                        <i class="material-icons"> add_circle </i> 
                                                        <span> Add Products </span> 
                                                    </a>
                                                    <a href="#"> 
                                                        <i class="material-icons"> visibility </i> 
                                                        <span> View Products </span> 
                                                    </a>
                                                    <a href="#"> 
                                                        <i class="material-icons"> delete </i> 
                                                        <span> Delete </span> 
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
											
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endif; ?>
                                   
                                </tbody>
                            </table>
                        </div>
                    </div-->




                </div>
            </div>
        </div>

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/dashboard/index.blade.php ENDPATH**/ ?>