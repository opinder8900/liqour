<div class="footer-tabs-main">
    <ul>
        <li class="home-ltab">
            <a href="<?php echo e(url('/')); ?>">
                <i class="material-icons">home</i>
                <p>Home</p>
            </a>
        </li>
        <li>
            <a href="#">
                <i class="material-icons">location_on</i>
                <p>Change Location</p>
            </a>
        </li>
        <li class="order-ltab">
            <a href="<?php echo e(route('my-orders')); ?>">
                <i class="material-icons">shopping_bag</i>
                <p>Orders</p>
            </a>
        </li>
        <li class="account-ltab">
            <a href="<?php echo e(route('my-profile')); ?>">
                <i class="material-icons">person</i>
                <p>My Account</p>
            </a>
        </li>
    </ul>
</div><?php /**PATH /var/www/html/liquorstore/resources/views/layouts/lower-tabs.blade.php ENDPATH**/ ?>