<?php $__env->startSection('content'); ?>


<h2>Show page</h2>
<table class="table">
    <tr>
        <th>Actions</th>
        <th>Name</th>
        <th>Price</th>
        <th>Category</th>
        <th>Status</th>
        <th>Image</th>
        <th>In Stock</th>
        <th>Created At</th>

    </tr>
    <tr>
        <td>
            <a href="<?php echo e(route('products.show', $product->id)); ?>">Show</a>
            <a href="<?php echo e(route('products.edit', $product->id)); ?>">Edit</a>
            <form method="post" action="<?php echo e(route('products.destroy', $product->id)); ?>" onsubmit="return beforeProductDelete('<?php echo e($product->name); ?>')">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field('DELETE')); ?>

                <div class="form-group">
                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                </div>
            </form>
        </td>
        <td><?php echo e($product->name); ?></td>
        <td><?php echo e($product->price); ?></td>
        <td><?php echo e($product->category->name); ?></td>
        <td>
            <?php if($product->status): ?>
            <span>Active</span>
            <?php else: ?>
            <span>Inactive</span>
            <?php endif; ?>
        </td>
        <td>
            <?php if($product->image): ?>
            <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($product->image); ?>" class="product-image-listing">
            <?php endif; ?>
        </td>
        <td><?php echo e($product->in_stock_quantity); ?></td>
        <td><?php echo e($product->created_at); ?></td>
    </tr>
</table>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('shop/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/shop/product/show.blade.php ENDPATH**/ ?>