<ul>
	<li class="bussiness-tab">
		<a href="<?php echo e(url('superadmin\business')); ?>">Business</a>
	</li>
	<li class="category-tab">
		<a href="<?php echo e(url('superadmin\categories')); ?>">Category</a>
	</li>
	<li class="brands-tab">
		<a href="<?php echo e(url('superadmin\brands')); ?>">Brands</a>
	</li>
	<li class="product-tab">
		<a href="<?php echo e(url('superadmin\products')); ?>">Products</a>
	</li>
</ul>
<?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/layouts/tabs.blade.php ENDPATH**/ ?>