<?php $__env->startSection('content'); ?>

            <h2>Unauthorized</h2>
            <p>You are not autorized to view this page</p>
            <?php if(Auth::user()->role == 'superadmin'): ?>
            <a class="btn btn-success" href="<?php echo e(url('superadmin/dashboard')); ?>">Home</a>
            <?php elseif(Auth::user()->role == 'tradesman'): ?>
            <a class="btn btn-success" href="<?php echo e(url('tradesman/dashboard')); ?>">Home</a>
            <?php elseif(Auth::user()->role == 'shop'): ?>
            <a class="btn btn-success" href="<?php echo e(url('shop/dashboard')); ?>">Home</a>
            <?php else: ?>
            <a class="btn btn-success" href="<?php echo e(url('/')); ?>">Home</a>
            <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/errors/403.blade.php ENDPATH**/ ?>