<!doctype html>

<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

    <head>
        <!--<script src="<?php echo e(asset('js/jquery-3.5.1.min.js')); ?>"></script>-->

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?php echo e(asset('assets/images/favicon.ico')); ?>" type="image/gif" sizes="16x16"> 
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title>E-theka</title>
         <?php $config = (new \LaravelPWA\Services\ManifestService)->generate(); echo $__env->make( 'laravelpwa::meta' , ['config' => $config])->render(); ?>
        <!--Global JS variables-->
        <script>
            var CONFIG = {
            site_url: "<?php echo e(url('/')); ?>",
                    google_map_api_key: "<?php echo e(config('app.google_map_api_key')); ?>",
                    default_location:{
                    latitude : <?php echo e(config('app.default_latitude')); ?>,
                            longitude : <?php echo e(config('app.default_longitude')); ?>

                    },
                    user:{
                    is_logged_in: "<?php echo e(Auth::guest() ? false:true); ?>"
                    }
            }
        </script>

        <style>
            #map {
                height: 100%;
            }

        </style>
        <!-- Scripts -->

        <!--Please contact jatinder if you want to add app.js here-->
        <!--<script src="<?php echo e(asset('js/front-app.js')); ?>" defer></script>-->

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet">

        <!-- Styles -->
        <!--<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">-->
        <link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('assets/css/style.css')); ?>" rel="stylesheet">





    </head>
    <body class="<?php echo $__env->yieldContent('body-class'); ?>">
        <div id="app">
            <div class="f-w front-header">
                <nav class="">
                    <div class="container">
                        <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                            E-theka
                        </a>
                        <div id="change-delivery-address" class="loc-header-main">
                            <?php if(Session::has('location')): ?>
                            <div class="main-loc-head">
                                <?php if(Session::has('location')): ?>
                                <h4><?php echo e(Session::get('location.type')); ?></h4>
                                <p><?php echo e(Session::get('location.display')); ?></p>
                                <?php endif; ?>
                            </div>
                            <button class="loc-head-btn"> <i class="material-icons">expand_more</i> </button>
                            <?php else: ?>
                            <p class="cho-head-dl-add">Choose Delivery Address</p>
                            <button class="loc-head-btn"> <i class="material-icons">expand_more</i> </button>
                            <?php endif; ?>
                        </div>

                        <div class=" justify-content-end" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <!--div class="heaer-btns">
                                <a class="common-btn" href="#"><?php echo e(__('E-theka for Partners')); ?></a>
                                <a class="common-btn" href="#"><?php echo e(__('E-theka for Business')); ?></a>
                            </div-->
                            <div class="header-cart">
                                <a href="<?php echo e(route('cart-index')); ?>">
                                    <i class="material-icons">shopping_cart</i>
                                    <?php if(!ShoppingCart::isEmpty()): ?>
                                    <span id="card-number"><?php echo e(ShoppingCart::count()); ?></span>
                                    <?php endif; ?>
                                </a>
                            </div>
                            <ul class="my-acc-usr-ul">
                                <!-- Authentication Links -->
                                <?php if(auth()->guard()->guest()): ?>
                                <div class="dropdown front-head-user">
                                    <a class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">account_circle</i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <ul>
                                            <li class="">
                                                <a class="" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                                            </li>
                                            <li class="">
                                                <a class="" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <?php else: ?>
                                <li class="dropdown">
                                    <a id="dropdownMenuButton" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php $__currentLoopData = explode(' ', Auth::user()->name); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $firstname): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php echo e($firstname); ?>

                                        <?php break; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">

                                        <a class="dropdown-item" href="<?php echo e(route('my-profile')); ?>">
                                            <?php echo e(__('My Profile')); ?>

                                        </a>

                                        <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                           onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            <?php echo e(__('Logout')); ?>

                                        </a>

                                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                            <?php echo csrf_field(); ?>
                                        </form>

                                    </div>

                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="f-w p-r z-2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo $__env->make('superadmin/flash/flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo $__env->make('location/address', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php echo $__env->yieldContent('content'); ?>
            <?php echo $__env->make('layouts.front-footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <!-- Scripts -->


            <script src="<?php echo e(asset('js/jquery-3.5.1.min.js')); ?>" ></script>
            <script src="<?php echo e(asset('js/jquery.cookie.js')); ?>" ></script>
            <script src="<?php echo e(asset('js/app.js')); ?>"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>            
            <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo e(config('app.google_map_api_key')); ?>&libraries=places&callback=initMap" async defer></script>
        </div>
    </body>

</html>
<?php /**PATH /var/www/html/liquorstore/resources/views/layouts/front-header.blade.php ENDPATH**/ ?>