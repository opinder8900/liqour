<?php $__env->startSection('body-class', 'register-page'); ?>
<?php $__env->startSection('content'); ?>

<div class="back-login-bg"></div>
<div class="f-w back-login-main col-lg-12 d-flex h-100vh">

    <div class="container">
        <div class="row justify-content-center h-100vh pt-5 pb-5">
            <div class="col-md-8 login-outer my-auto">

                <div class="col-md-12 heading-common mb-5">
                    <h2>
                        <?php echo e(__('Register')); ?>

                    </h2>
                    <p>Please share your details to initiate the onboarding process.</p>
                </div>

                <form method="POST" action="<?php echo e(route('register')); ?>">
                    <?php echo csrf_field(); ?>

                    <div class="form-group">
                        <label for="name" class="col-md-12 col-form-label"><?php echo e(__('Name')); ?></label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">person</i>
                                <input id="name" placeholder="Enter your name" type="text" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="name" value="<?php echo e(old('name')); ?>" required autocomplete="name" autofocus>
                            </div>
                            <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-md-12 col-form-label"><?php echo e(__('E-Mail Address')); ?></label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">email</i>
                                <input id="email" type="email" placeholder="Enter your email" class="form-control <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email">
                            </div>
                            <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone" class="col-md-12 col-form-label"><?php echo e(__('Phone')); ?></label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">local_phone</i>
                                <input id="name" placeholder="Enter your phone No:" type="text" class="form-control <?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="phone" value="<?php echo e(old('phone')); ?>" required autocomplete="phone" autofocus maxlength="10">
                            </div>
                            <?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-12 col-form-label"><?php echo e(__('Password')); ?></label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-3">
                                <i class="material-icons">lock</i>
                                <input id="password" placeholder="Create you password" type="password" class="form-control <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="password" required autocomplete="new-password">
                            </div>
                            <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-12 col-form-label"><?php echo e(__('Confirm Password')); ?></label>

                        <div class="col-md-12">
                            <div class="input-icon-outer mb-4">
                                <i class="material-icons">lock</i>
                                <input id="password-confirm" placeholder="Confirm your password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                    </div>

                    <div class="form-group mb-0 mt-4">
                        <div class="col-md-12">
                            <button type="submit" class="common-btn blue-btn blue-bg w-100">
                                <?php echo e(__('Register')); ?>

                            </button>
                        </div>
                    </div>
                </form>
                <div class="col-md-12 f-w login-page-r">
                    <p> Already have an account. Click here </p>
                    <a class="" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/auth/register.blade.php ENDPATH**/ ?>