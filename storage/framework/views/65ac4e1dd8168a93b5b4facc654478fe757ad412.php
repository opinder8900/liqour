<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> delivery boys </h2>
        </div>

        <div class="col-md-4">
                <input type="text" placeholder="Search Delivery Boy" class="form-control">
                <button class="common-btn serch-front-btn" type="submit">Search</button>
        </div>
        <div class="col-md-2">
			<a href="<?php echo e(url('superadmin/delivery/create')); ?>" class="common-btn w-100 text-center"> Add delivery boy </a>
        </div>
    </div>
        

    <div class="row mt-5">
		<div class="col-md-12">
			<div class="table custom-table">
                <table>
                    <tr>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Phone </th>
                        <th> Address </th>
                        <th> Status </th>
                        <th> Actions </th>
                    </tr>
                    <tr>
                        <td> Donna H. Hill </td>
                        <td> DonnaHHill@rhyta.com </td>
                        <td> 530-266-9621 </td>
                        <td> 2290 Francis Mine Trinity Center, CA 96091 </td>
                        <td> Online </td>
                        <td>
                            <div class="dropdown dropleft">
                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="<?php echo e(url('superadmin/delivery/1')); ?>"> 
                                        <i class="material-icons"> visibility </i> 
                                        <span> View </span> 
                                    </a>
                                    <a href="<?php echo e(url('superadmin/delivery/details/1')); ?>"> 
                                        <i class="material-icons"> visibility </i> 
                                        <span> Details </span> 
                                    </a>
                                    <a href="<?php echo e(url('superadmin/delivery/timings/1')); ?>"> 
                                        <i class="material-icons"> visibility </i> 
                                        <span> Timings </span> 
                                    </a>
                                    <a href="#"> 
                                        <i class="material-icons"> edit </i> 
                                        <span> Edit </span> 
                                    </a>
                                    <a href="#"> 
                                        <i class="material-icons"> delete </i> 
                                        <span> Delete </span> 
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
		</div>
    </div>

</div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/delivery/details.blade.php ENDPATH**/ ?>