<?php $__env->startSection('body-class', 'account-page'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.lower-tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="my-acc-bg"></div>
<section class="my-acc-sec">
    <div class="container">

        <div class="acc-main">

            <div class="row">

                <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                <div class="col-md-8">
                    <div class="my-acc-main">
                        <h4 class="f-w mb-5">My Profile</h4>
                        <div class="f-w mb-4">
                            <label> Phone number </label>
                            <h4> <?php echo e($user->phone); ?> </h4>
                        </div>

                        <div class="f-w mb-4">
                            <label> Email id </label>
                            <h4> <?php echo e($user->email); ?> </h4>
                        </div>

                        <div class="f-w mb-4">
                            <label> Name </label>
                            <h4> <?php echo e($user->name); ?></h4>
                        </div>
                        <div class="f-w">
                            <a class="common-btn blue-btn" href="<?php echo e(route('show-edit-profile')); ?>"> Edit Profile </a>
                        </div>
                    </div>
                </div>

            </div>	

        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/profile/my-profile.blade.php ENDPATH**/ ?>