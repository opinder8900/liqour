<footer class="f-w">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 footer-block">
				<h4 class="mb-3"> You can’t stop time, but you can save it! </h4>
				<p>
					Living in the city, there is never enough time to shop for groceries, pick-up supplies, grab food and wade through traffic on the way back home. How about we take care of all of the above for you? What if we can give you all that time back? Send packages across the city and get everything from food, groceries, medicines and pet supplies delivered right to your doorstep. From any store to your door, just make a list and we’ll make it disappear. Just Dunzo It!
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 footer-block mb-0">
				<h4 class="mb-3"> Services we offer </h4>
				<p>
					Only in Patiala
				</p>
			</div>
		</div>
		<div class="row">

			<div class="col-lg-4 mt-5">
				<h4 class="f-w mb-3"> Etheeka </h4>
				<ul>
					<li> <a href="#"> About </a> </li>
					<li> <a href="#"> Jobs </a> </li>
					<li> <a href="#"> Contact </a> </li>
					<li> <a href="#"> Terms & Conditions </a> </li>
					<li> <a href="#"> Privacy Policy </a> </li>
					<!--li> <a href="#"> E-theka for partner </a> </li>
					<li> <a href="#"> E-theka for business </a> </li-->
				</ul>
			</div>

			<div class="col-lg-4 mt-5">
				<h4 class="f-w mb-3"> Get in touch </h4>
				<ul>
					<li> <a href="#"> Email </a> </li>
					<li> <a href="#"> Twitter </a> </li>
					<li> <a href="#"> Facebook </a> </li>
					<li> <a href="#"> Instagram </a> </li>
					<li> <a href="#"> Linkedin </a> </li>
				</ul>
			</div>

			<div class="col-lg-4 mt-5">
				<h4 class="f-w mb-3"> Serviceable Cities </h4>
				<ul>
					<li> <a href="#"> Mohali </a> </li>
					<li> <a href="#"> Patiala </a> </li>
				</ul>
			</div>

		</div>
	</div>
</footer>
<div class="f-w footer-btm">
    <div class="container">
        <p> © E-theka. 2020, All Rights Reserved </p>
    </div>
</div><?php /**PATH /var/www/html/liquorstore/resources/views/layouts/front-footer.blade.php ENDPATH**/ ?>