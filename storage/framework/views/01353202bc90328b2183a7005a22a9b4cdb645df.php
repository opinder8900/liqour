<?php $__env->startSection('content'); ?>


<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-lg-12">
                <h2>  Order & Account Information  </h2>
            </div>

            <div class="col-lg-12 mt-5">

                <div class="table tr-hover bb-none-th">
                    <table>
                        <tr>
                            <th>Order Date</th>
                            <td>Feb 23, 2020, 12:40:12 AM</td>
                        </tr>
                        <tr>
                            <th>Order Status</th>
                            <td><?php echo e($order->status); ?></td>
                        </tr>
                        <tr>
                            <th>Customer Name</th>
                            <td><?php echo e($order->user->name); ?></td>
                        </tr>
                    </table>
                </div>

                <div class="f-w mt-5">
                    <h2>   Address Information   </h2>
                    <ul class="f-w mt-3 bk-ul">
                        <li> <p> <?php echo e($order->shipping_address->full_name); ?> </p> </li>
                        <li> <p> <?php echo e($order->shipping_address->address_line_1); ?> </p> </li>
                        <li> <p> <?php echo e($order->shipping_address->address_line_2); ?> </p> </li>
                        <li> <p> <?php echo e($order->shipping_address->city); ?>, <?php echo e($order->shipping_address->state); ?>, <?php echo e($order->shipping_address->pin_code); ?> </p> </li>
                        <li> <p> <?php echo e($order->shipping_address->country); ?> </p> </li>
                        <li> <p> <?php echo e($order->shipping_address->phone); ?> </p> </li>
                    </ul>
                </div>

                <div class="f-w mt-5">
                    <h2>    Payment & Shipping Method    </h2>
                    <ul class="f-w mt-3 bk-ul">
                        <li> <p> Cash On Delivery </p> </li>
                    </ul>
                </div>

                <div class="f-w mt-5">
                    <h2>  Items Ordered </h2>
                    <div class="table mt-3">
                        <table>
                            <tr>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Sub Toatal</th>
                            </tr>
                            <?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <tr>
                            <td><?php echo e($item->name); ?></td>
                            <td><?php echo e($item->quantity); ?></td>
                            <td><?php echo e($item->price); ?></td>
                            <td><?php echo e($item->sub_total); ?></td>
                        </tr>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </table>
                    </div>
                </div>
                 <a href="<?php echo e(route('view-order-invoice', ['id'=>$order->id])); ?>">Print Invoice</a>
				<?php if($order->status == "pending" || $order->status == "accepted"): ?>
                <div class="f-w mt-5">
                    <h2>  Select Status </h2>
                    <form action="<?php echo e(route('orders.update', $order->id)); ?>" method="post" >
						<?php echo e(method_field('PUT')); ?>

						<?php echo e(csrf_field()); ?>

                    <div class="f-w mt-3">
                        <div class="sel-ord-st">
                            <select class="form-control" name="order_status">
								<option value="accepted"> Accept </option>
                                <option value="cancelled"> Cancel </option>
                                
                                <option value="delivered"> Deliver</option>
                            </select>
                        </div>
                    </div>
                    <div class="f-w mt-5">
                       
                        <input class="common-btn blue-btn" type="submit" value="Accept Order">

                    </div>
                    </form>
                </div>
                <?php endif; ?>
               
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('shop/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/shop/order/show.blade.php ENDPATH**/ ?>