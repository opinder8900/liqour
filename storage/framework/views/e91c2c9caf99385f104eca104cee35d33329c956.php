<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.lower-tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="f-w inner-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
                <h2 class="f-w text-center"> Products </h2>
			</div>
		</div>
	</div>
</div>

<div class="f-w bread inner-bread">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <ul class="f-w">
                    <li> <a href="<?php echo e(url('/')); ?>"> Home </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <a href="<?php echo e(route('business-shops',['business'=>$business])); ?>"> <?php echo e($business); ?> </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <p> <?php echo e($shop->name); ?> </p> </li>
                </ul>
            </div>
        </div>

    </div>
</div>

<section class="front-shops-page">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">

                <div class="detail-shop">
                    <div class="listing-text">
                        <div class="list-icon">
                            <i class="material-icons">store</i>
                            <h4> <?php echo e($shop->name); ?></h4>
                        </div>
                        <!--p class="mb-0"> All kind of <?php echo e($business); ?> </p-->
                        <div class="list-icon-p p-r">
                            <i class="material-icons">access_time</i>
                            <p class="mb-2"> Timings:   <?php echo e(date('h:i a',(strtotime($shop->open_at)))); ?> – <?php echo e(date('h:i a',(strtotime($shop->close_at)))); ?>  </p>
                        </div>
                        <div class="list-icon-p p-r">
                            <i class="material-icons">location_city</i>
                            <p> <?php echo e($shop->address); ?>, <?php echo e($shop->city->name); ?> </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-4 mt-4">

                <div class="f-w search-common">
                    <form action="" method="GET">
                        <div class="front-search-out">
                            <i class="material-icons">search</i>
                            <input  class="f-w" placeholder="Search for items in store" type="text" value="<?php echo e(app('request')->input('search')); ?>" name="search" required/>
                            <button class="common-btn serch-front-btn" type="submit">Search</button>
                        </div>
                        <a href="<?php echo e(route('business-shop-products',['business' => $business,'shop' => $shop->slug])); ?>"><i class="material-icons">close</i> Clear <span> Search </span></a>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">

            <div class="col-lg-12">

                <div class="details-tabs-outer">

                    <ul class="nav nav-tabs" role="tablist">
                        <?php
                        $i = 0
                        ?>
                        <?php $__currentLoopData = $groupByCategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category => $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="nav-item">
                            <a class="nav-link <?php echo e($i == 0?'active':''); ?>" data-toggle="tab" href="#<?php echo e($category); ?>"><?php echo e($category); ?></a>
                        </li>
                        <?php
                        $i++
                        ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>

                    <div class="tab-content">
                        <?php
                        $i = 0

                        ?>

                        <?php $__currentLoopData = $groupByCategoryProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category => $brands): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div id="<?php echo e($category); ?>" class="container tab-pane <?php echo e($i == 0?'active':'fade'); ?>"><br>
                            <div class="row">
                                <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brandName => $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="f-w front-brant-n">
                                    <h4><?php echo e($brandName); ?></h4>
                                </div>
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="f-w mb-4">
                                    <div class="listing-outer details-outer">
                                        <div class="listing-img">
                                            <?php if($product->image): ?>
                                            <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($product->image); ?>" class="product-image-listing">
                                            <?php endif; ?>
                                        </div>
                                        <div class="listing-text">
                                            <h4> <?php echo e($product->name); ?> </h4>
                                            <?php
                                            $cartQuantity = 0;
                                            if (!ShoppingCart::isEmpty()) {
                                                $cartQuantity = getProductFromCartById($product->id,$shop->id);
                                            }
                                            ?>
                                            <h5> Category:<?php echo e($product->category->name); ?> </h5>
                                            <p class="mb-0"> Rs <?php echo e($product->shop_product->price); ?> </p>

                                            <div class="add-to-cart-btn">
                                                <form class="add-to-cart-form" method="post">
                                                    <?php if($cartQuantity): ?>
                                                    <input class="number-pro-btn" type="number" name="quantity" value="<?php echo $cartQuantity ?>" readonly="readonly">
                                                    <?php else: ?>
                                                    <input class="number-pro-btn" type="number" name="quantity" readonly="readonly">
                                                    <?php endif; ?>
													 <input type="hidden" name="shop_id" value="<?php echo e($shop->id); ?>">
                                                    <input type="hidden" name="product_id" value="<?php echo e($product->id); ?>">
                                                    <input type="hidden" name="action" value="">
                                                    <input type="hidden" name="raw_id" value="">
                                                    <button class="add-pro-btn" type="submit" onclick="this.form.action.value = this.value" value="increase">
                                                        <i class="material-icons">add</i>
                                                    </button>
                                                    <button class="min-pro-btn" type="submit" onclick="this.form.action.value = this.value" value="decrease">
                                                        <i class="material-icons">remove</i>
                                                    </button>

                                                </form>
                                                <!--<a class="" href="#"> Add </a>-->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <?php
                        $i++
                        ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                    </div>

                </div>

            </div>



        </div>

    </div>
</section>

<div id="cartModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <!--div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Modal Header</h4>

      </div-->
      <div class="modal-body">
          <h4 class="mt-4"> Important Note </h4>
        <p id="cart-notice"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="common-btn blue-btn" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div class="cart-fixed <?php echo e(ShoppingCart::count()==0?'d-none':''); ?>" id="cart-footer">
    <div class="container">
        <div class="cart-fix-left">
            <i class="material-icons">shopping_cart</i>
            <div class="cart-f-cont">
                <span id="cart-items-count"><?php echo e(ShoppingCart::count()); ?></span> <span class="mr-2"> item(s) </span>
                <p> Rs </p>
                <span id="cart-total"><?php echo e(ShoppingCart::totalPrice()); ?></span>
            </div>
        </div>
        <div class="cart-fix-right">
            <a class="common-btn" href="<?php echo e(route('cart-index')); ?>"> View Cart </a>
            <!--a class="common-btn" href="<?php echo e(route('cart-remove')); ?>"> Empty Cart </a-->
        </div>
    </div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/products.blade.php ENDPATH**/ ?>