<?php $__env->startSection('content'); ?>
<div class="f-w inner-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="f-w text-center"> cart </h2>
			</div>
		</div>
	</div>
</div>
<div class="f-w mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 mt-80">

                <!--h2>Cart</h2-->
                <?php if(count($items)): ?>
                <div class="table">
                    <table>
                        <tr>
                            <th> Items </th>
                            <th> Name </th>
                            <th> Quantity </th>
                            <th> Price </th>
                            <th> Total </th>
                            <th> Action </th>
                        </tr>
                        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td> <img src="<?php echo e(URL::asset($item->image)); ?>"> </td>
                            <td> <?php echo e($item->name); ?> </td>
                            <td> <?php echo e($item->qty); ?> </td>
                            <td> <?php echo e($item->price); ?> </td>
                            <td> <?php echo e($item->total); ?> </td>
                            <td> 
                                <a class="text-danger" href="<?php echo e(route('delete-cart-item',['id' =>$item->__raw_id])); ?>"> 
                                    <span> Delete </span> 
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>

                    <div class="row mt-5">
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6">

                            <div class="table no-border cart-table">
                                <table>
                                    <tr>
                                        <th> Subtotal </th>
                                        <td> <?php echo e($cart_total); ?> </td>
                                    </tr>
                                    <tr>
                                        <th> Tax </th>
                                        <td> 0.00 </td>
                                    </tr>
                                    <tr>
                                        <th> Total </th>
                                        <td> <?php echo e($cart_total); ?> </td>
                                    </tr>
                                </table>
                                <div class="scheck-out-btn">
                                    <?php if(count($items)): ?>
                                    <form action="<?php echo e(action('AddToCart@checkout')); ?>" method="get">
                                        <button class="common-btn blue-btn w-100" type="submit" name="page" value="checkout">Checkout</button>
                                    </form>
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <?php else: ?>
                <p>Your cart is empty.</p>
                <a class="btn btn-success" href="<?php echo e(url('/')); ?>">Continue Shopping</a>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/cart/index.blade.php ENDPATH**/ ?>