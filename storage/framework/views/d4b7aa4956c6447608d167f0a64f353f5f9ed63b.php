<?php $__env->startSection('content'); ?>

<div class="f-w">
    <div class="container-fluid">

        <div class="row mt-80">
            <div class="col-md-12">
                <h2>Shop dashboard</h2>
            </div>
            <div class="col-md-12 mt-5">

                <div class="row">
                    
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Today's Orders</h4> 
                            <i class="material-icons">shopping_cart</i>
                            <h2 class="mb-0 mt-4"> <?php echo e($todayOrders); ?> </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Today's Sale</h4> 
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4"> 
							<?php if($todaySales->total): ?>  
								<?php echo e($todaySales->total); ?>

                            <?php else: ?> 
								0
                            <?php endif; ?>  </h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">Last Week Sale</h4> 
                            <i class="material-icons">payments</i>
                            <h2 class="mb-0 mt-4"><?php echo e($lastWeekSales->total); ?></h2>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="shop-d-block">
                            <h4 class="mb-2">In Stock Products</h4>
                            <i class="material-icons">list_alt</i>
                            <h2 class="mb-0 mt-4"> <?php echo e($inStockProducts); ?> </h2>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('shop/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/shop/dashboard/index.blade.php ENDPATH**/ ?>