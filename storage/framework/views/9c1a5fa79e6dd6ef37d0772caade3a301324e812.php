<ul>
<?php $__currentLoopData = $childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<li>
	    <label><input type="radio" name="category_id" value="<?php echo e($child->id); ?>" <?php echo e($child->id ==$product_category_id?'checked="checked"':''); ?>><?php echo e($child->name); ?></label>
	<?php if(count($child->childs)): ?>
            <?php echo $__env->make('shop.product.categoryChildControl',['product_category_id' => $product_category_id, 'childs' => $child->childs], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?>
	</li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
<?php /**PATH /var/www/html/liquorstore/resources/views/shop/product/categoryChildControl.blade.php ENDPATH**/ ?>