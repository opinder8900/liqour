<?php $__env->startSection('content'); ?>
<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Type of Business</h2>
		</div>
	</div>  

    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            <?php echo $__env->make('superadmin/layouts/tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
    <form action="<?php echo e(url('superadmin/business')); ?>" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo e(old('id',$business->id)); ?>">
        <?php echo e(csrf_field()); ?>

        <div class="cate-input-main">
            <div class="row">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Type Your Business</label>
                    <input type="text" onkeyup="replaceAsSlug('business_name','business_slug')" id="business_name" name="name" required value="<?php echo e(old('name',$business->name)); ?>" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Enter Type of Your Business">
                    <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label class="d-block">Upload Image <?php if($business->image): ?> <img class="pro-img float-right mr-2" src="<?php echo e(URL::asset($business->image)); ?>"> <?php endif; ?></label>
                    <input type="file" name="image" accept="image/*" class="form-control" placeholder="Upload Image">
                    <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Slug</label>
                    <input type="text" name="slug" required value="<?php echo e(old('slug',$business->slug)); ?>" class="form-control <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Slug Name" id="business_slug">
                    <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-12 sa-cat-tabs mt-4">
                    <input class="common-btn" type="submit" value="<?php echo e(empty($business->id) ? 'Add' : 'Save'); ?> Type">
                    <?php if($business->id): ?>
                    <a href="<?php echo e(url('superadmin/business')); ?>" class="common-btn">Cancel</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
    <div class="row mb-3">
		<div class="col-md-6">
			<h2> Business List</h2>
		</div>
    </div>

    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">
            <div class="table custom-table">
                <table>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Images </th>
                            <th> Slug </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $__currentLoopData = $businesses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $business): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td> <?php echo e($business->name); ?> </td>
                            <td> <?php if($business->image): ?> <img class="pro-img" src="<?php echo e(URL::asset($business->image)); ?>"> <?php endif; ?> </td>
                            <td> <?php echo e($business->slug); ?> </td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" data-href="<?php echo e(url('superadmin/business/toggle_status/'.$business->id)); ?>" <?php if($business->status): ?> checked="checked" <?php endif; ?> >
                                    <span class="slider round"></span>
                                </label> 
                            </td>
                            <td>
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="<?php echo e(url('superadmin/business/'.$business->id)); ?>"> 
                                            <img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="<?php echo e(url('superadmin/business/destroy/'.$business->id)); ?>" onclick="return beforeCategoryDelete('<?php echo e($business->name); ?>')"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>    
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <?php echo e($businesses->links()); ?>    
            </div>
                
        </div>
    </div>

</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/business/index.blade.php ENDPATH**/ ?>