<?php $__env->startSection('content'); ?>
<div class="f-w">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mt-80">

            <h2>Order Summary</h2>

            <div class="table mt-5">
                <table>
                    <tr>
                        <th> Items </th>
                        <th> Name </th>
                        <th> Quantity </th>
                        <th> Price </th>
                        <th> Total </th>
                    </tr>
                    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td> item image </td>
                        <td> <?php echo e($item->name); ?> </td>
                        <td> <?php echo e($item->qty); ?> </td>
                        <td> <?php echo e($item->price); ?> </td>
                        <td> <?php echo e($item->total); ?> </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </table>
            </div>
        
        </div>
    </div>
</div>
</div>

<div class="f-w">
<div class="container mb-80">
    <div class="row">

                <div class="col-md-8 mt-5">
                    <h2 class="f-w mb-4"> Delivery address </h2>
                    <?php if(count($addresses) == 0): ?>
                    <form id="address-form" name="address-form" class="address-form" method="post"  action="<?php echo e(action('AddressController@store')); ?>">
                        <input type="hidden" name="redirect_url" value="<?php echo e(Request::url()); ?>"/>
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group">
                            <label for="full_name" class="col-md-12 col-form-label"><?php echo e(__('Full Name')); ?></label>
                            <div class="col-md-12">
                                <input id="full_name" type="text" class="form-control <?php $__errorArgs = ['full_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="full_name" value="<?php echo e(old('full_name')); ?>" required autocomplete="full_name" autofocus>
                                <?php $__errorArgs = ['full_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone" class="col-md-12 col-form-label"><?php echo e(__('Phone')); ?></label>
                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control <?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="phone" value="<?php echo e(old('phone')); ?>" required autocomplete="phone" maxlength="10">
                                <?php $__errorArgs = ['phone'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pin_code" class="col-md-12 col-form-label"><?php echo e(__('Pin Code')); ?></label>
                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control <?php $__errorArgs = ['pin_code'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="pin_code" value="<?php echo e(old('pin_code')); ?>" required autocomplete="pin_code" maxlength="6">
                                <?php $__errorArgs = ['pin_code'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address_line_1" class="col-md-12 col-form-label"><?php echo e(__('Address Line 1')); ?></label>
                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control <?php $__errorArgs = ['address_line_1'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="address_line_1" value="<?php echo e(old('address_line_1')); ?>" required>
                                <?php $__errorArgs = ['address_line_1'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address_line_2" class="col-md-12 col-form-label"><?php echo e(__('Address Line 2')); ?></label>
                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control <?php $__errorArgs = ['address_line_2'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="address_line_2" value="<?php echo e(old('address_line_2')); ?>">
                                <?php $__errorArgs = ['address_line_2'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="landmark" class="col-md-12 col-form-label"><?php echo e(__('landmark')); ?></label>
                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control <?php $__errorArgs = ['landmark'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="landmark" value="<?php echo e(old('landmark')); ?>" required>
                                <?php $__errorArgs = ['landmark'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                <span class="invalid-feedback" role="alert">
                                    <strong><?php echo e($message); ?></strong>
                                </span>
                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="country" class="col-md-12 col-form-label"><?php echo e(__('Country')); ?></label>
                            <div class="col-md-12">
                                <input name="country" id = "country" class="form-control" placeholder = "Country" type="text" value="India" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="state" class="col-md-12 col-form-label"><?php echo e(__('State')); ?></label>
                            <div class="col-md-12">
                                <select name="state" id = "state" class="form-control">
                                    <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($state->state_code); ?>"><?php echo e($state->state_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="city" class="col-md-12 col-form-label"><?php echo e(__('City')); ?></label>
                            <div class="col-md-12">
                                <select name="city" id = "city" class="form-control">
                                    <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($city->id); ?>"><?php echo e($city->city_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </select>
                            </div>
                        </div>

                        <div class="f-w add-new-add-outer mb-4">
                            <input name="default" id="default" class="" type="checkbox"> <span> Mark this as you default address </span>
                        </div>

                        <div class="f-w add-new-add-outer">
                            <input type="submit" value = "Add" id="submit" name="submit" class="common-btn blue-btn">
                        </div>

                    </form>
                </div>
    </div>
</div>
</div>

<div class="f-w">
    <div class="col-lg-12">
        <div class="row">

            <?php else: ?>
            <div class="checkout-add-main">
                <?php if($errors->any()): ?>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo e($err); ?>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>


                <?php if(count($items)): ?>
                <form action="<?php echo e(route('place-order')); ?>" method="post">
                    <?php $__currentLoopData = $addresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="checkout-add-inner">

                        <?php if($address->type == 'shipping'): ?>
                        <input type="radio" name="shipping_address_id" value="<?php echo e($address->id); ?>" <?php echo e($address->default?'checked="checked"':''); ?>>
                        <?php endif; ?>
                        <h4><?php echo e($address->full_name); ?></h4>


                        <?php echo e($address->phone); ?>

                        <?php echo e($address->pin_code); ?>

                        <?php echo e($address->address_line_1); ?>

                        <?php echo e($address->city); ?>

                        <?php echo e($address->state); ?>

                        <?php echo e($address->country); ?>

                        <?php echo e($address->landmark); ?>

                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <div class="f-w checkout-add-payment">

                        <h2 class="mt-5 mb-4"> Payment </h2>

                        <?php echo e(csrf_field()); ?>

                        <label>
                            <input type="radio" name="payment_mode" value="cod"> COD
                        </label>

                        <div class="f-w mt-4">
                            <input class="common-btn blue-btn" type="submit" name="checkout" value="Place Order">
                        </div>

                    </div>

                </form>
                <?php endif; ?>

            </div>

        </div>
    </div>
</div>


                    <?php endif; ?>

            <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/cart/checkout.blade.php ENDPATH**/ ?>