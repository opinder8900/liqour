<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title>E-theka</title>
        <!--Global JS variables-->
        <script>
            var CONFIG = {
            site_url: "<?php echo e(url('/')); ?>",
                    google_map_api_key: "<?php echo e(config('app.google_map_api_key')); ?>",
                    default_location:{
                    latitude : <?php echo e(config('app.default_latitude')); ?>,
                            longitude : <?php echo e(config('app.default_longitude')); ?>

                    },
                    user:{
                    is_logged_in: <?php echo e(Auth::guest() ? false:true); ?>

                    }
            }
        </script>

        <style>
            #map {
                height: 100%;
            }

        </style>
        <!-- Scripts -->

        <!--Please contact jatinder if you want to add app.js here-->
        <!--<script src="<?php echo e(asset('js/front-app.js')); ?>" defer></script>-->

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
              rel="stylesheet">

        <!-- Styles -->
        <!--<link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">-->
        <link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('assets/css/style.css')); ?>" rel="stylesheet">





    </head>
    <body class="<?php echo $__env->yieldContent('body-class'); ?>">
        <div id="app">
            <div class="f-w front-header">
                <nav class="">
                    <div class="container">
                        <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                            E-theka
                        </a>
                        <?php if(Session::has('location')): ?>
                        <button id="change-delivery-address">Change Delivery Address</button>
                        <?php endif; ?>

                        <div class=" justify-content-end" id="navbarSupportedContent">
                            <!-- Left Side Of Navbar -->
                            <!--div class="heaer-btns">
                                <a class="common-btn" href="#"><?php echo e(__('E-theka for Partners')); ?></a>
                                <a class="common-btn" href="#"><?php echo e(__('E-theka for Business')); ?></a>
                            </div-->
                            <div class="header-cart">
                                <a href="<?php echo e(route('cart-index')); ?>">
                                    <i class="material-icons">shopping_cart</i>
                                    <?php if(!ShoppingCart::isEmpty()): ?>
                                    <span id="card-number"><?php echo e(ShoppingCart::count()); ?></span>
                                    <?php endif; ?>
                                </a>
                            </div>
                            <ul class="my-acc-usr-ul">
                                <!-- Authentication Links -->
                                <?php if(auth()->guard()->guest()): ?>
                                <div class="dropdown front-head-user">
                                    <a class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">account_circle</i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <ul>
                                            <li class="">
                                                <a class="" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                                            </li>
                                            <li class="">
                                                <a class="" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <?php else: ?>
                                <li class="dropdown">
                                    <a id="dropdownMenuButton" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php echo e(Auth::user()->name); ?> <?php echo e(Auth::user()->role); ?><span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">

                                        <a class="dropdown-item" href="<?php echo e(route('my-profile')); ?>">
                                            <?php echo e(__('My Profile')); ?>

                                        </a>

                                        <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                           onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                            <?php echo e(__('Logout')); ?>

                                        </a>

                                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                            <?php echo csrf_field(); ?>
                                        </form>

                                    </div>

                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="container-fluid">
                <div class="row"><?php echo $__env->make('superadmin/flash/flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?></div>
            </div>
            <?php if(Session::has('location')): ?>
            <b><?php echo e(Session::get('location.type')); ?></b>
            <p><?php echo e(Session::get('location.display')); ?></p>
            <?php endif; ?>
            
            <?php echo $__env->yieldContent('content'); ?>
            <?php echo $__env->make('layouts.front-footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- Scripts -->


            <script src="<?php echo e(asset('js/jquery-3.5.1.min.js')); ?>" ></script>
            <script src="<?php echo e(asset('js/jquery.cookie.js')); ?>" ></script>
            <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>            
        </div>
    </body>

</html>
<?php /**PATH /var/www/html/liquorstore/resources/views/layouts/error.blade.php ENDPATH**/ ?>