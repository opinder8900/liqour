<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> List of Orders </h2>
        </div>
      
    </div>
    
    <div class="row mt-5">
		<div class="col-md-12">
			<div class="table custom-table">
                <table>
                    <tr>
                        <th> Discount </th>
                        <th> Tax </th>
                        <th> Total </th>
                        <th> Payment Mode </th>
                        <th> Placed On </th>
                       
                        <th> Actions </th>
                    </tr>
                       <?php if($orders): ?>
							<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								 <tr>
									<td> <?php if($order->discount): ?> <?php echo e($order->discount); ?> <?php else: ?> 0 <?php endif; ?></td>
									<td> <?php echo e($order->tax); ?></td>
									<td> <?php echo e($order->total); ?></td>
									<td> <?php echo e($order->payment_mode); ?></td>
									<td> <?php echo e(date("l jS \of F Y h:i:s A",strtotime($order->created_at))); ?></td>
									
									
									
							<td>
								<div class="dropdown dropleft">
									<div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
									</div>
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<a href="<?php echo e(url('superadmin/customer/orders/details/'.$order->id)); ?>"> 
											<i class="material-icons"> add_circle </i> 
											<span> Order Details </span> 
										</a>
										
									</div>
								</div>
							</td>
						</tr>
								
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif; ?>
                                   
                  
                </table>
            </div>
		</div>
    </div>
    
</div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/customer/orders.blade.php ENDPATH**/ ?>