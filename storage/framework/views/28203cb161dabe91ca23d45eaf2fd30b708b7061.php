<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row mb-5">

      <div class="col-md-6">
        <h2> timings Details </h2>
      </div>

      <div class="col-md-6">

        <div class="row">

          <div class="col-lg-4">
            <label >Start</label>
            <input type="date" name="bday" max="3000-12-31" min="1000-01-01" class="form-control">
          </div>

          <div class="col-lg-4">
            <label >End</label>
            <input type="date" name="bday" min="1000-01-01"max="3000-12-31" class="form-control">
          </div>

          <div class="col-lg-4">
            <label class="w-100">&nbsp;</label>
            <button class="common-btn w-100"> Submit </button>
          </div>

        </div>

      </div>
    
    </div>

    <div class="row mb-5">
      <div class="col-lg-12">
        <div class="table custom-table">
          <h4 class="mb-3"> Last Week </h4>
          <table>
            <tr> 
              <th> Day </th>
              <th> Check In </th>
              <th> Check Out </th>
              <th> Total Time </th>
            </tr>
            <tr>
              <td> Monday </td>
              <td> 10:00 am </td>
              <td> 09:00 pm </td>
              <td> 11 Hours </td>
            </tr>
            <tr>
              <td> Tuesday </td>
              <td> 10:00 am </td>
              <td> 09:00 pm </td>
              <td> 11 Hours </td>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <div class="row mb-3">

      <div class="col-md-6">
        <h2> delivery Details </h2>
      </div>

      <div class="col-md-6">

        <div class="row">

          <div class="col-lg-4">
            <label >Start</label>
            <input type="date" name="bday" max="3000-12-31" min="1000-01-01" class="form-control">
          </div>

          <div class="col-lg-4">
            <label >End</label>
            <input type="date" name="bday" min="1000-01-01"max="3000-12-31" class="form-control">
          </div>

          <div class="col-lg-4">
            <label class="w-100">&nbsp;</label>
            <button class="common-btn w-100"> Submit </button>
          </div>

        </div>

      </div>
    
    </div>

    <div class="row mb-5">
      <div class="col-lg-12">
        <div class="table custom-table">
          <h4 class="mb-3"> Monday </h4>
          <h6> Total Delivery: 10 </h6>
          <table>
            <tr> 
              <th> # </th>
              <th> Shop </th>
              <th> Customer </th>
              <th> Pick Up Time </th>
              <th> Delivery Time </th>
            </tr>
            <tr>
              <td> 1 </td>
              <td> tekha 1, Sector 71, mohali </td>
              <td> Deepak, sector 70, mohali </td>
              <td> 11:30 </td>
              <td> 11:50 </td>
            </tr>
            <tr>
              <td> 2 </td>
              <td> tekha 2, Sector 71, mohali </td>
              <td> Aman, sector 80, mohali </td>
              <td> 12:05 </td>
              <td> 12:25 </td>
            </tr>
          </table>
        </div>
      </div>
    </div>

</div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/delivery/show.blade.php ENDPATH**/ ?>