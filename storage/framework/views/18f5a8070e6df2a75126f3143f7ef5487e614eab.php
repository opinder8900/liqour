<?php $__env->startSection('content'); ?>
<div class="f-w">
<div class="container-fluid">

	<div class="row mt-80">
		<div class="col-md-6">
			<h2>Shops List</h2>
		</div>
		<div class="col-md-6 text-right">

			<a class="common-btn green-btn" href="<?php echo e(action('Tradesman\ShopController@create')); ?>"> <i class="material-icons">add_circle_outline</i> <span> Add Shop </span> </a>

		</div>
	</div>  
    
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="table  custom-table">
            <table>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Primary Phone</th>
                    <th>Seconday Phone</th>
                    <th>Open At</th>
                    <th>Close At</th>
                    <th>License Number</th>
                    <th>Actions</th>

                </tr>
                <?php if( $shops ): ?>
                <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
                <tr>
                    <td><?php echo e($shop->name); ?></td>
                    <td><?php echo e($shop->address); ?></td>
                    <td><?php echo e($shop->city->city_name); ?></td>
                    <td><?php echo e($shop->state->state_name); ?></td>
                    <td><?php echo e($shop->primary_phone); ?></td>
                    <td><?php echo e($shop->secondary_phone); ?></td>
                    <td><?php echo e($shop->open_at); ?></td>
                    <td><?php echo e($shop->close_at); ?></td>
                    <td><?php echo e($shop->license_number); ?></td>
                    <td>
                        <div class="dropdown dropleft">
                            <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                            </div>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a href="<?php echo e(route('stores.show', $shop->id)); ?>"> 
								<img class="" src="<?php echo e(URL::asset('assets/images/eye.svg')); ?>"> 
								<span> Show </span> 
							</a>
                            <a href="<?php echo e(route('stores.edit', $shop->id)); ?>"> 
								<img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
								<span> Edit </span> 
							</a>
                            <form method="post" action="<?php echo e(route('stores.destroy', $shop->id)); ?>" onsubmit="return beforeProductDelete('<?php echo e($shop->name); ?>')">
                            <?php echo e(csrf_field()); ?>

                            <?php echo e(method_field('DELETE')); ?>

                            <div class="form-group delete-shop">
                                <img class="" src="<?php echo e(URL::asset('assets/images/delete.svg')); ?>">
                                <input type="submit" class="" value="Delete">
                            </div>
                            </form>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </table>
            <?php echo e($shops->links()); ?>

            </div>
        </div>

</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('tradesman/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/tradesman/shops/index.blade.php ENDPATH**/ ?>