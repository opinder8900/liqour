<?php $__env->startSection('content'); ?>

<h2>Not Found</h2>
<p>You are not autorized to view this page</p>
<?php echo e($exception->getMessage()); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.error', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/errors/404.blade.php ENDPATH**/ ?>