<?php $__env->startSection('body-class', 'order-page'); ?>
<?php $__env->startSection('content'); ?>

<?php echo $__env->make('layouts.lower-tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<div class="my-acc-bg"></div>
<section class="my-acc-sec">

    <div class="container">

        <div class="acc-main">
            <div class="row">


                <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


                <div class="col-md-8">

                    <div class="my-acc-main">
                        <h4 class="f-w mb-4">My Order</h4>

                        <div class="f-w">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#order" role="tab" aria-controls="home" aria-selected="true">Open Orders</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#past_order" role="tab" aria-controls="profile" aria-selected="false">Past Orders</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#cancel_order" role="tab" aria-controls="contact" aria-selected="false">Canceled Orders</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                
                                <div class="tab-pane fade show active" id="order" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="my-acc-or-main mt-4">
                                        <ul>
                                            <?php if($new_orders->count()): ?>
                                            <?php $__currentLoopData = $new_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li>
                                                <div class="my-acc-or-img">
                                                    <?php if( $order->shop->image): ?>
                                                    <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($order->shop->image); ?>" class="shop-image-listing">
                                                    <?php endif; ?>

                                                </div>
                                                <div class="my-acc-or-text">
                                                    <h4> <?php echo e($order->shop->name); ?> </h4>
                                                    <p>  <?php echo e($order->shop->address); ?> </p>
                                                    <p> ORDER #<?php echo e($order->id); ?> | <?php echo e(date('l, F jS Y h:i A',strtotime($order->created_at))); ?> </p>
                                                    <a class="common-btn blue-btn mt-3" href="<?php echo e(route('show-cancel-order',['id' => $order->id])); ?>">Cancel Order</a>
                                                </div>

                                                <div class="my-acc-or-item">
                                                    <p> 
                                                        <?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php echo e($item->product->name); ?>, 
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </p>
                                                    <h4> Total Paid:  <?php echo e($order->total); ?> </h4>
                                                </div>
                                            </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>


                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="past_order" role="tabpanel" aria-labelledby="profile-tab">
                                    
                                        <div class="my-acc-or-main mt-4">
                                            <ul>
                                                <?php if($past_orders->count()): ?>
                                                
                                                <?php $__currentLoopData = $past_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li>
                                                    <div class="my-acc-or-img">
                                                        <?php if( $order->shop->image): ?>
                                                        <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($order->shop->image); ?>" class="shop-image-listing">
                                                        <?php endif; ?>

                                                    </div>
                                                    <div class="my-acc-or-text">
                                                        <h4> <?php echo e($order->shop->name); ?> </h4>
                                                        <p>  <?php echo e($order->shop->address); ?> </p>
                                                        <p> ORDER #<?php echo e($order->id); ?> | <?php echo e(date('l, F jS Y h:i A',strtotime($order->created_at))); ?> </p>
                                                        <p>Delivered On ,  <?php echo e(date('l, F jS Y h:i A',strtotime($order->delivered_on))); ?> </p>

                                                    </div>

                                                    <div class="my-acc-or-item">
                                                        <p> 
                                                            <?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php echo e($item->product->name); ?>, 
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </p>
                                                        <h4> Total Paid:  <?php echo e($order->total); ?> </h4>
                                                    </div>
                                                </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php else: ?>
                                                <li>No Order Found</li>
                                                <?php endif; ?>


                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="cancel_order" role="tabpanel" aria-labelledby="contact-tab">
                                        
                                            <div class="my-acc-or-main mt-4">
                                                <ul>
                                                    <?php if($cancelled_orders->count()): ?>
                                                    <?php $__currentLoopData = $cancelled_orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li>
                                                        <div class="my-acc-or-img">
                                                            <?php if( $order->shop->image): ?>
                                                            <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($order->shop->image); ?>" class="shop-image-listing">
                                                            <?php endif; ?>

                                                        </div>
                                                        <div class="my-acc-or-text">
                                                            <h4> <?php echo e($order->shop->name); ?> </h4>
                                                            <p>  <?php echo e($order->shop->address); ?> </p>
                                                            <p> ORDER #<?php echo e($order->id); ?> | <?php echo e(date('l, F jS Y h:i A',strtotime($order->created_at))); ?> </p>
                                                            <p>Delivered On ,  <?php echo e(date('l, F jS Y h:i A',strtotime($order->delivered_on))); ?> </p>

                                                        </div>

                                                        <div class="my-acc-or-item">
                                                            <p> 
                                                                <?php $__currentLoopData = $order->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php echo e($item->product->name); ?>, 
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </p>
                                                            <h4> Total Paid:  <?php echo e($order->total); ?> </h4>
                                                        </div>
                                                    </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php else: ?>
                                                    <li>No Order Found</li>
                                                    <?php endif; ?>


                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
            </section>


            <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/profile/my-orders.blade.php ENDPATH**/ ?>