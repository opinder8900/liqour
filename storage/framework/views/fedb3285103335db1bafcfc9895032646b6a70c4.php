<div class="row text-center">
    <?php $__currentLoopData = $businesses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $business): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-md-4">
        <a class="bus-home-outer" href="<?php echo e(route('city-business-shops',['city' => $city, 'business' => $business->slug])); ?>">
            <div class="cat-img-home">
                <img src="<?php echo e(URL::asset($business->image)); ?>">
            </div>
            <div class="cat-img-home">
                <h4> <?php echo e($business->name); ?> </h4>
            </div>
        </a>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div><?php /**PATH /var/www/html/liquorstore/resources/views/location/main.blade.php ENDPATH**/ ?>