<?php $__env->startSection('content'); ?>

<div class="my-acc-bg"></div>
<section class="my-acc-sec">

    <div class="container">

    <div class="acc-main">

        <div class="row">

            <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
          

            <div class="col-lg-8">
            <div class="my-acc-main">
            <h4 class="f-w mb-5">Change Password</h4>
                    <form method="POST" action="<?php echo e(route('change-password')); ?>">
                        <?php echo csrf_field(); ?> 
   
                         <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <p class="text-danger"><?php echo e($error); ?></p>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
  
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">Current Password</label>
  
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">New Password</label>
  
                            <div class="col-md-12">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-12 col-form-label">New Confirm Password</label>
    
                            <div class="col-md-12">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
   
                        <div class="form-group mb-0 mt-4 row">
                            <div class="col-md-12">
                                <button type="submit" class="common-btn blue-btn">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
               
            </div>
            </div>

        </div>

    </div>
    </div>
</section>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/profile/change-password.blade.php ENDPATH**/ ?>