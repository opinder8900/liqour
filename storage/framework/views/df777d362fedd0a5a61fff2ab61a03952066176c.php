<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 product-tab-page">
    <div class="container-fluid">

    <div class="row">
		<div class="col-md-6">
			<h2>Products List</h2>
		</div>
    </div>
    
    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            <?php echo $__env->make('superadmin/layouts/tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
    <form action="<?php echo e(url('superadmin/products')); ?>" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo e(old('id',$product->id)); ?>">
        <?php echo e(csrf_field()); ?>

        <div class="cate-input-main">
            <div class="row">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Business</label>
                    <select data-target-href="<?php echo e(url('superadmin/categories/by_business')); ?>" class="form-control product_business <?php $__errorArgs = ['business_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="business_id">
                        <option value="">Select Business</option>
                        <?php $__currentLoopData = $businesses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($id); ?>" <?php if($product->business_id==$id): ?> selected="selected" <?php endif; ?> ><?php echo e($name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <?php $__errorArgs = ['business_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Category</label>
                    <select data-target-href="<?php echo e(url('superadmin/brands/by_category')); ?>" class="form-control product_category <?php $__errorArgs = ['category_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="category_id">
                        <option value="">Select Category</option>
                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($id); ?>" <?php if($product->category_id==$id): ?> selected="selected" <?php endif; ?> ><?php echo e($name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <?php $__errorArgs = ['category_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Brand</label>
                    <select class="form-control product_brand <?php $__errorArgs = ['brand_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="brand_id">
                        <option value="">Select Brand</option>
                        <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($id); ?>" <?php if($product->brand_id==$id): ?> selected="selected" <?php endif; ?> ><?php echo e($name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <?php $__errorArgs = ['brand_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Name of Product</label>
                    <input type="text" onkeyup="replaceAsSlug('product_name','product_slug')" id="product_name"  name="name" required value="<?php echo e(old('name',$product->name)); ?>" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Enter Product Name">
                    <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Slug</label>
                    <input type="text" name="slug" required value="<?php echo e(old('slug',$product->slug)); ?>" class="form-control <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Slug Name" id="product_slug">
                    <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Size</label>
                    <div class="input-group">
                      <input type="text" name="size" required value="<?php echo e(old('size',$product->size)); ?>" class="form-control  <?php $__errorArgs = ['size'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Size" aria-label="Size" aria-describedby="basic-addon2">
                      <div class="input-group-append">
                        <select class="btn btn-outline-secondary" name="size_label">
                            <option value="ml" <?php if($product->size_label=='ml'): ?> selected="selected" <?php endif; ?> >ml</option>
                            <option value="lt" <?php if($product->size_label=='lt'): ?> selected="selected" <?php endif; ?> >ltr</option>
                            <option value="gm" <?php if($product->size_label=='gm'): ?> selected="selected" <?php endif; ?> >gm</option>
                            <option value="kg" <?php if($product->size_label=='kg'): ?> selected="selected" <?php endif; ?> >kg</option>
                        </select>
                      </div>
                      <?php $__errorArgs = ['size'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                      <span class="invalid-feedback" role="alert">
                          <strong><?php echo e($message); ?></strong>
                      </span>
                      <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                </div>
                <div class="col-lg-4 sa-cat-tabs mt-5">
                    <label class="d-block">Upload Image <?php if($product->image): ?> <img class="pro-img float-right mr-2" src="<?php echo e(URL::asset($product->image)); ?>"> <?php endif; ?></label>
                    
                    <input type="file" class="form-control" accept="image/*"  name="image" placeholder="Upload Image">
                    <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs mt-5">
                    <input class="common-btn mt-4" type="submit" value="<?php echo e(empty($product->id) ? 'Add' : 'Save'); ?> Product">
                    <?php if($product->id): ?>
                    <a href="<?php echo e(url('superadmin/products')); ?>" class="common-btn">Cancel</a>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </form>

    <div class="row mb-3">
		<div class="col-md-6">
			<h2>Products</h2>
		</div>
    </div>


    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">

            <div class="table custom-table">

                <table>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Category </th>
                            <th> Brand </th>
                            <th> Slug </th>
                            <th> Image </th>
                            <th> Size </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td> <?php echo e($product->name); ?> </td>
                            <td> <?php echo e($product->category->name); ?> </td>
                            <td> <?php echo e($product->brand->name); ?> </td>
                            <td> <?php echo e($product->slug); ?> </td>
                            <td> <?php if($product->image): ?> <img class="pro-img" src="<?php echo e(URL::asset($product->image)); ?>"> <?php endif; ?> </td>
                            <td> <?php echo e($product->size); ?> <?php echo e($product->size_label); ?> </td>
                            <td>
                                <label class="switch">
                                    <input type="checkbox" data-href="<?php echo e(url('superadmin/products/toggle_status/'.$product->id)); ?>" <?php if($product->status): ?> checked="checked" <?php endif; ?> >
                                    <span class="slider round"></span>
                                </label> 
                            </td>
                            <td>
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="<?php echo e(url('superadmin/products/'.$product->id)); ?>"> 
                                            <img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="<?php echo e(url('superadmin/products/destroy/'.$product->id)); ?>" onclick="return beforeCategoryDelete('<?php echo e($product->name); ?>')"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>    
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <?php echo e($products->links()); ?> 

            </div>
        </div>
    </div>



    

    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/product/index.blade.php ENDPATH**/ ?>