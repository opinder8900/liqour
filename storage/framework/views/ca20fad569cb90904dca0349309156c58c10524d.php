<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 catrgory-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Add Category</h2>
		</div>
	</div>  

    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            <?php echo $__env->make('superadmin/layouts/tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
    <form action="<?php echo e(url('superadmin/categories')); ?>" method="post"  enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo e(old('id',$category->id)); ?>">
        <?php echo e(csrf_field()); ?>

    
        <div class="cate-input-main">
            <div class="row">
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Select Business</label>
                    <select class="form-control <?php $__errorArgs = ['business_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="business_id">
                        <option value="">Select Business</option>
                        <?php $__currentLoopData = $businesses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id=>$name): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($id); ?>" <?php if($category->business_id==$id): ?> selected="selected" <?php endif; ?> ><?php echo e($name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <?php $__errorArgs = ['business_id'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Category</label>
                    <input type="text"  onkeyup="replaceAsSlug('category_name','category_slug')" id="category_name" name="name" required value="<?php echo e(old('name',$category->name)); ?>" class="form-control <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Category Name">
                    <?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-4 sa-cat-tabs">
                    <label>Enter Slug</label>
                    <input type="text" name="slug" required value="<?php echo e(old('slug',$category->name)); ?>" class="form-control <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" placeholder="Slug name"  id="category_slug">
                    <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($message); ?></strong>
                    </span>
                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                </div>
                <div class="col-lg-12 sa-cat-tabs mt-4">
                    <input class="common-btn" type="submit" value="<?php echo e(empty($category->id) ? 'Add' : 'Save'); ?> Category">
                    <?php if($category->id): ?>
                    <a href="<?php echo e(url('superadmin/categories')); ?>" class="common-btn">Cancel</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </form>
    <div class="row mb-3">
		<div class="col-md-6">
			<h2> Categories List</h2>
			<h2> Total - <?php echo e($totalCategories); ?></h2>
			
		</div>
    </div>

    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">
            <div class="table custom-table">
                <table>
                    <thead>
                        <tr>
                            <th> Name </th>
                            <th> Business </th>
                            <th> Slug </th>
                            <th> Status </th>
                            <th> Actions </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td> <?php echo e($category->name); ?> </td>
                            <td> <?php echo e($category->business->name); ?> </td>
                            <td> <?php echo e($category->slug); ?> </td>
                            <td> 
                                <label class="switch">
                                    <input type="checkbox" data-href="<?php echo e(url('superadmin/categories/toggle_status/'.$category->id)); ?>" <?php if($category->status): ?> checked="checked" <?php endif; ?> >
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="<?php echo e(url('superadmin/categories/'.$category->id)); ?>"> 
                                            <img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="<?php echo e(url('superadmin/categories/destroy/'.$category->id)); ?>" onclick="return beforeCategoryDelete('<?php echo e($category->name); ?>')"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>    
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                <?php echo e($categories->links()); ?>

            </div>
        </div>
    </div>

</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/category/index.blade.php ENDPATH**/ ?>