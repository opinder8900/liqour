<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2>Categories</h2>
		</div>
	</div>  

    <div class="row mt-5">
        <div class="col-lg-12 sa-cat-tabs">
            <?php echo $__env->make('superadmin/layouts/tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>

    <div class="cate-input-main">
        <div class="row">
            <div class="col-lg-4 sa-cat-tabs">
                <input type="text" class="form-control" placeholder="Enter Type of Your Business">
            </div>
            <div class="col-lg-4 sa-cat-tabs">
                <input type="text" class="form-control" placeholder="Enter Slug">
            </div>
            <div class="col-lg-4 sa-cat-tabs">
                <input class="common-btn" type="submit" value="Add Type">
            </div>
        </div>
    </div>

    <div class="row mb-80">
        <div class="col-lg-12 sa-cat-tabs">
            <div class="f-w custom-table">

                    <div class="table-head">
                        <div class="table-th"> Name </div>
                        <div class="table-th"> Slug </div>
                        <div class="table-th"> Status </div>
                        <div class="table-th"> Actions </div>
                    </div>

                    <div class="table-body">
                        
                        <div class="table-td"> Liquor </div>
                        <div class="table-td"> 123 </div>
                        <div class="table-td">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label> 
                        </div>
                        <div class="table-td">
                            <div class="dropdown dropleft">
                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="#"> 
                                        <img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
                                        <span> Edit </span> 
                                    </a>
                                    <a href="#"> 
                                        <i class="material-icons"> delete </i> 
                                        <span> Delete </span> 
                                    </a>
                                </div>
                            </div>    
                        </div>
                        
                    </div>

                    <div class="table-body">
                        
                        <div class="table-td"> Grocery </div>
                        <div class="table-td"> 32 </div>
                        <div class="table-td">
                                <label class="switch">
                                    <input type="checkbox">
                                    <span class="slider round"></span>
                                </label> 
                        </div>
                        <div class="table-td">
                            <div class="dropdown dropleft">
                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="#"> 
                                        <img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
                                        <span> Edit </span> 
                                    </a>
                                    <a href="#"> 
                                        <i class="material-icons"> delete </i> 
                                        <span> Delete </span> 
                                    </a>
                                </div>
                            </div>    
                        </div>
                        
                    </div>

                    <div class="table-body">
                        
                        <div class="table-td"> Grocery </div>
                        <div class="table-td"> 56 </div>
                        <div class="table-td">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label> 
                        </div>
                        <div class="table-td">
                                <div class="dropdown dropleft">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                    </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="#"> 
                                            <img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
                                            <span> Edit </span> 
                                        </a>
                                        <a href="#"> 
                                            <i class="material-icons"> delete </i> 
                                            <span> Delete </span> 
                                        </a>
                                    </div>
                                </div>    
                        </div>
                        
                    </div>

                    <div class="table-body">
                        
                        <div class="table-td"> Medical </div>
                        <div class="table-td"> 123 </div>
                        <div class="table-td">
                            <label class="switch">
                                <input type="checkbox">
                                <span class="slider round"></span>
                            </label> 
                        </div>
                        <div class="table-td">
                            <div class="dropdown dropleft">
                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a href="#"> 
                                        <img class="" src="<?php echo e(URL::asset('assets/images/edit.svg')); ?>"> 
                                        <span> Edit </span> 
                                    </a>
                                    <a href="#"> 
                                        <i class="material-icons"> delete </i> 
                                        <span> Delete </span> 
                                    </a>
                                </div>
                            </div>    
                        </div>
                        
                    </div>
                    
            </div>
        </div>
    </div>

</div>
</div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/shops/index.blade.php ENDPATH**/ ?>