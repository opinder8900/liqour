<?php $__env->startSection('content'); ?>

<div class="f-w mt-80 bussiness-tab-page">
<div class="container-fluid">
    
    <div class="row">
		<div class="col-md-6">
			<h2> List of Customers </h2>
        </div>
        <div class="col-md-6">
            <div class="search-common back-search w-100">
                <input type="text" placeholder="Search Customer" class="form-control">
                <button class="common-btn serch-front-btn" type="submit">Search</button>
            </div>
		</div>
    </div>
    
    <div class="row mt-5">
		<div class="col-md-12">
			<div class="table custom-table">
                <table>
                    <tr>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Phone </th>
                        <th> Address </th>
                        <th> Actions </th>
                    </tr>
                       <?php if($customers): ?>
										<?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											 <tr>
												<td> <?php echo e($customer->name); ?></td>
												<td> <?php echo e($customer->email); ?></td>
												<td> <?php echo e($customer->phone); ?> </td>
												<td> <?php echo e($customer->phone); ?> </td>

                                        <td>
                                            <div class="dropdown dropleft">
                                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <img class="more-icon" src="<?php echo e(URL::asset('assets/images/more.svg')); ?>">
                                                </div>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="<?php echo e(url('superadmin/customer/orders/'.$customer->id)); ?>"> 
                                                        <i class="material-icons"> add_circle </i> 
                                                        <span> View Orders </span> 
                                                    </a>
                                                    <a href="#"> 
                                                        <i class="material-icons"> visibility </i> 
                                                        <span> View Products </span> 
                                                    </a>
                                                    <a href="#"> 
                                                        <i class="material-icons"> delete </i> 
                                                        <span> Delete </span> 
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
											
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<?php endif; ?>
                                   
                  
                </table>
            </div>
		</div>
    </div>
    
</div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('superadmin/layouts/app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/superadmin/customer/index.blade.php ENDPATH**/ ?>