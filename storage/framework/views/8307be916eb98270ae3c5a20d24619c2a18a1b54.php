<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.lower-tabs', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<div class="f-w inner-banner">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
                <h2 class="f-w text-center"> Shops in <?php echo e($city); ?> </h2>
			</div>
		</div>
	</div>
</div>

<div class="f-w bread inner-bread">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <ul class="f-w">
                    <li> <a href="<?php echo e(url('/')); ?>"> Home </a> <span><i class="material-icons"> keyboard_arrow_right </i></span> </li>
                    <li> <p> Shops </p> </li>
                </ul>
            </div>
        </div>

    </div>
</div>
<section class="front-shops-page">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">

                <div class="f-w search-common">
                    <form action="" method="GET">
                        <div class="front-search-out">
                            <i class="material-icons">search</i>
                            <input  class="f-w" placeholder="Search for stores" type="text" value="<?php echo e(app('request')->input('search')); ?>" name="search" required/>
                            <button class="common-btn serch-front-btn" type="submit">Search</button>
                        </div>
                        <a href="<?php echo e(route('business-shops',['business'=>$business])); ?>"> <i class="material-icons">close</i> Clear <span> Search </span></a>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">


            <?php $__currentLoopData = $shops; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
				$checkOpen = in_range(strtotime($shop->open_at), strtotime($shop->close_at)) ? "out-of-time" : "";
				
            ?>
            <div class="col-lg-6 mt-5 <?php echo e($checkOpen); ?>">
                <a href="<?php echo e(route('business-shop-products',['business' => $business,'shop' => $shop->slug])); ?>">
                    <div class="listing-outer">
                        <div class="listing-img">
                            <?php if($shop->image): ?>
                            <img src="<?php echo e(URL::to('/')); ?>/<?php echo e($shop->image); ?>" class="shop-image-listing">
                            <?php endif; ?>
                        </div>
                        <div class="listing-text">
                            <h4> <?php echo e($shop->name); ?></h4>
                            <!--p class="mb-0"> All kind of liquor </p-->
                            <p class="mb-0"> Timings: <?php echo e(date('h:i a',(strtotime($shop->open_at)))); ?> – <?php echo e(date('h:i a',(strtotime($shop->close_at)))); ?>    </p>
                            <p class="mb-2"> <?php echo e($shop->address); ?>, <?php echo e($shop->city->name); ?> </p>

<!--
                            <h6> Lat: <?php echo e($shop->latitude); ?> </h6>
                            <h6> Long: <?php echo e($shop->longitude); ?>  </h6>
-->
                           
                            <h6> Distance: <?php echo e(round($shop->distance,2)); ?> km </h6>

                        </div>
                    </div>
                </a>
               
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/business/shops.blade.php ENDPATH**/ ?>