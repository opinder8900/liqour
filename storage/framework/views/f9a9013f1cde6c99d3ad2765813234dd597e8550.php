<?php $__env->startSection('content'); ?>

<div class="my-acc-bg"></div>
<section class="my-acc-sec">
    <div class="container">

        <div class="acc-main">
            <div class="row">
                <?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <div class="col-md-8">
                    <div class="my-acc-main">
                        <h4 class="f-w mb-5">Shipping Address</h4>
                        <?php if($shipping_address): ?>
                        <?php $__currentLoopData = $shipping_address; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="add-list">
                            <h4> <?php echo e($address->full_name); ?> </h4>
                            <?php if($address->default): ?>
                            <span class="badge-info">Default</span>
                            <?php endif; ?>
                            <p> <?php echo e($address->address_line_1); ?>, <?php echo e($address->address_line_1); ?>, <?php echo e($address->city); ?> <?php echo e($address->state); ?> <?php echo e($address->pin_code); ?>, <?php echo e($address->country); ?> </p>
                            <a class="f-w mb-4" href="<?php echo e(route('address.edit', $address->id)); ?>"> Edit </a>
                            <form method="post" action="<?php echo e(route('address.destroy', $address->id)); ?>" onsubmit="return beforeAddressDelete()">
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(method_field('DELETE')); ?>

                                <div class="form-group delete-shop">

                                    <input class="common-btn blue-btn" type="submit" class="" value="Delete">
                                </div>
                            </form>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>

                        <a href="<?php echo e(route('address.create')); ?>" id="add-new-address"  class="common-btn blue-btn" >Add Address</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

    <?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.front-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/liquorstore/resources/views/address/index.blade.php ENDPATH**/ ?>